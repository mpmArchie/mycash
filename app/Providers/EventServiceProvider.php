<?php

namespace App\Providers;

use App\Events\ActivityTriggered;
use App\Events\NewLoanRequested;
use App\Events\TopUpRequested;
use App\Events\Claimed;
use App\Events\Retrieved;
use App\Events\Complained;
use App\Events\Contacted;
use App\Events\Subscribed;
use App\Events\CalculationCreated;
use App\Events\CalculationVerificationResent;
use App\Events\LoanRequested;
use App\Listeners\ApiRequestLoanNotification;
use App\Listeners\LogActivityNotification;
use App\Listeners\SendEmailNewLoanRequestNotification;
use App\Listeners\SendEmailTopUpRequestNotification;
use App\Listeners\SendEmailClaimNotification;
use App\Listeners\SendEmailRetrieveNotification;
use App\Listeners\SendEmailComplainNotification;
use App\Listeners\SendEmailContactNotification;
use App\Listeners\SendEmailSubscribeNotification;
use App\Listeners\SendEmailLoanNotification;
use App\Listeners\SendEmailVerificationNotification;
use App\Listeners\SendSMSVerificationNotification;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification as VerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            VerificationNotification::class,
        ],
        NewLoanRequested::class => [
            SendEmailNewLoanRequestNotification::class,
        ],
        TopUpRequested::class => [
            SendEmailTopUpRequestNotification::class,
        ],
        Claimed::class => [
            SendEmailClaimNotification::class,
        ],
        Retrieved::class => [
            SendEmailRetrieveNotification::class,
        ],
        Complained::class => [
            SendEmailComplainNotification::class,
        ],
        Contacted::class => [
            SendEmailContactNotification::class,
        ],
        Subscribed::class => [
            SendEmailSubscribeNotification::class,
        ],
        CalculationCreated::class => [
            SendEmailVerificationNotification::class,
            SendSMSVerificationNotification::class,
        ],
        CalculationVerificationResent::class => [
            SendEmailVerificationNotification::class,
            SendSMSVerificationNotification::class,
        ],
        LoanRequested::class => [
            ApiRequestLoanNotification::class,
            SendEmailLoanNotification::class,
        ],
        ActivityTriggered::class => [
            LogActivityNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
