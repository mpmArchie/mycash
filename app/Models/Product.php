<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Product extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id')->withDefault(['path' => config('setting.default.placeholder')]);
    }

    public function banner()
    {
        return $this->hasOne(Image::class, 'id', 'banner_id')->withDefault(['path' => config('setting.default.placeholder')]);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    public function getUpdateAttribute()
    {
        $name = $this->updater->name;
        $datetime = new Carbon($this->updated_at);
        return 'Pembaruan terakhir oleh ' . $name . ' pada ' . $datetime->setTimezone('Asia/Jakarta')->format('d-m-Y, H:i');
    }

    public function getEscapedNameAttribute()
    {
        return str_replace('<br/>', ' ', $this->name);
    }

    public function getSlugAttribute()
    {
        return str_slug($this->escaped_name, '-');
    }

    private function __populate($data, $image, $banner)
    {
        $dataWithoutImages = array_except($data, ['image', 'banner']);
        $dataWithImage = array_add($dataWithoutImages, 'image_id', $image->id);
        $dataWithImageBanner = array_add($dataWithImage, 'banner_id', $banner->id);
        $dataWithUser = array_add($dataWithImageBanner, 'updated_by', auth()->user()->id);

        return $dataWithUser;
    }

    public function add($data)
    {
        $image = Image::add($data['image']);
        $banner = Image::add($data['banner']);

        return $this->create($this->__populate($data, $image, $banner));
    }

    public function edit($data)
    {
        $image = $this->image;
        if (array_has($data, 'image'))
        {
            Image::remove($image);
            $image = Image::add($data['image']);
        }

        $banner = $this->banner;
        if (array_has($data, 'banner'))
        {
            Image::remove($banner);
            $banner = Image::add($data['banner']);
        }

        return $this->update($this->__populate($data, $image, $banner));
    }

    public function remove()
    {
        Image::remove($this->image);
        Image::remove($this->banner);
        return $this->delete();
    }
}
