<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $primaryKey = 'key';
    protected $guarded = [];

    public $incrementing = false;

    public function updateValue($value)
    {
        return $this->update($value);
    }
}
