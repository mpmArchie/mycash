<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Activity extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault(['id' => 0]);
    }

    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->setTimezone('Asia/Jakarta')->locale('id')->isoFormat('D MMMM YYYY, HH:mm [WIB]');
    }

    public function add($data)
    {
        return $this->create($data);
    }
}
