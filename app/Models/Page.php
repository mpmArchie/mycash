<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    const HOME = 1;
    const ABOUT = 2;
    const PRODUCT = 3;
    const CONTACT = 4;
    const INFO = 5;
    const ARTICLE = 6;
}
