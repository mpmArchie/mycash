<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    protected $guarded = [];
    protected $hidden = ['pivot'];
    protected $appends = array('label_duration');

    public function getLabelAttribute()
    {
        return $this->duration . ' bulan, ' . $this->rate . '%';
    }

    public function getLabelDurationAttribute()
    {
        return $this->duration . ' bulan';
    }

    public function getLabelRateAttribute()
    {
        return $this->rate . '%';
    }

    public function add($data)
    {
        return $this->create($data);
    }

    public function edit($data)
    {
        return $this->update($data);
    }
}
