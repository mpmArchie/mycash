<?php

namespace App\Models;

use App\Models\Activity;
use App\Models\Image;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'role', 'email_verified_at',
        'photo_id',
        'ktp', 'phone', 'address', 'rt', 'rw',
        'city', 'kelurahan', 'kecamatan',
        'postcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id')->orderBy('created_at', 'desc');
    }

    public function photo()
    {
        return $this->hasOne(Image::class, 'id', 'photo_id')->withDefault(['path' => config('setting.default.photo')]);
    }

    public function hasRole($role)
    {
        return $this->role == $role;
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function add($data)
    {
        $filteredData = array_except($data, ['password']);
        $dataWithPassword = array_add($filteredData, 'password', bcrypt($data['password']));
        $dataWithRole = array_add($dataWithPassword, 'role', 'admin');
        $finalData = array_add($dataWithRole, 'email_verified_at', now());

        return $this->create($finalData);
    }

    public function edit($data)
    {
        $filteredData = array_except($data, ['password']);
        $dataWithPassword = array_has($data, 'password') ? array_add($filteredData, 'password', bcrypt($data['password'])) : $filteredData;

        return $this->update($dataWithPassword);
    }

    public function updatePassword($data)
    {
        $filteredData = array_except($data, ['password']);
        array_set($filteredData, 'password', bcrypt($data['password']));

        return $this->update($filteredData);
    }

    public function updatePhoto($data)
    {
        $photo = $this->photo;
        if (isset($photo->id))
        {
            Image::remove($photo);
        }
        $photo = Image::add($data['photo'], 'uploads/photos');

        $data = ['photo_id' => $photo->id];

        return $this->update($data);
    }

    public function toActivity($name, $detail, $data)
    {
        return [
            'user_id' => $this->id,
            'name' => $name,
            'detail' => $detail,
            'data' => json_encode($data),
        ];
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($this, $token));
    }
}
