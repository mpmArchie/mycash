<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $guarded = [];

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    private function __populate($data)
    {
        $dataWithUser = array_add($data, 'updated_by', auth()->user()->id);

        return $dataWithUser;
    }

    public function add($data)
    {
        return $this->create($this->__populate($data));
    }

    public function edit($data)
    {
        return $this->update($this->__populate($data));
    }
}
