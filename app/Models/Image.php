<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];

    public function getUriAttribute()
    {
        return asset($this->path);
    }

    public static function add($image, $path = 'uploads/images')
    {
        $filename = $image->getClientOriginalName();
        $path = $image->store($path);

        return Image::create(compact('filename', 'path'));
    }

    public static function remove($image)
    {
        $path = $image->path;
        $exists = Storage::exists($path);
        if ($exists && starts_with($path, 'uploads/')) Storage::delete($path);

        return $image->delete();
    }
}
