<?php

namespace App\Models;

use App\Models\Image;
use App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Banner extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id')->withDefault(['path' => config('setting.default.placeholder')]);
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'page_id')->withDefault(['name' => 'Tidak Diketahui']);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    public function setTitleAttribute($value)
    {
        if (is_null($value)) {
            $this->attributes['title'] = '';
        }
        else
        {
            $this->attributes['title'] = $value;
        }
    }

    public function setSubtitleAttribute($value)
    {
        if (is_null($value)) {
            $this->attributes['subtitle'] = '';
        }
        else
        {
            $this->attributes['subtitle'] = $value;
        }
    }

    public function getUpdateAttribute()
    {
        $name = $this->updater->name;
        $datetime = new Carbon($this->updated_at);
        return 'Pembaruan terakhir oleh ' . $name . ' pada ' . $datetime->setTimezone('Asia/Jakarta')->format('d-m-Y, H:i');
    }

    private function __populate($data, $image)
    {
        $dataWithoutBannerPage = array_except($data, ['image', 'page']);
        $dataWithPage = array_add($dataWithoutBannerPage, 'page_id', $data['page']);
        $dataWithImage = array_add($dataWithPage, 'image_id', $image->id);
        $dataWithUser = array_add($dataWithImage, 'updated_by', auth()->user()->id);

        return $dataWithUser;
    }

    public function add($data)
    {
        $image = Image::add($data['image']);

        return $this->create($this->__populate($data, $image));
    }

    public function edit($data)
    {
        $image = $this->image;
        if (array_has($data, 'image'))
        {
            Image::remove($this->image);
            $image = Image::add($data['image']);
        }

        return $this->update($this->__populate($data, $image));

    }

    public function remove()
    {
        Image::remove($this->image);
        return $this->delete();
    }
}
