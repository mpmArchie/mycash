<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $guarded = [];

    public function add($data)
    {
        $dataWithUser = array_add($data, 'user_id', auth()->user()->id);
        return $this->create($dataWithUser);
    }
}
