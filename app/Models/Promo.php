<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Promo extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id')->withDefault(['path' => config('setting.default.placeholder')]);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    public function getUpdateAttribute()
    {
        $name = $this->updater->name;
        $datetime = new Carbon($this->updated_at);
        return 'Pembaruan terakhir oleh ' . $name . ' pada ' . $datetime->setTimezone('Asia/Jakarta')->format('d-m-Y, H:i');
    }

    public function getSlugAttribute()
    {
        return str_slug($this->name, '-');
    }

    private function __populate($data, $image)
    {
        $dataWithoutLogo = array_except($data, ['image']);
        $dataWithImage = array_add($dataWithoutLogo, 'image_id', $image->id);
        $dataWithUser = array_add($dataWithImage, 'updated_by', auth()->user()->id);

        return $dataWithUser;
    }

    public function add($data)
    {
        $image = Image::add($data['image']);

        return $this->create($this->__populate($data, $image));
    }

    public function edit($data)
    {
        $image = $this->image;
        if (array_has($data, 'image'))
        {
            Image::remove($this->image);
            $image = Image::add($data['image']);
        }

        return $this->update($this->__populate($data, $image));

    }

    public function remove()
    {
        Image::remove($this->image);
        return $this->delete();
    }
}
