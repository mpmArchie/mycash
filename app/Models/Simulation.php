<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Simulation extends Model
{
    const MOBIL = '8';
    const MOTOR = '7';

    protected $guarded = [];

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    public function installments()
    {
        return $this->belongsToMany(Installment::class)->withTimestamps();
    }

    public function getCodeFormattedAttribute()
    {
        if ($this->attributes['code'] == Simulation::MOBIL)
        {
            return 'Mobil';
        }
        return 'Motor';
    }

    public function getUpdateAttribute()
    {
        $name = $this->updater->name;
        $datetime = new Carbon($this->updated_at);
        return $name . ', ' . $datetime->setTimezone('Asia/Jakarta')->format('d-m-Y, H:i');
    }

    public function add($data)
    {
        $dataWithUser = array_add(array_except($data, ['installments']), 'updated_by', auth()->user()->id);

        $meta = $this->create($dataWithUser);
        return $meta->installments()->sync(array_get($data, 'installments'));
    }

    public function edit($data)
    {
        $dataWithUser = array_add(array_except($data, ['installments']), 'updated_by', auth()->user()->id);

        $this->update($dataWithUser);
        return $this->installments()->sync(array_get($data, 'installments'));
    }
}
