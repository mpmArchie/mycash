<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    private function __populate($data, $image)
    {
        $dataWithoutLogo = array_except($data, ['image']);
        $dataWithUser = array_add($dataWithoutLogo, 'updated_by', auth()->user()->id);

        if ($image)
        {
            $dataWithUser = array_add($dataWithUser, 'image_id', $image->id);
        }

        return $dataWithUser;
    }

    public function add($data)
    {
        $image = null;
        if (array_has($data, 'image'))
        {
            $image = Image::add($data['image']);
        }

        return $this->create($this->__populate($data, $image));
    }

    public function edit($data)
    {
        $image = $this->image;
        if (array_has($data, 'image'))
        {
            if ($image) Image::remove($this->image);
            $image = Image::add($data['image']);
        }

        return $this->update($this->__populate($data, $image));

    }

    public function remove()
    {
        Image::remove($this->image);
        return $this->delete();
    }
}
