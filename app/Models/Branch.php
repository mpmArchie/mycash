<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Branch extends Model
{
    protected $guarded = [];

    public function getUpdateAttribute()
    {
        $datetime = new Carbon($this->updated_at);
        return 'Pembaruan terakhir pada ' . $datetime->setTimezone('Asia/Jakarta')->format('d-m-Y, H:i');
    }

    public static function resync()
    {
        Branch::where('is_active', true)->update(['is_active' => false]);
        $reference = Branch::where('code', '000');
        $branches = Branch::where('code', '!=', '000');
        $result = api_get_branches();

        DB::beginTransaction();

        foreach ($result as $item) {
            $test = Branch::updateOrCreate(
                ['code' => $item['code']],
                [
                    'name' => $item['name'],
                    'email' => $item['email'],
                    'type' => $item['type'],
                    'is_active' => true,
                ]
            );
        }

        $reference->update(['updated_at' => now()]);

        DB::commit();
    }
}
