<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $guarded = [];

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault(['name' => 'Tidak Diketahui']);
    }

    public function pages()
    {
        return $this->belongsToMany(Page::class)->withTimestamps();
    }

    public function add($data)
    {
        $dataWithUser = array_add(array_except($data, ['pages']), 'updated_by', auth()->user()->id);

        $meta = $this->create($dataWithUser);
        return $meta->pages()->sync(array_get($data, 'pages'));
    }

    public function edit($data)
    {
        $dataWithUser = array_add(array_except($data, ['pages']), 'updated_by', auth()->user()->id);

        $this->update($dataWithUser);
        return $this->pages()->sync(array_get($data, 'pages'));
    }
}
