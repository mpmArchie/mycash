<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calculation extends Model
{
    protected $guarded = [];
    protected $keyType = 'string';
    protected $casts = [
        'is_verified' => 'boolean',
        'is_requested' => 'boolean',
    ];
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault(['id' => 0]);
    }

    public function simulation()
    {
        return $this->hasOne(Simulation::class, 'id', 'simulation_id');
    }

    public function installment()
    {
        return $this->hasOne(Installment::class, 'id', 'installment_id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'code', 'area');
    }

    public function getTotalFormattedAttribute()
    {
        return str_currency($this->total);
    }

    public function isVerified()
    {
        return $this->is_verified;
    }

    public function isRequested()
    {
        return $this->is_requested;
    }

    public function toParams()
    {
        return [
            'id' => $this->id,
            'namaktp' => $this->name,
            'nik' => $this->ktp,
            'notelp' => $this->phone,
            'email' => $this->email,
            'rt' => $this->rt,
            'rw' => $this->rw,
            'kelurahan' => $this->kelurahan,
            'kecamatan' => $this->kecamatan,
            'kota' => $this->city,
            'kodepos' => $this->postcode,
            'jenisagunan' => $this->simulation->code,
            'tahunkendaraan' => $this->year,
            'nilaipembiayaan' => $this->total,
            'tenor' => $this->installment->duration,
            'areapengajuan' => $this->area,
        ];
    }

    public function toActivity()
    {
        $data = [
            'Jenis Agunan' => $this->simulation->name,
            'Nomor Pengajuan' => $this->id,
            'Nama' => $this->name,
            'Nomor KTP' => $this->ktp,
            'Telepon' => $this->phone,
            'Email' => $this->email,
            'RT' => $this->rt,
            'RW' => $this->rw,
            'Kelurahan' => $this->kelurahan,
            'Kecamatan' => $this->kecamatan,
            'Kota' => $this->city,
            'Kode Pos' => $this->postcode,
            'Tahun Kendaraan' => $this->year,
            'Nilai Pembiayaan' => $this->total_formatted,
            'Tenor' => $this->installment->label_duration,
            'Area' => $this->branch->name,
        ];
        return $this->user->toActivity(
            'Pengajuan Baru',
            'Pengajuan baru sudah dikirimkan ke Tim MPM Finance',
            $data
        );
    }

    private function __populate($data)
    {
        $withoutInstallment = array_except($data, ['simulation', 'installment']);
        $verificationCode = str_random(6);
        $id = strtoupper(uniqid('MCR'));
        while (Calculation::where('id', $id)->count() > 0) { // Check for duplicates
            $id = strtoupper(uniqid('MCR'));
        }

        $dataWithId = array_add($withoutInstallment, 'id', $id);
        $dataWithCode = array_add($dataWithId, 'verification_code', $verificationCode);
        $dataWithSimulation = array_add($dataWithCode, 'simulation_id', $data['simulation']);
        $dataWithInstallment = array_add($dataWithSimulation, 'installment_id', $data['installment']);

        if (!$data['area']) array_set($dataWithInstallment, 'area', '');

        return $dataWithInstallment;
    }

    public function result()
    {
        $rate = ($this->installment->rate / 100) / 12;
        $nper = $this->installment->duration;
        $pv = $this->total * -1;
        $fv = 0;
        $type = 0;

        if ($rate == 0)
        {
            return round($this->total / $this->installment->duration, -3);
        }

        // Based on https://stackoverflow.com/a/31088490/5556880
        $PMT = (-$fv - $pv * pow(1 + $rate, $nper)) / (1 + $rate * $type) / ((pow(1 + $rate, $nper) - 1) / $rate);
        return round($PMT, -3);
    }

    public function add($data)
    {
        return $this->create($this->__populate($data));
    }

    public function verify($data)
    {
        if ($data['verification_code'] == $this->verification_code)
        {
            $data = [];
            $data['is_verified'] = true;
            $data['verified_at'] = now();
            $this->update($data);

            return true;
        }

        return false;
    }

    public function resend()
    {
        $data = [];
        $data['verification_code'] = str_random(6);
        $this->update($data);
        return $this;
    }

    public function request()
    {
        if ($this->isRequested()) return;

        $data = [];
        $data['is_requested'] = true;
        $data['requested_at'] = now();
        $this->update($data);

        $email = $this->email;

        $user = User::where('email', $email)->first();
        if ($user)
        {
            $this->update(['user_id' => $user->id]);
            return null;
        }
        else
        {
            $user = new User;
            $password = str_random(10);
            $userData = [
                'role' => 'customer',
                'name' => $this->name,
                'email' => $email,
                'email_verified_at' => now(),
                'password' => bcrypt($password),
                'ktp' => $this->ktp,
                'phone' => $this->phone,
                'address' => $this->address,
                'rt' => $this->rt,
                'rw' => $this->rw,
                'city' => $this->city,
                'kecamatan' => $this->kecamatan,
                'kelurahan' => $this->kelurahan,
                'postcode' => $this->postcode,
            ];
            $user = $user->create($userData);

            $this->update(['user_id' => $user->id]);
            return $password;
        }
    }
}
