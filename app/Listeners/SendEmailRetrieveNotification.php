<?php

namespace App\Listeners;

use App\Events\Retrieved;
use App\Notifications\MailRetrieve;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailRetrieveNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Retrieved $event)
    {
        Notification::route('mail', trim($event->email))->notify(new MailRetrieve($event->data));
    }
}
