<?php

namespace App\Listeners;

use App\Events\Complained;
use App\Notifications\MailComplain;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailComplainNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Complained $event)
    {
        Notification::route('mail', trim(config('setting.email.customer')))->notify(new MailComplain($event->data));
    }
}
