<?php

namespace App\Listeners;

use App\Events\Contacted;
use App\Notifications\MailContact;
use App\Notifications\MailContactToUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailContactNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Contacted $event)
    {
        Notification::route('mail', trim(config('setting.email.customer')))->notify(new MailContact($event->data));
        Notification::route('mail', trim($event->data['email']))->notify(new MailContactToUser($event->data));
    }
}
