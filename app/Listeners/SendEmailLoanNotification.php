<?php

namespace App\Listeners;

use App\Events\LoanRequested;
use App\Notifications\MailNewCustomer;
use App\Notifications\MailRequestLoan;
use App\Notifications\MailRequestLoanToAdmin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailLoanNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(LoanRequested $event)
    {
        Notification::route('mail', trim($event->data['email']))->notify(new MailRequestLoan($event->data));
        Notification::route('mail', trim(config('setting.email.admin')))->notify(new MailRequestLoanToAdmin($event->data));
        if (isset($event->password))
        {
            Notification::route('mail', trim($event->data['email']))->notify(new MailNewCustomer($event->data['name'], $event->password));
        }
    }
}
