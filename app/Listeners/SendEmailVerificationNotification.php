<?php

namespace App\Listeners;

use App\Notifications\MailVerificationCode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailVerificationNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::route('mail', trim($event->data->email))->notify(new MailVerificationCode($event->data));
    }
}
