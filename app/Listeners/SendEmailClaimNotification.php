<?php

namespace App\Listeners;

use App\Events\Claimed;
use App\Notifications\MailClaim;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailClaimNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Claimed $event)
    {
        Notification::route('mail', trim($event->email))->notify(new MailClaim($event->data));
    }
}
