<?php

namespace App\Listeners;

use App\Events\NewLoanRequested;
use App\Notifications\MailNewLoanRequest;
use App\Notifications\MailNewLoanRequestToUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailNewLoanRequestNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewLoanRequested $event)
    {
        Notification::route('mail', trim($event->email))->notify(new MailNewLoanRequest($event->data));
        Notification::route('mail', trim($event->data['email']))->notify(new MailNewLoanRequestToUser($event->data));
    }
}
