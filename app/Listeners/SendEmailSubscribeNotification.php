<?php

namespace App\Listeners;

use App\Events\Subscribed;
use App\Notifications\MailSubscribeNewsletter;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailSubscribeNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Subscribed $event)
    {
        Notification::route('mail', trim($event->data['email']))->notify(new MailSubscribeNewsletter());
    }
}
