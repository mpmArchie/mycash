<?php

namespace App\Listeners;

use App\Models\Activity;
use App\Events\ActivityTriggered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogActivityNotification
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ActivityTriggered $event)
    {
        $activity = new Activity;
        $activity->add($event->data);
    }
}
