<?php

namespace App\Listeners;

use App\Events\LoanRequested;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class ApiRequestLoanNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(LoanRequested $event)
    {
        api_send_loan($event->data->toParams());
    }
}
