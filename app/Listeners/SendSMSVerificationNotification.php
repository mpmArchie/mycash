<?php

namespace App\Listeners;

use App\Channels\SMSChannel;
use App\Notifications\SMSVerificationCode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendSMSVerificationNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::route(SMSChannel::class, $event->data->phone)->notify(new SMSVerificationCode($event->data->verification_code));
    }
}
