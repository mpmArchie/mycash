<?php

namespace App\Listeners;

use App\Events\TopUpRequested;
use App\Notifications\MailTopUpRequest;
use App\Notifications\MailTopUpRequestToUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class SendEmailTopUpRequestNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TopUpRequested $event)
    {
        Notification::route('mail', trim($event->email))->notify(new MailTopUpRequest($event->data));
        Notification::route('mail', trim($event->data['email']))->notify(new MailTopUpRequestToUser($event->data));
    }
}
