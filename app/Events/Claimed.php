<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class Claimed
{
    use SerializesModels;

    public $email;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email, $data)
    {
        $this->email = $email;
        $this->data = $data;
    }
}
