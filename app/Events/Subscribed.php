<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class Subscribed
{
    use SerializesModels;

    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}
