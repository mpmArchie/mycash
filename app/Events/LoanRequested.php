<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class LoanRequested
{
    use SerializesModels;

    public $data;
    public $password;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $password = null)
    {
        $this->data = $data;
        $this->password = $password;
    }
}
