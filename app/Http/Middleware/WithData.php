<?php

namespace App\Http\Middleware;

use Closure;

class WithData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$data)
    {
        if (in_array('user', $data)) {
            $user = auth()->user();
            \View::share('userData', $user ?? []);
        }
        return $next($request);
    }
}
