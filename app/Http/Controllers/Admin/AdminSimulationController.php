<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use App\Models\Installment;
use App\Models\Simulation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminSimulationController extends Controller
{
    public function index()
    {
        $installments = Installment::all()->sortBy('rate')->sortBy('duration');
        $simulations = Simulation::all();
        $branchCount = Branch::where('code', '!=', '000')->count();
        $branchLastUpdate = Branch::where('code', '000')->first()->update;
        $branches = [
            'count' => $branchCount,
            'update' => $branchLastUpdate,
        ];

        return view('admin.simulations.home')->with(compact('installments', 'simulations', 'branches'));
    }

    public function create()
    {
        $installments = Installment::all()->sortBy('rate')->sortBy('duration');

        return view('admin.simulations.create')->with(compact('installments'));
    }

    public function store(Request $request, Simulation $simulation)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());
        $success = $simulation->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.simulations.index')->with('status', 'Simulasi kredit berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Simulation $simulation)
    {
        $installments = Installment::all()->sortBy('rate')->sortBy('duration');

        return view('admin.simulations.edit')->with(compact('installments', 'simulation'));
    }

    public function update(Request $request, Simulation $simulation)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $simulation->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.simulations.index')->with('status', 'Simulasi kredit berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Simulation $simulation)
    {
        $simulation->delete();

        return response()->json();
    }

    public function updateBranches()
    {
        Branch::resync();

        return redirect()->route('admin.simulations.index')->with('status', 'Daftar cabang berhasil diperbarui');
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'code' => 'required',
            'start_year' => 'required|numeric',
            'end_year' => 'required|numeric|gte:start_year',
            'max_payment' => 'required|numeric|max:100',
            'installments' => 'required',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama produk tidak boleh kosong',
            'code.required' => 'Jenis agunan tidak boleh kosong',
            'start_year.required' => 'Tahun tidak boleh kosong',
            'start_year.numeric' => 'Tahun harus berupa angka',
            'end_year.required' => 'Tahun tidak boleh kosong',
            'end_year.numeric' => 'Tahun harus berupa angka',
            'end_year.gte' => 'Tahun terbaru harus lebih besar atau sama dengan tahun terlama',
            'max_payment.required' => 'Maksimum pembiayaan tidak boleh kosong',
            'max_payment.numeric' => 'Maksimum pembiayaan harus berupa angka',
            'max_payment.max' => 'Maksimum pembiayaan tidak boleh lebih dari 100%',
            'installments.required' => 'Pilih minimal satu',
        ];
    }
}
