<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminProductController extends Controller
{
    public function index()
    {
        $encouragementWords = Setting::where('key', 'encouragement_words')->first();
        $products = Product::all();

        return view('admin.products.home', [
            'encouragementWords' => $encouragementWords['value'],
            'products' => $products,
        ]);
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request, Product $product)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $product->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.products.index')->with('status', 'Produk berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Product $product)
    {
        return view('admin.products.edit')->with(compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $rules = $this->validationRules();
        array_set($rules, 'image', 'image|max:500');
        array_set($rules, 'banner', 'image|max:500');
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $product->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.products.index')->with('status', 'Produk berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Product $product)
    {
        $product->remove();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'summary' => 'required|max:140',
            'image' => 'required|image|max:500',
            'banner' => 'required|image|max:500',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'summary.required' => 'Ringkasan tidak boleh kosong',
            'summary.max' => 'Ringkasan tidak boleh lebih dari :max karakter',
            'image.required' => 'Gambar tidak boleh kosong',
            'image.image' => 'File harus berupa gambar',
            'image.max' => 'Ukuran file maksimum 500 KB',
            'banner.required' => 'Banner tidak boleh kosong',
            'banner.image' => 'File harus berupa gambar',
            'banner.max' => 'Ukuran file maksimum 500 KB',
        ];
    }
}
