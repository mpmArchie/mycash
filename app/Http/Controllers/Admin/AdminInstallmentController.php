<?php

namespace App\Http\Controllers\Admin;

use App\Models\Installment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminInstallmentController extends Controller
{
    public function create()
    {
        return view('admin.simulations.installment.create');
    }

    public function store(Request $request, Installment $installment)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());
        $success = $installment->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.simulations.index')->with('status', 'Tenor berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Installment $installment)
    {
        return view('admin.simulations.installment.edit')->with(compact('installment'));
    }

    public function update(Request $request, Installment $installment)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $installment->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.simulations.index')->with('status', 'Tenor berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Installment $installment)
    {
        $installment->delete();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'duration' => 'required|numeric|min:1',
            'rate' => 'required|numeric|min:0|max:100',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'duration.required' => 'Lama tenor tidak boleh kosong',
            'duration.numeric' => 'Lama tenor harus berupa angka',
            'duration.min' => 'Lama tenor minimum 1 bulan',
            'rate.required' => 'Persentase tenor tidak boleh kosong',
            'rate.numeric' => 'Persentase tenor harus berupa angka',
            'rate.min' => 'Persentase tenor minimum 0%',
            'rate.max' => 'Persentase tenor maksimum 100%',
        ];
    }
}
