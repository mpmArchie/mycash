<?php

namespace App\Http\Controllers\Admin;

use App\Models\Partner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminPartnerController extends Controller
{
    public function index()
    {
        $partners = Partner::all();

        return view('admin.contents.partner.home')->with(compact('partners'));
    }

    public function create()
    {
        return view('admin.contents.partner.create');
    }

    public function store(Request $request, Partner $partner)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $partner->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.partner.index')->with('status', 'Partner berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Partner $partner)
    {
        return view('admin.contents.partner.edit')->with(compact('partner'));
    }

    public function update(Request $request, Partner $partner)
    {
        $rules = $this->validationRules();
        array_set($rules, 'image', 'image|max:100');
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $partner->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.partner.index')->with('status', 'Partner berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Partner $partner)
    {
        $partner->remove();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'website' => 'url',
            'image' => 'required|image|max:100',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'website.url' => 'Website tidak valid',
            'image.required' => 'Logo tidak boleh kosong',
            'image.image' => 'Logo harus berupa gambar',
            'image.max' => 'Ukuran file maksimum 100 KB',
        ];
    }
}
