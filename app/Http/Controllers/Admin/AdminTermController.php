<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminTermController extends Controller
{
    public function index()
    {
        $termsConditions = Setting::where('key', 'terms_conditions')->first();

        return view('admin.contents.term.home', [
            'termsConditions' => $termsConditions['value'],
        ]);
    }
}
