<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminFaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::all()->sortBy('ordering');

        return view('admin.contents.faq.home')->with(compact('faqs'));
    }

    public function create()
    {
        return view('admin.contents.faq.create');
    }

    public function store(Request $request, Faq $faq)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());
        $success = $faq->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.faq.index')->with('status', 'Pertanyaan berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Faq $faq)
    {
        return view('admin.contents.faq.edit')->with(compact('faq'));
    }

    public function update(Request $request, Faq $faq)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $faq->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.faq.index')->with('status', 'Pertanyaan berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function updateOrdering(Request $request)
    {
        $faqsOrdering = $request->input('faq');
        for ($i=0; $i < count($faqsOrdering); $i++) {
            Faq::where('id', $faqsOrdering[$i])->update([ 'ordering' => $i ]);
        }
        return response()->json([ 'input' => $faqsOrdering ]);
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'question' => 'required',
            'answer' => 'required',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'question.required' => 'Pertanyaan tidak boleh kosong',
            'answer.required' => 'Jawaban tidak boleh kosong',
        ];
    }
}
