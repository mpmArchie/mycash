<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Models\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminBannerController extends Controller
{
    public function index()
    {
        $banners = Banner::all();
        $pages = Page::where('id', 1)->orWhere('id', 2)->get();

        return view('admin.contents.banner.home')->with(compact('banners', 'pages'));
    }

    public function create()
    {
        $pages = Page::where('id', 1)->orWhere('id', 2)->get();

        return view('admin.contents.banner.create')->with(compact('pages'));
    }

    public function store(Request $request, Banner $banner)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $banner->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.banner.index')->with('status', 'Banner berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Banner $banner)
    {
        $pages = Page::where('id', 1)->orWhere('id', 2)->get();

        return view('admin.contents.banner.edit')->with(compact('banner', 'pages'));
    }

    public function update(Request $request, Banner $banner)
    {
        $rules = $this->validationRules();
        array_set($rules, 'image', 'image|max:500');
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $banner->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.banner.index')->with('status', 'Banner berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Banner $banner)
    {
        $banner->remove();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'title' => 'max:255',
            'subtitle' => 'max:255',
            'page' => 'required',
            'image' => 'required|image|max:500',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'title.max' => 'Maksimum :max karakter',
            'subtitle.max' => 'Maksimum :max karakter',
            'page.required' => 'Pilihan halaman tidak boleh kosong',
            'image.required' => 'Gambar tidak boleh kosong',
            'image.image' => 'File harus berupa gambar',
            'image.max' => 'Ukuran file maksimum 500 KB',
        ];
    }
}
