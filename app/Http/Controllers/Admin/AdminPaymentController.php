<?php

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use App\Models\Payment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminPaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::all()->sortBy('ordering');

        return view('admin.contents.payment.home')->with(compact('payments'));
    }

    public function create()
    {
        return view('admin.contents.payment.create');
    }

    public function store(Request $request, Payment $payment)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $payment->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.payment.index')->with('status', 'Metode pembayaran berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Payment $payment)
    {
        return view('admin.contents.payment.edit')->with(compact('payment'));
    }

    public function update(Request $request, Payment $payment)
    {
        $rules = $this->validationRules();
        if (isset($payment->image)) {
            array_set($rules, 'name', 'max:100');
            array_set($rules, 'image', 'image|max:100');
        }

        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $payment->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.payment.index')->with('status', 'Metode pembayaran berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function updateOrdering(Request $request)
    {
        $paymentsOrdering = $request->input('payment');
        for ($i=0; $i < count($paymentsOrdering); $i++) {
            Payment::where('id', $paymentsOrdering[$i])->update([ 'ordering' => $i ]);
        }
        return response()->json([ 'input' => $paymentsOrdering ]);
    }

    public function destroy(Payment $payment)
    {
        $payment->remove();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'name' => 'max:100|required_without_all:image',
            'description' => 'required',
            'image' => 'required_without_all:name|image|max:100',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.max' => 'Maksimal :max karakter',
            'name.required_without_all' => 'Nama tidak boleh kosong jika logo kosong',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'image.required_without_all' => 'Logo tidak boleh kosong jika nama kosong',
            'image.image' => 'Logo harus berupa gambar',
            'image.max' => 'Ukuran file maksimum 100 KB',
        ];
    }
}
