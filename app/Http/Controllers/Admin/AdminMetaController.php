<?php

namespace App\Http\Controllers\Admin;

use App\Models\Meta;
use App\Models\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminMetaController extends Controller
{
    public function index()
    {
        $metas = Meta::all();

        return view('admin.metas.home')->with(compact('metas'));
    }

    public function create()
    {
        $pages = Page::all();

        return view('admin.metas.create')->with(compact('pages'));
    }

    public function store(Request $request, Meta $meta)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $meta->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.metas.index')->with('status', 'Meta tag berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Meta $meta)
    {
        $pages = Page::all();

        return view('admin.metas.edit')->with(compact('pages', 'meta'));
    }

    public function update(Request $request, Meta $meta)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $meta->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.metas.index')->with('status', 'Meta tag berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Meta $meta)
    {
        $meta->delete();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'name' => 'max:100|required_without_all:property,content',
            'property' => 'max:100|required_without_all:name,content',
            'content' => 'required_without_all:name,property',
            'pages' => 'required',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.max' => 'Maksimal :max karakter',
            'name.required_without_all' => 'Data name, property, dan content harus terisi minimal salah satu',
            'property.max' => 'Maksimal :max karakter',
            'property.required_without_all' => 'Data name, property, dan content harus terisi minimal salah satu',
            'content.required_without_all' => 'Data name, property, dan content harus terisi minimal salah satu',
            'pages.required' => 'Pilih minimal satu halaman',
        ];
    }
}
