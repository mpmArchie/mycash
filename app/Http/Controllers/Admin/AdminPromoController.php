<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminPromoController extends Controller
{
    public function index()
    {
        $promos = Promo::all();

        return view('admin.contents.promo.home')->with(compact('promos'));
    }

    public function create()
    {
        return view('admin.contents.promo.create');
    }

    public function store(Request $request, Promo $promo)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $promo->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.promo.index')->with('status', 'Promo berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Promo $promo)
    {
        return view('admin.contents.promo.edit')->with(compact('promo'));
    }

    public function update(Request $request, Promo $promo)
    {
        $rules = $this->validationRules();
        array_set($rules, 'image', 'image|max:500');
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $promo->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.promo.index')->with('status', 'Promo berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Promo $promo)
    {
        $promo->remove();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|max:500',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'image.required' => 'Gambar tidak boleh kosong',
            'image.image' => 'File harus berupa gambar',
            'image.max' => 'Ukuran file maksimum 500 KB',
        ];
    }
}
