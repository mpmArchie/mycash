<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminUserController extends Controller
{
    public function showProfile()
    {
        $user = auth()->user();

        return view('admin.users.profile')->with(compact('user'));
    }

    public function updatePhoto(Request $request)
    {
        $attributes = $request->validate(
            [
                'photo' => 'required|image',
            ],
            [
                'photo.required' => 'Gambar tidak boleh kosong',
                'photo.image' => 'File harus berupa gambar',
            ]
        );

        $user = auth()->user();
        $success = $user->updatePhoto($attributes);
        if ($success)
        {
            return redirect()->route('admin.users.index')->with('status', 'Gambar profil berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function updateProfile(Request $request)
    {
        $user = auth()->user();
        $rules = $this->validationRules(',' . $user->id);
        $rules = array_except($rules, ['password_user', 'password']);
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $user->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.users.index')->with('status', 'Profil berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function showPassword()
    {
        return view('admin.users.password');
    }

    public function updatePassword(Request $request)
    {
        $attributes = $request->validate(
            [
                'password_old' => 'required',
                'password' => 'required|min:6|confirmed',
            ],
            [
                'password_old.required' => 'Kata sandi lama tidak boleh kosong',
                'password.required' => 'Kata sandi baru tidak boleh kosong',
                'password.min' => 'Minimal :min karakter',
                'password.confirmed' => 'Konfirmasi kata sandi tidak sama',
            ]
        );

        $user = auth()->user();
        $oldPassword = $attributes['password_old'];
        if (!Auth::once(
            [
                'email' => $user->email,
                'password' => $oldPassword,
            ]
        ))
        {
            return back()->withErrors(
                [
                    'password_old' => 'Kata sandi salah. Silakan ulangi lagi',
                ]
            );
        }

        $success = $user->updatePassword($attributes);
        if ($success)
        {
            return redirect()->route('admin.users.index')->with('status', 'Kata sandi berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function index()
    {
        $users = User::where('role', 'admin')->get();

        return view('admin.users.home')->with(compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request, User $user)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        if (!Auth::once(
            [
                'email' => auth()->user()->email,
                'password' => $attributes['password_user'],
            ]
        ))
        {
            return back()->withErrors(
                [
                    'password_user' => 'Kata sandi Anda salah. Silakan ulangi lagi',
                ]
            )->withInputs($request->all());
        }

        $success = $user->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.users.index')->with('status', 'Pengguna berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(User $user)
    {
        return view('admin.users.edit')->with(compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $rules = $this->validationRules(',' . $user->id);
        if (!$request->input('password')) {
            $rules = array_except($rules, ['password']);
        }
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        if (!Auth::once(
            [
                'email' => auth()->user()->email,
                'password' => $attributes['password_user'],
            ]
        ))
        {
            return back()->withErrors(
                [
                    'password_user' => 'Kata sandi Anda salah. Silakan ulangi lagi',
                ]
            )->withInputs($request->all());
        }

        $success = $user->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.users.index')->with('status', 'Pengguna berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json();
    }

    public function validationRules($id = '')
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users,email' . $id,
            'password' => 'required|min:6',
            'password_user' => 'required',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama pengguna tidak boleh kosong',
            'name.max' => 'Maksimal :max karakter',
            'email.required' => 'Email pengguna tidak boleh kosong',
            'email.email' => 'Format email tidak valid',
            'email.unique' => 'Email sudah terdaftar',
            'password.required' => 'Kata sandi pengguna tidak boleh kosong',
            'password.min' => 'Minimal :min karakter',
            'password_user.required' => 'Kata sandi Anda tidak boleh kosong',
        ];
    }
}
