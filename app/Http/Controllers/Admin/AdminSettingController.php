<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminSettingController extends Controller
{
    public function create()
    {
    }

    public function store(Request $request)
    {
    }

    public function edit(Setting $setting)
    {
    }

    public function update(Request $request, Setting $setting)
    {
        switch ($setting->key) {
            case 'encouragement_words':
                $name = 'Encouragement words';
                break;
            case 'terms_conditions':
                $name = 'Syarat dan ketentuan';
                break;
            default:
                $name = 'Data';
                break;
        }

        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages($name));

        $success = $setting->updateValue($attributes);
        if ($success)
        {
            return back()->with('status', $name . ' berhasil diubah');
        }

        return back();
    }

    public function destroy(Setting $setting)
    {
    }

    public function validationRules()
    {
        return [
            'value' => 'required',
        ];
    }

    public function validationErrorMessages($key)
    {
        return [
            'value.required' => $key . ' tidak boleh kosong',
        ];
    }
}
