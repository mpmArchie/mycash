<?php

namespace App\Http\Controllers\Admin;

use App\Models\Testimony;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminTestimonyController extends Controller
{
    public function index()
    {
        $testimonies = Testimony::all();

        return view('admin.contents.testimony.home')->with(compact('testimonies'));
    }

    public function create()
    {
        return view('admin.contents.testimony.create');
    }

    public function store(Request $request, Testimony $testimony)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $testimony->add($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.testimony.index')->with('status', 'Testimoni berhasil ditambahkan');
        }

        return back()->withErrors($attributes);
    }

    public function edit(Testimony $testimony)
    {
        return view('admin.contents.testimony.edit')->with(compact('testimony'));
    }

    public function update(Request $request, Testimony $testimony)
    {
        $rules = $this->validationRules();
        array_set($rules, 'image', 'image|max:200');
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $testimony->edit($attributes);
        if ($success)
        {
            return redirect()->route('admin.contents.testimony.index')->with('status', 'Testimoni berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function destroy(Testimony $testimony)
    {
        $testimony->remove();

        return response()->json();
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'image' => 'required|image|max:200',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'content.required' => 'Isi testimoni tidak boleh kosong',
            'image.required' => 'Foto tidak boleh kosong',
            'image.image' => 'Foto harus berupa gambar',
            'image.max' => 'Ukuran file maksimum 200 KB',
        ];
    }
}
