<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CustomerAccountController extends Controller
{
    public function index()
    {
        return view('customer.account.home');
    }

    public function showProfile()
    {
        $user = auth()->user();

        return view('customer.account.profile')->with(compact('user'));
    }

    public function updatePhoto(Request $request)
    {
        $attributes = $request->validate(
            [
                'photo' => 'required|image',
            ],
            [
                'photo.required' => 'Gambar tidak boleh kosong',
                'photo.image' => 'File harus berupa gambar',
            ]
        );

        $user = auth()->user();
        $success = $user->updatePhoto($attributes);
        if ($success)
        {
            return redirect()->route('customer.account.profile')->with('status', 'Gambar profil berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function updateProfile(Request $request)
    {
        $user = auth()->user();
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $user->edit($attributes);
        if ($success)
        {
            return redirect()->route('customer.account.profile')->with('status', 'Profil berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function showPassword()
    {
        return view('customer.account.password');
    }

    public function updatePassword(Request $request)
    {
        $attributes = $request->validate(
            [
                'password_old' => 'required',
                'password' => 'required|min:6|confirmed',
            ],
            [
                'password_old.required' => 'Kata sandi lama tidak boleh kosong',
                'password.required' => 'Kata sandi baru tidak boleh kosong',
                'password.min' => 'Minimal :min karakter',
                'password.confirmed' => 'Konfirmasi kata sandi tidak sama',
            ]
        );

        $user = auth()->user();
        $oldPassword = $attributes['password_old'];
        if (!Auth::once(
            [
                'email' => $user->email,
                'password' => $oldPassword,
            ]
        ))
        {
            return back()->withErrors(
                [
                    'password_old' => 'Kata sandi salah. Silakan ulangi lagi',
                ]
            );
        }

        $success = $user->updatePassword($attributes);
        if ($success)
        {
            return redirect()->route('customer.account')->with('status', 'Kata sandi berhasil diperbarui');
        }

        return back()->withErrors($attributes);
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|numeric|starts_with:0',
            'address' => 'required',
            'rt' => 'required|numeric',
            'rw' => 'required|numeric',
            'city' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'postcode' => 'required',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'phone.required' => 'Nomor ponsel tidak boleh kosong',
            'phone.numeric' => 'Nomor ponsel harus berupa angka',
            'phone.starts_with' => 'Nomor ponsel harus diawali dangan angka nol',
            'address.required' => 'Alamat tidak boleh kosong',
            'rt.required' => 'RT tidak boleh kosong',
            'rt.numeric' => 'RT harus berupa angka',
            'rw.required' => 'RW tidak boleh kosong',
            'rw.numeric' => 'RW harus berupa angka',
            'city.required' => 'Kota tidak boleh kosong',
            'kecamatan.required' => 'Kecamatan tidak boleh kosong',
            'kelurahan.required' => 'Kelurahan tidak boleh kosong',
            'postcode.required' => 'Kode pos tidak boleh kosong',
        ];
    }
}
