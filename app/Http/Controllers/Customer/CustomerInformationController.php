<?php

namespace App\Http\Controllers\Customer;

use App\Models\Complain;
use App\Events\Complained;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerInformationController extends Controller
{
    public function index()
    {
        return view('customer.info.home');
    }

    public function showComplain()
    {
        return view('customer.info.complain');
    }

    public function storeComplain(Request $request, Complain $complain)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $complain->add($attributes);
        if ($success)
        {
            $user = auth()->user();
            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'ktp' => $user->ktp,
                'complain' => $attributes['detail'],
            ];
            event(new Complained($data));

            return redirect()->route('customer.info')->with('status', 'Pengaduan berhasil dikirimkan');
        }

        return back()->withErrors($attributes);
    }

    public function validationRules()
    {
        return [
            'detail' => 'required|min:200',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'detail.required' => 'Rincian pengaduan tidak boleh kosong',
            'detail.min' => 'Minimal :min karakter',
        ];
    }
}
