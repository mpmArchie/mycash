<?php

namespace App\Http\Controllers\Customer;

use App\Models\Branch;
use App\Models\Simulation;
use App\Models\Installment;
use App\Events\NewLoanRequested;
use App\Events\TopUpRequested;
use App\Events\Claimed;
use App\Events\Retrieved;
use App\Events\ActivityTriggered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerServiceController extends Controller
{
    public function index()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);

        return view('customer.service.home')->with(compact('agreements'));
    }

    public function showRequest()
    {
        return view('customer.service.request');
    }

    public function showRequestNew()
    {
        $simulations = Simulation::with(['installments' => function ($query) {
            return $query->select('id', 'duration')->orderBy('duration');
        }])->select('id', 'code')
           ->get();

        return view('customer.service.request-new')->with(compact('simulations'));
    }

    public function storeRequestNew(Request $request)
    {
        $attributes = $request->validate([
            'type' => 'required',
            'brand' => 'required',
            'year' => 'required|numeric',
            'license_plate' => 'required',
            'total' => 'required|numeric',
            'installment' => 'required',
            'area' => 'required',
        ], [
            'type.required' => 'Jenis kendaraan tidak boleh kosong',
            'brand.required' => 'Unit / brand tidak boleh kosong',
            'year.required' => 'Tahun kendaraan tidak boleh kosong',
            'year.numeric' => 'Tahun kendaraan harus berupa angka',
            'license_plate.required' => 'Nomor plat tidak boleh kosong',
            'total.required' => 'Nilai pembiayaan tidak boleh kosong',
            'total.numeric' => 'Nilai pembiayaan harus berupa angka',
            'area.required' => 'Area pangajuan tidak boleh kosong',
        ]);

        $user = auth()->user();
        $branch = Branch::where('code', $attributes['area'])->first();

        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'ktp' => $user->ktp,
            'type' => $attributes['type'] == '8' ? 'Mobil' : 'Motor',
            'brand' => $attributes['brand'],
            'year' => $attributes['year'],
            'license_plate' => $attributes['license_plate'],
            'total' => $attributes['total'],
            'installment' => Installment::find($attributes['installment'])->label_duration,
            'created_at' => now(),
        ];

        event(new NewLoanRequested($branch->email, $data));

        $activityData = $user->toActivity(
            'Pengajuan Baru',
            'Email pengajuan pinjaman baru telah dikirimkan ke ' . $branch->email,
            [
                'Jenis Kendaraan' => $attributes['type'] == '8' ? 'Mobil' : 'Motor',
                'Unit / Brand' => $attributes['brand'],
                'Tahun Kendaraan' => $attributes['year'],
                'Nomor Plat' => $attributes['license_plate'],
                'Total Pembiayaan' => $attributes['total'],
                'Jangka Waktu' => Installment::find($attributes['installment'])->label_duration,
            ]
        );
        event(new ActivityTriggered($activityData));

        return redirect()->route('customer.service')->with('status', 'Pengajuan baru berhasil diajukan');
    }

    public function showRequestTopup()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);

        return view('customer.service.request-topup')->with(compact('agreements'));
    }

    public function storeRequestTopup(Request $request)
    {
        $attributes = $request->validate([
            'email' => 'required',
            'agreement_number' => 'required',
            'total' => 'required|numeric',
        ], [
            'agreement_number.required' => 'Nomor kontrak tidak boleh kosong',
            'total.required' => 'Nilai pembiayaan tidak boleh kosong',
            'total.numeric' => 'Nilai pembiayaan harus berupa angka',
        ]);

        $user = auth()->user();
        $summary = api_get_summary($attributes['agreement_number']);

        $data = [
            'email' => $user->email,
            'agreement_number' => $attributes['agreement_number'],
            'total' => str_currency($attributes['total']),
            'name' => $summary['name'],
            'type' => $summary['type'],
            'brand' => $summary['brand'],
            'year' => $summary['year'],
            'license_plate' => $summary['license_plate'],
            'total_outstanding' => str_currency($summary['total_outstanding']),
            'received' => str_currency($attributes['total'] - $summary['total_outstanding']),
            'created_at' => now(),
        ];

        event(new TopUpRequested($attributes['email'], $data));

        $activityData = $user->toActivity(
            'Top Up',
            'Email pengajuan top up telah dikirimkan ke ' . $attributes['email'],
            [
                'Nomor Kontrak' => $attributes['agreement_number'],
                'Nilai Pembiayaan' => str_currency($attributes['total']),
            ]
        );
        event(new ActivityTriggered($activityData));

        return redirect()->route('customer.service')->with('status', 'Top up berhasil diajukan');
    }

    public function showClaim()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);

        return view('customer.service.claim')->with(compact('agreements'));
    }

    public function storeClaim(Request $request)
    {
        $attributes = $request->validate([
            'email' => 'required',
            'claim_type' => 'required',
            'agreement_number' => 'required',
            'date' => 'required',
            'location' => 'required',
            'detail' => 'required',
        ], [
            'claim_type.required' => 'Jenis klaim tidak boleh kosong',
            'agreement_number.required' => 'Nomor kontrak tidak boleh kosong',
            'date.required' => 'Tanggal kejadian tidak boleh kosong',
            'location.required' => 'Lokasi kejadian tidak boleh kosong',
            'detail.required' => 'Detail kejadian tidak boleh kosong',
        ]);

        $data = array_except($attributes, ['email']);
        event(new Claimed($attributes['email'], $data));

        $user = auth()->user();
        $activityData = $user->toActivity(
            'Klaim Asuransi',
            'Email pengajuan klaim asuransi telah dikirimkan ke ' . $attributes['email'],
            [
                'Jenis Klaim' => $attributes['claim_type'],
                'Nomor Kontrak' => $attributes['agreement_number'],
                'Tanggal Kejadian' => $attributes['date'],
                'Lokasi Kejadian' => $attributes['location'],
                'Detail Kejadian' => $attributes['detail'],
            ]
        );
        event(new ActivityTriggered($activityData));

        return redirect()->route('customer.service')->with('status', 'Klaim asuransi berhasil diajukan');
    }

    public function showRetrieve()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);

        return view('customer.service.retrieve')->with(compact('agreements'));
    }

    public function storeRetrieve(Request $request)
    {
        $attributes = $request->validate([
            'email' => 'required',
            'agreement_number' => 'required',
            'date' => 'required',
            'time' => 'required',
        ], [
            'agreement_number.required' => 'Nomor kontrak tidak boleh kosong',
            'date.required' => 'Tanggal pengambilan tidak boleh kosong',
            'time.required' => 'Waktu pengambilan tidak boleh kosong',
        ]);

        $data = array_except($attributes, ['email']);
        event(new Retrieved($attributes['email'], $data));

        $user = auth()->user();
        $activityData = $user->toActivity(
            'Pengambilan BPKB',
            'Email pengajuan pengambilan BPKB telah dikirimkan ke ' . $attributes['email'],
            [
                'Nomor Kontrak' => $attributes['agreement_number'],
                'Tanggal Pengambilan' => $attributes['date'],
                'Waktu Pengambilan' => $attributes['time'],
            ]
        );
        event(new ActivityTriggered($activityData));

        return redirect()->route('customer.service')->with('status', 'Pengambilan BPKB berhasil diajukan');
    }
}
