<?php

namespace App\Http\Controllers\Customer;

use App\Models\Activity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerCreditController extends Controller
{
    public function index()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);

        return view('customer.credit.home')->with(compact('agreements'));
    }

    public function showAll()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);
        $summaries = [];
        foreach ($agreements as $agreement)
        {
            $summary = api_get_summary($agreement['agreement_number']);
            $summary['agreement_number'] = $agreement['agreement_number'];
            $summaries[] = $summary;
        }

        $aggregate['count'] = count($summaries);
        $aggregate['total'] = array_sum(array_map(function ($summary) {
            return $summary['total_outstanding'];
        }, $summaries));

        return view('customer.credit.all')->with(compact('aggregate', 'summaries'));
    }

    public function showTransaction(Request $request, $agreement = null)
    {
        $agreements = api_get_agreements(auth()->user()->ktp);
        $transactions = isset($agreement) ? api_get_transactions($agreement) : collect([]);

        return view('customer.credit.transaction')->with(compact('agreement', 'agreements', 'transactions'));
    }

    public function showActivity()
    {
        $activities = auth()->user()->activities;

        return view('customer.credit.activity')->with(compact('activities'));
    }

    public function detailActivity(Activity $activity)
    {
        if ($activity->user->id != auth()->user()->id) abort(403);

        return view('customer.credit.activity-detail')->with(compact('activity'));
    }

    public function showStatus()
    {
        $agreements = api_get_agreements(auth()->user()->ktp, false);

        return view('customer.credit.status')->with(compact('agreements'));
    }
}
