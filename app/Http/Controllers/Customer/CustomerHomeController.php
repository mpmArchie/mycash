<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerHomeController extends Controller
{
    public function index()
    {
        $agreements = api_get_agreements(auth()->user()->ktp);
        $summaries = [];
        foreach ($agreements as $agreement)
        {
            $summary = api_get_summary($agreement['agreement_number']);
            $summary['agreement_number'] = $agreement['agreement_number'];
            $summaries[] = $summary;
        }

        $requests = [];
        if (count($agreements) == 0)
        {
            $requests = api_get_agreements(auth()->user()->ktp, false);
        }

        return view('customer.home')->with(compact('summaries', 'requests'));
    }
}
