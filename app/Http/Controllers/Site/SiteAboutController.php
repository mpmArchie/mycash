<?php

namespace App\Http\Controllers\Site;

use App\Models\Setting;
use App\Models\Product;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Meta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteAboutController extends Controller
{
    public function index()
    {
        $banners = Banner::where('page_id', Page::ABOUT)->get();
        $encouragementWords = Setting::where('key', 'encouragement_words')->first()->value;
        $products = Product::all();
        $metas = Meta::whereHas('pages', function ($query) {
            $query->where('id', Page::ABOUT);
        })->get();

        return view('site.about')->with(compact('banners', 'encouragementWords', 'products', 'metas'));
    }
}
