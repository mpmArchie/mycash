<?php

namespace App\Http\Controllers\Site;

use App\Models\Page;
use App\Models\Meta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteArticleController extends Controller
{
    public function index()
    {
        $metas = Meta::whereHas('pages', function ($query) {
            $query->where('id', Page::ARTICLE);
        })->get();
        $articles = api_get_article();

        return view('site.article')->with(compact('metas', 'articles'));
    }

    public function detail(Request $request, $id)
    {
        $articles = array_slice(api_get_article(), 0, 4);
        $article = api_get_article($id);

        return view('site.article-detail')->with(compact('article', 'articles'));
    }
}
