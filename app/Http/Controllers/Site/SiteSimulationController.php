<?php

namespace App\Http\Controllers\Site;

use App\Models\Branch;
use App\Models\Simulation;
use App\Models\Installment;
use App\Models\Calculation;
use App\Events\CalculationCreated;
use App\Events\CalculationVerificationResent;
use App\Events\LoanRequested;
use App\Events\ActivityTriggered;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteSimulationController extends Controller
{
    public function calculate(Request $request)
    {
        $data = $request->validate(array_only($this->validationRules(), ['simulation', 'year', 'total', 'installment']), $this->validationErrorMessages());

        return redirect()->route('simulation.index')->withInput($data);
    }

    public function index(Request $request)
    {
        if ($request->old('simulation')) {
            $data = $request->old();

            $information = [];
            $information['name'] = Simulation::find($data['simulation'])->name;
            $information['year'] = $data['year'];
            $information['total'] = str_currency($data['total']);
            $information['installment'] = Installment::find($data['installment'])->label_duration;

            $cities = api_get_address_references();

            return view('site.simulation')->with(compact('information', 'cities'));
        }

        abort(404);
    }

    public function create(Request $request, Calculation $calculation)
    {
        $isAreaMandatory = $request->input('is-area-mandatory');
        if ($isAreaMandatory) {
            $rules = $this->validationRules();
            array_set($rules, 'area', 'required');
        } else {
            $rules = $this->validationRules();
        }
        $attributes = $request->validate($rules, $this->validationErrorMessages());

        $success = $calculation->add($attributes);
        if ($success)
        {
            event(new CalculationCreated($success));
            return redirect()->route('simulation.verification', ['calculation' => $success['id']]);
        }

        return back()->withErrors($attributes);
    }

    public function verification(Request $request, Calculation $calculation)
    {
        if ($calculation->isVerified())
        {
            return redirect()->route('simulation.result', ['calculation' => $calculation->id]);
        }

        return view('site.simulation-verification')->with(compact('calculation'));
    }

    public function verify(Request $request, Calculation $calculation)
    {
        $attributes = $request->validate(['verification_code' => 'required'], ['verification_code.required' => 'Kode verifikasi tidak boleh kosong']);

        $success = $calculation->verify($attributes);
        if ($success)
        {
            return redirect()->route('simulation.result', ['calculation' => $calculation->id]);
        }
        else
        {
            return back()->withErrors(['verification_code' => 'Kode verifikasi tidak sesuai']);
        }

        return back()->withErrors($attributes);
    }

    public function resend(Request $request, Calculation $calculation)
    {
        $success = $calculation->resend();
        event(new CalculationVerificationResent($success));

        return back()->with('status', 'Silakan periksa kode verifikasi yang sudah dikirimkan ke nomor ponsel Anda');
    }

    public function result(Request $request, Calculation $calculation)
    {
        if ($calculation->isVerified())
        {
            $installment = $calculation->installment;
            $result = $calculation->result();
            return view('site.simulation-result')->with(compact('calculation', 'installment', 'result'));
        }

        return redirect()->route('simulation.verification', ['calculation' => $calculation->id]);
    }

    public function request(Request $request, Calculation $calculation)
    {
        if ($calculation->isRequested())
        {
            return back()->with('status-error', 'Pengajuan pinjaman telah dilakukan. Silakan cek email Anda untuk masuk ke dasbor dan melihat status pengajuan');
        }

        $password = $calculation->request();
        event(new LoanRequested($calculation, $password));
        event(new ActivityTriggered($calculation->toActivity()));

        return back()->with('status', 'Tim kami akan menghubungi Anda untuk melanjutkan proses peminjaman');
    }

    public function validationRules()
    {
        return [
            'simulation' => 'required',
            'year' => 'required',
            'total' => 'required|numeric',
            'installment' => 'required',
            'area' => 'nullable',
            'name' => 'required',
            'ktp' => 'required',
            'phone' => 'required|numeric|starts_with:0',
            'email' => 'required|email',
            'address' => 'required',
            'rt' => 'required|numeric',
            'rw' => 'required|numeric',
            'city' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'postcode' => 'required',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'simulation.required' => 'Jenis agunan/produk tidak boleh kosong',
            'year.required' => 'Tahun kendaraan tidak boleh kosong',
            'total.required' => 'Total pembiayaan tidak boleh kosong',
            'total.numeric' => 'Total harus berupa angka',
            'installment.required' => 'Jangka waktu tidak boleh kosong',
            'area.required' => 'Cabang tidak boleh kosong',
            'name.required' => 'Nama tidak boleh kosong',
            'ktp.required' => 'Nomor KTP tidak boleh kosong',
            'phone.required' => 'Nomor ponsel tidak boleh kosong',
            'phone.numeric' => 'Nomor ponsel harus berupa angka',
            'phone.starts_with' => 'Nomor ponsel harus diawali dangan angka nol',
            'email.required' => 'Alamat email tidak boleh kosong',
            'email.email' => 'Alamat email tidak valid',
            'address.required' => 'Alamat domisili tidak boleh kosong',
            'rt.required' => 'RT tidak boleh kosong',
            'rt.numeric' => 'RT harus berupa angka',
            'rw.required' => 'RW tidak boleh kosong',
            'rw.numeric' => 'RW harus berupa angka',
            'city.required' => 'Kota tidak boleh kosong',
            'kecamatan.required' => 'Kecamatan tidak boleh kosong',
            'kelurahan.required' => 'Kelurahan tidak boleh kosong',
            'postcode.required' => 'Kode pos tidak boleh kosong',
        ];
    }
}
