<?php

namespace App\Http\Controllers\Site;

use App\Models\Setting;
use App\Models\Product;
use App\Models\Promo;
use App\Models\Partner;
use App\Models\Page;
use App\Models\Meta;
use App\Models\Simulation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteProductController extends Controller
{
    public function index()
    {
        $encouragementWords = Setting::where('key', 'encouragement_words')->first()->value;
        $products = Product::all();
        $promos = Promo::all();
        $partners = Partner::all();
        $metas = Meta::whereHas('pages', function ($query) {
            $query->where('id', Page::PRODUCT);
        })->get();
        $simulations = Simulation::with(['installments' => function ($query) {
            return $query->select('id', 'duration')->orderBy('duration');
        }])->select('id', 'code', 'name', 'start_year', 'end_year')
           ->get();

        return view('site.product')->with(compact('encouragementWords', 'products', 'promos', 'partners', 'metas', 'simulations'));
    }

    public function detail(Request $request, $slug)
    {
        $product;
        $products = Product::all();
        foreach ($products as $currProduct)
        {
            if ($currProduct->slug == $slug)
            {
                $product = $currProduct;
                break;
            }
        }

        if (isset($product))
        {
            $simulations = Simulation::with(['installments' => function ($query) {
                return $query->select('id', 'duration')->orderBy('duration');
            }])->select('id', 'code', 'name', 'start_year', 'end_year')
               ->get();

            return view('site.product-detail')->with(compact('product', 'simulations'));
        }

        abort(404);
    }
}
