<?php

namespace App\Http\Controllers\Site;

use App\Models\Promo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SitePromoController extends Controller
{
    public function detail(Request $request, $slug)
    {
        $promo;
        $promos = Promo::all();
        foreach ($promos as $currPromo)
        {
            if ($currPromo->slug == $slug)
            {
                $promo = $currPromo;
                break;
            }
        }

        if (isset($promo))
        {
            return view('site.promo-detail')->with(compact('promo'));
        }

        abort(404);
    }
}
