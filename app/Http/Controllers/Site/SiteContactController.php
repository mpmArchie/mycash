<?php

namespace App\Http\Controllers\Site;

use App\Models\Contact;
use App\Models\Page;
use App\Models\Meta;
use App\Events\Contacted;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteContactController extends Controller
{
    public function index()
    {
        $metas = Meta::whereHas('pages', function ($query) {
            $query->where('id', Page::CONTACT);
        })->get();

        return view('site.contact')->with(compact('metas'));
    }

    public function submit(Request $request, Contact $contact)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $inputs = array_except($attributes, ['g-recaptcha-response']);
        $success = $contact->add($inputs);
        if ($success)
        {
            event(new Contacted($attributes));

            return redirect()->route('contact')->with('status', 'Terima kasih sudah menghubungi kami');
        }

        return back()->withErrors($attributes);
    }

    public function validationRules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format email tidak valid',
            'phone.required' => 'Nomor telepon tidak boleh kosong',
            'message.required' => 'Pesan tidak boleh kosong',
            'g-recaptcha-response.required' => 'Lakukan validasi dengan captcha',
            'g-recaptcha-response.recaptcha' => 'Captcha salah',
        ];
    }
}
