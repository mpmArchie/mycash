<?php

namespace App\Http\Controllers\Site;

use App\Models\Setting;
use App\Models\Product;
use App\Models\Banner;
use App\Models\Promo;
use App\Models\Testimony;
use App\Models\Page;
use App\Models\Subscriber;
use App\Models\Meta;
use App\Models\Simulation;
use App\Events\Subscribed;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteHomeController extends Controller
{
    public function index()
    {
        $banners = Banner::where('page_id', Page::HOME)->get();
        $encouragementWords = Setting::where('key', 'encouragement_words')->firstOrFail()->value;
        $products = Product::all();
        $promos = Promo::all();
        $testimonies = Testimony::all();
        $metas = Meta::whereHas('pages', function ($query) {
            $query->where('id', Page::HOME);
        })->get();
        $simulations = Simulation::with(['installments' => function ($query) {
            return $query->select('id', 'duration')->orderBy('duration');
        }])->select('id', 'code', 'name', 'start_year', 'end_year')
           ->get();

        return view('site.home')->with(compact('banners', 'encouragementWords', 'products', 'promos', 'testimonies', 'metas', 'simulations'));
    }

    public function subscribe(Request $request, Subscriber $subscriber)
    {
        $attributes = $request->validate($this->validationRules(), $this->validationErrorMessages());

        $success = $subscriber->add($attributes);
        if ($success)
        {
            event(new Subscribed($attributes));
            return redirect()->route('home')->with('status', 'Terima kasih sudah menjadi pelanggan newsletter kami');
        }

        return back()->withErrors($attributes);
    }

    public function validationRules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function validationErrorMessages()
    {
        return [
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format email tidak valid',
        ];
    }
}
