<?php

namespace App\Http\Controllers\Site;

use App\Models\Setting;
use App\Models\Promo;
use App\Models\Payment;
use App\Models\Faq;
use App\Models\Page;
use App\Models\Meta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteInfoController extends Controller
{
    public function index()
    {
        $promos = Promo::all();
        $payments = Payment::all()->sortBy('ordering');
        $metas = Meta::whereHas('pages', function ($query) {
            $query->where('id', Page::INFO);
        })->get();

        return view('site.info')->with(compact('promos', 'payments', 'metas'));
    }

    public function terms()
    {
        $termsConditions = Setting::where('key', 'terms_conditions')->firstOrFail()->value;

        return view('site.terms')->with(compact('termsConditions'));
    }

    public function faq(Request $request)
    {
        $faqs = Faq::all()->sortBy('ordering');

        if ($request->has('search'))
        {
            $faqs = Faq::where('question', 'like', '%' . $request->input('search') . '%')
                        ->orWhere('answer', 'like', '%' . $request->input('search') . '%')
                        ->get()
                        ->sortBy('ordering');
        }

        return view('site.faq')->with(compact('faqs'));
    }
}
