<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClaimResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->resource['CustomerName'],
            'brand' => $this->resource['NamaAset'],
            'license_plate' => $this->resource['LicensePlate'],
            'insco' => $this->resource['Description'],
            'insco_phone' => $this->resource['Phone'],
        ];
    }
}
