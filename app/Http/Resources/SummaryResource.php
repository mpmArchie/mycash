<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SummaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->resource['CustomerName'],
            'brand' => $this->resource['Asset'],
            'type' => $this->resource['jenisKendaraan'],
            'license_plate' => $this->resource['LicensePlate'],
            'year' => $this->resource['ManufacturingYear'],
            'total' => $this->resource['NTF'],
            'total_outstanding' => $this->resource['TotalOutstandingInstallment'],
            'installment_amount' => $this->resource['InstallmentAmount'],
            'due_date' => trim($this->resource['NextInstallmentDate']),
            'branch_id' => $this->resource['branchid'],
            'branch_name' => $this->resource['BranchFullName'],
            'topup_enable' => $this->resource['abletoTopup'],
        ];
    }
}
