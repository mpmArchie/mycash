<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'request_status_id' => $this->resource['status_id'],
            'request_number' => $this->resource['mcrid'],
            'agreement_number' => trim($this->resource['AgreementNo']),
            'type' => $this->resource['tipe_kendaraan'],
            'created_at' => $this->resource['created_at'],
        ];
    }
}
