<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'installment' => $this->resource['InsSeqNo'],
            'installment_amount' => $this->resource['InstallmentAmount'],
            'installment_charge' => $this->resource['LateCharges'],
            'transaction_date' => $this->resource['PaidDate'],
            'payment_method' => $this->resource['WOP'],
            'total' => $this->resource['NTF'],
            'total_outstanding' => $this->resource['TotalOutstandingInstallment'],
            'due_date' => $this->resource['DueDate'],
        ];
    }
}
