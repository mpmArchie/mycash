<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressReferenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'city' => $this->resource['city'],
            'kecamatan' => $this->when(array_has($this->resource, 'kecamatan'), array_get($this->resource, 'kecamatan')),
            'kelurahan' => $this->when(array_has($this->resource, 'Kelurahan'), array_get($this->resource, 'Kelurahan')),
            'postcode' => $this->when(array_has($this->resource, 'ZipCode'), array_get($this->resource, 'ZipCode')),
        ];
    }
}
