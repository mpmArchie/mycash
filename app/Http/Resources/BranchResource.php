<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => $this->resource['BranchID'],
            'name' => $this->resource['BranchFullName'],
            'email' => $this->resource['emailbo'],
            'type' => $this->resource['agunan_id'],
        ];
    }
}
