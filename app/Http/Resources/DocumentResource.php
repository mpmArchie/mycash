<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->resource['CustomerName'],
            'brand' => $this->resource['AssetCode'],
            'license_plate' => $this->resource['LicensePlate'],
            'branch' => $this->resource['BranchFullName'],
            'release_enable' => $this->resource['releaseBPKB'],
        ];
    }
}
