<?php

namespace App\Http\Traits;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

trait GuzzleTrait {

    public function getBaseUrl()
    {
        return 'http://localhost';
    }

    public function getUrl($path = '')
    {
        return $this->getBaseUrl() . $path;
    }

    public function request($method, $url, $options = [])
    {
        $client = new Client();
        return $client->request($method, $url, $options);
    }

    public function get($url, $options = [])
    {
        return $this->request('GET', $url, $options);
    }

    public function post($url, $options = [])
    {
        return $this->request('POST', $url, $options);
    }

    public function put($url, $options = [])
    {
        return $this->request('PUT', $url, $options);
    }

    public function delete($url, $options = [])
    {
        return $this->request('DELETE', $url, $options);
    }
}
