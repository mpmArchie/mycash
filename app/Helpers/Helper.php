<?php

use App\Helpers\LogHelper;
use App\Helpers\ResponseHelper;
use App\Helpers\ApiHelper;
use App\Helpers\StringHelper;

if (!function_exists('log_error'))
{
    function log_error($error)
    {
        return LogHelper::log_error($error);
    }
}

if (!function_exists('log_message'))
{
    function log_message($error)
    {
        return LogHelper::log_message($error);
    }
}

if (!function_exists('response_success'))
{
    function response_success($message)
    {
        return ResponseHelper::response_success($message);
    }
}

if (!function_exists('response_error'))
{
    function response_error($message)
    {
        return ResponseHelper::response_error($message);
    }
}

if (!function_exists('str_limit_html'))
{
    function str_limit_html($string, $limit, $end = "...")
    {
        return StringHelper::str_limit_html($string, $limit, $end);
    }
}

if (!function_exists('str_currency'))
{
    function str_currency($number, $symbol = true)
    {
        return StringHelper::str_currency($number, $symbol);
    }
}

if (!function_exists('str_remove_protocol'))
{
    function str_remove_protocol($url)
    {
        return StringHelper::str_remove_protocol($url);
    }
}

if (!function_exists('api_get_agreements'))
{
    function api_get_agreements($ktp, $active = true)
    {
        return ApiHelper::api_get_agreements($ktp, $active);
    }
}

if (!function_exists('api_get_transactions'))
{
    function api_get_transactions($agreementNumber)
    {
        return ApiHelper::api_get_transactions($agreementNumber);
    }
}

if (!function_exists('api_get_summary'))
{
    function api_get_summary($agreementNumber)
    {
        return ApiHelper::api_get_summary($agreementNumber);
    }
}

if (!function_exists('api_get_claim'))
{
    function api_get_claim($claimType, $agreementNumber)
    {
        return ApiHelper::api_get_claim($claimType, $agreementNumber);
    }
}

if (!function_exists('api_get_document'))
{
    function api_get_document($agreementNumber)
    {
        return ApiHelper::api_get_document($agreementNumber);
    }
}

if (!function_exists('api_get_branches'))
{
    function api_get_branches($postcode = null)
    {
        return ApiHelper::api_get_branches($postcode);
    }
}

if (!function_exists('api_get_article'))
{
    function api_get_article($id = null)
    {
        return ApiHelper::api_get_article($id);
    }
}

if (!function_exists('api_get_address_references'))
{
    function api_get_address_references($city = 'Nan', $kecamatan = 'Nan', $kelurahan = 'Nan')
    {
        return ApiHelper::api_get_address_references($city, $kecamatan, $kelurahan);
    }
}

if (!function_exists('api_send_loan'))
{
    function api_send_loan($data = [])
    {
        return ApiHelper::api_send_loan($data);
    }
}

if (!function_exists('api_send_message'))
{
    function api_send_message($number, $message)
    {
        return ApiHelper::api_send_message($number, $message);
    }
}
