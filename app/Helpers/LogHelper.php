<?php

namespace App\Helpers;

use App\Models\Log;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class LogHelper
{
    public static function log_error(RequestException $error)
    {
        $request = $error->getRequest();
        $response = $error->getResponse();
        $code = $response->getStatusCode();
        $message = $response->getReasonPhrase();
        $data = [
            'code' => $code,
            'message' => $message,
            'request' => Psr7\str($request),
            'response' => Psr7\str($response),
        ];

        $log = new Log;
        $log->add($data);

        if (config('app.env') != 'production')
        {
            throw $error;
        }
    }

    public static function log_message(RequestException $error)
    {
        $response = $error->getResponse();
        return $response->getStatusCode() . ' - ' . $response->getReasonPhrase();
    }
}
