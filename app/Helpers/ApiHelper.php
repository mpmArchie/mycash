<?php

namespace App\Helpers;

use App\Repositories\ApiRepository;

class ApiHelper
{
    public static function api_get_agreements($ktp, $active = true)
    {
        $api = new ApiRepository;
        return $api->getAgreements($ktp, $active);
    }

    public static function api_get_transactions($agreementNumber)
    {
        $api = new ApiRepository;
        return $api->getTransactions($agreementNumber);
    }

    public static function api_get_summary($agreementNumber)
    {
        $api = new ApiRepository;
        return $api->getSummary($agreementNumber);
    }

    public static function api_get_claim($claimType, $agreementNumber)
    {
        $api = new ApiRepository;
        return $api->getClaim($claimType, $agreementNumber);
    }

    public static function api_get_document($agreementNumber)
    {
        $api = new ApiRepository;
        return $api->getDocument($agreementNumber);
    }

    public static function api_get_branches($postcode = null)
    {
        $api = new ApiRepository;
        return $api->getBranches($postcode);
    }

    public static function api_get_article($id = null)
    {
        $api = new ApiRepository;
        if (isset($id))
        {
            return $api->getArticle($id);
        }
        return $api->getArticles();
    }

    public static function api_get_address_references($city = 'Nan', $kecamatan = 'Nan', $kelurahan = 'Nan')
    {
        $api = new ApiRepository;
        return $api->getAddressReferences($city, $kecamatan, $kelurahan);
    }

    public static function api_send_loan($data = [])
    {
        $api = new ApiRepository;
        return $api->sendLoan($data);
    }

    public static function api_send_message($number, $message)
    {
        $api = new ApiRepository;
        return $api->sendMessage($number, $message);
    }
}
