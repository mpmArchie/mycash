<?php

namespace App\Helpers;

class ResponseHelper
{
    public static function response_success($message)
    {
        return [
            'status' => 'SUCCESS',
            'message' => $message,
        ];
    }

    public static function response_error($message)
    {
        return [
            'status' => 'ERROR',
            'message' => $message,
        ];
    }
}
