<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class StringHelper
{
    public static function str_limit_html($string, $limit, $end = "...")
    {
        if(strlen($string) <= $limit) {
            return $string;
        }

        $truncatedText = substr($string, $limit);
        $pos = strpos($truncatedText, ">");
        if($pos !== false)
        {
            $string = substr($string, 0, $limit + $pos + 1);
        }
        else
        {
            $string = substr($string, 0, $limit);
        }

        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $string, $result);
        $openedtags = $result[1];

        preg_match_all('#</([a-z]+)>#iU', $string, $result);
        $closedtags = $result[1];

        $len_opened = count($openedtags);

        $string .= $end;

        if (count($closedtags) == $len_opened)
        {
            return $string;
        }

        $openedtags = array_reverse($openedtags);
        for ($i=0; $i < $len_opened; $i++)
        {
            if (!in_array($openedtags[$i], $closedtags))
            {
                $string .= '</'.$openedtags[$i].'>';
            }
            else
            {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }


        return $string;
    }

    public static function str_currency($number, $symbol = true)
    {
        if ($symbol) return 'Rp ' . number_format($number, 0, ',', '.');

        return number_format($number, 0, ',', '.');
    }

    public static function str_remove_protocol($url)
    {
        // Based on https://stackoverflow.com/a/4357691/5556880
        $disallowed = array('http://', 'https://');
        foreach($disallowed as $d) {
            if(strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }
        return $url;
    }
}
