<?php

namespace App\Repositories;

use App\Http\Traits\GuzzleTrait;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\SummaryResource;
use App\Http\Resources\ClaimResource;
use App\Http\Resources\ContractResource;
use App\Http\Resources\DocumentResource;
use App\Http\Resources\BranchResource;
use App\Http\Resources\ArticleResource;
use App\Http\Resources\AddressReferenceResource;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp;

class ApiRepository
{
    use GuzzleTrait;

    const GET_TRANSACTIONS = '/GetMobilePaymentHistoryInfo';
    const GET_SUMMARY = '/GetMobilePaymentHistoryHeaderInfo';
    const GET_CLAIM = '/GetMobileInsuranceInfo';
    const GET_CONTRACTS = '/getlistcontractbynik';
    const GET_DOCUMENT = '/GetMobileBPKBInfo';
    const GET_BRANCHES = '/getlistbranch';
    const GET_BRANCHES_BY_POSTCODE = '/getlistbranchbyZIP';
    const GET_ARTICLES = '/GetlistBerita';
    const GET_ARTICLE = '/Getdetailberita';
    const GET_ADDRESS_REFERENCES = '/GetlistAdress';
    const SEND_LOAN = '/InsertWeborder/Register.json';
    const SEND_MESSAGE = '/sendSMS';

    private function getStatusName($id)
    {
        switch ($id) {
            case 1:
                return 'Kredit Diajukan';
            case 2:
                return 'Pemeriksaan oleh Petugas';
            case 3:
                return 'Pemeriksaan Selesai';
            case 4:
                return 'Permohonan Diajukan';
            case 5:
                return 'Kontrak Aktif';
            default:
                return 'Pengajuan Ditolak';
        }
    }

    public function getBaseUrl()
    {
        return config('setting.api.url');
    }

    public function getAgreements($ktp, $active = true)
    {
        $url = $this->getUrl(ApiRepository::GET_CONTRACTS . '/' . $ktp);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [];
        }

        $raw = ContractResource::collection(collect($data))->resolve();

        $result = [];
        foreach ($raw as $curr) {
            $item = array_add($curr, 'request_status_name', $this->getStatusName($curr['request_status_id']));
            $result[] = $item;
        }

        if ($active)
        {
            $finalResult = array_where($result, function ($value) {
                return $value['request_status_id'] == 5; // Active contract
            });
        }
        else
        {
            $finalResult = $result;
        }

        return collect($finalResult);
    }

    public function getTransactions($agreementNumber)
    {
        $url = $this->getUrl(ApiRepository::GET_TRANSACTIONS . '/' . $agreementNumber);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [];
        }

        return TransactionResource::collection(collect($data))->resolve();
    }

    public function getSummary($agreementNumber)
    {
        $url = $this->getUrl(ApiRepository::GET_SUMMARY . '/' . $agreementNumber);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = null;
        }

        return SummaryResource::make($data)->resolve();
    }

    public function getClaim($claimType, $agreementNumber)
    {
        $url = $this->getUrl(ApiRepository::GET_CLAIM . '/' . $agreementNumber . '/' . $claimType);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = null;
        }

        return ClaimResource::make($data)->resolve();
    }

    public function getDocument($agreementNumber)
    {
        $url = $this->getUrl(ApiRepository::GET_DOCUMENT . '/' . $agreementNumber);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = null;
        }

        return DocumentResource::make($data)->resolve();
    }

    public function getBranches($postcode = null)
    {
        $url = $this->getUrl($postcode ? ApiRepository::GET_BRANCHES_BY_POSTCODE . '/' . $postcode : ApiRepository::GET_BRANCHES);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [];
        }

        return BranchResource::collection(collect($data))->resolve();
    }

    public function getArticles()
    {
        $url = $this->getUrl(ApiRepository::GET_ARTICLES);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [];
        }

        return ArticleResource::collection(collect($data))->resolve();
    }

    public function getArticle($id = null)
    {
        $url = $this->getUrl(ApiRepository::GET_ARTICLE . '/' . $id);

        try {
            $response = $this->get($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [];
        }

        return ArticleResource::make(head($data))->resolve();
    }

    public function getAddressReferences($city = 'Nan', $kecamatan = 'Nan', $kelurahan = 'Nan')
    {
        $url = $this->getUrl(ApiRepository::GET_ADDRESS_REFERENCES . '/' . $city . '/' . $kecamatan . '/' . $kelurahan);

        try {
            $response = $this->post($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [];
        }

        return AddressReferenceResource::collection(collect($data))->resolve();
    }

    public function sendLoan($params = [])
    {
        $url = $this->getUrl(ApiRepository::SEND_LOAN);
        $options = [GuzzleHttp\RequestOptions::BODY => json_encode($params)];

        try {
            $response = $this->post($url, $options);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [
                'code' => '100',
                'message' => log_message($e),
            ];
        }

        if ($data['code'] == '200')
        {
            return response_success($data['message']);
        }

        return response_error($data['message']);
    }

    public function sendMessage($number, $message)
    {
        $url = $this->getUrl(ApiRepository::SEND_MESSAGE . '/' . $number . '/' . $message);

        try {
            $response = $this->post($url);
            $data = json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            log_error($e);
            $data = [
                'sendSMSResult' => 'ERROR: ' . log_message($e),
            ];
        }

        if (str_contains($data['sendSMSResult'], 'SUCCESS'))
        {
            return response_success($data['sendSMSResult']);
        }

        return response_error($data['sendSMSResult']);
    }
}
