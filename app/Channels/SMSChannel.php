<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;

class SMSChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $phone = $notifiable->routes[SMSChannel::class];
        $message = $notification->toSMS($notifiable);

        api_send_message($phone, $message);
    }
}
