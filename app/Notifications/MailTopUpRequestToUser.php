<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use Carbon\Carbon;

class MailTopUpRequestToUser extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pengajuan Top Up Pinjaman MyCash Sedang Diproses')
                    ->greeting('Halo ' . $this->data['name'] . ',')
                    ->line('Pengajuan to up pinjaman MyCash Anda berhasil dan saat ini sedang diproses oleh tim terkait.')
                    ->line(new HtmlString('Berikut detail dari pengajuan Anda:<br/>Waktu Pengajuan: <b>' . Carbon::parse($this->data['created_at'])->setTimezone('Asia/Jakarta')->locale('id')->isoFormat('D MMM YYYY, HH:mm') . '</b><br/>Jenis Pengajuan: <b>Top Up</b><br/>Nomor Kontrak: <b>' . $this->data['agreement_number'] . '</b><br/>Nama: <b>' . $this->data['name'] . '</b><br/>Jenis Kendaraan: <b>' . $this->data['type'] . '</b><br/>Unit/Brand: <b>' . $this->data['brand'] . '</b><br/>Nomor Plat: <b>' . $this->data['license_plate'] . '</b><br/>Tahun Kendaraan: <b>' . $this->data['year'] . '</b><br/>Nilai Pembiayaan: <b>' . $this->data['total'] . '</b><br/>Sisa Hutang: <b>' . $this->data['total_outstanding'] . '</b><br/>Jumlah Diterima: <b>' . $this->data['received'] . '</b>'))
                    ->line(new HtmlString('Tim MPM Finance akan menghubungi Anda untuk proses selanjutnya. Pantau status top up Anda melalui <a href="' . url('customer') . '">' . str_remove_protocol(url('')) . '</a>'))
                    ->line('Demikian kami sampaikan.')
                    ->line(new HtmlString('Ada pertanyaan?<br/>Silakan tanyakan pertanyaan Anda melalui <a href="' . url('contact') . '">' . str_remove_protocol(url('')) . '</a>'))
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
