<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class MailNewCustomer extends Notification implements ShouldQueue
{
    use Queueable;

    public $name;
    public $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($name, $password)
    {
        $this->name = $name;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Password Login Dasbor MyCash')
                    ->greeting('Halo ' . $this->name . ',')
                    ->line('Selamat datang di MyCash, solusi finansial terbaik untuk Anda. Untuk masuk ke dasbor MyCash silakan login menggunakan email Anda dan masukkan kata sandi dibawah ini:')
                    ->line(new HtmlString('<b>' . $this->password . '</b>'))
                    ->line('Silakan pantau akun debitur Anda untuk produk MyCash di')
                    ->action('Masuk Dasbor MyCash', url('login'))
                    ->line('Silakan ganti kata sandi Anda melalui menu Pengaturan Akun.')
                    ->line(new HtmlString('Ada pertanyaan?<br/>Silakan tanyakan pertanyaan Anda melalui <a href="' . url('contact') . '">' . str_remove_protocol(url('')) . '</a>'))
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
