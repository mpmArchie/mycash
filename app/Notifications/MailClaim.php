<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class MailClaim extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pelanggan Mengajukan Klaim Asuransi')
                    ->greeting('Halo Tim MPM Finance,')
                    ->line(new HtmlString('Pelanggan dengan nomor kontrak <b>' . $this->data['agreement_number'] . '</b> ingin mengajukan klaim asuransi dengan detail:<br/>'))
                    ->line(new HtmlString('Jenis Klaim: <b>' . $this->data['claim_type'] . '</b><br/>Tanggal Kejadian: <b> ' . $this->data['date'] . ' </b><br/>Lokasi Kejadian: <b>' . $this->data['location'] . '</b><br/> Detail Kejadian:<br/><b>' . $this->data['detail'] . '</b>'))
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
