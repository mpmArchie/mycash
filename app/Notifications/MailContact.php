<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class MailContact extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pelanggan Menghubungi Tim MPM Finance')
                    ->greeting('Halo Tim MPM Finance,')
                    ->line('Seorang pelanggan mencoba menghubungi Anda melalui form "Hubungi Kami" pada website MyCash.')
                    ->line(new HtmlString('Data yang dikirimkan:<br/>Nama: <b>' . $this->data['name'] . '</b><br/>Email: <b> ' . $this->data['email'] . ' </b><br/>Nomor Telepon: <b>' . $this->data['phone'] . '</b><br/> Pesan: <br/><b>' . $this->data['message'] . '</b>'))
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
