<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use Carbon\Carbon;

class MailRequestLoan extends Notification implements ShouldQueue
{
    use Queueable;

    public $calculation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($calculation)
    {
        $this->calculation = $calculation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pengajuan Pinjaman MyCash Telah Berhasil Dikirim')
                    ->greeting('Halo ' . $this->calculation->name . ',')
                    ->line('Pengajuan pinjaman MyCash berhasil diajukan dan saat ini sedang diproses.')
                    ->line(new HtmlString('Berikut detail dari pangajuan Anda:<br/>Waktu Pengajuan: <b>' . Carbon::parse($this->calculation->requested_at)->setTimezone('Asia/Jakarta')->locale('id')->isoFormat('D MMM YYYY, HH:mm') . '</b><br/>Jenis Agunan/Produk: <b>' . $this->calculation->simulation->name . '</b><br/>Tahun Kendaraan: <b>' . $this->calculation->year . '</b><br/>Total Pembiayaan: <b>' . str_currency($this->calculation->total) . '</b><br/>Jangka Waktu: <b>' . $this->calculation->installment->label_duration . '</b><br/>Area Pengajuan: <b>' . $this->calculation->branch->name . '</b>'))
                    ->line(new HtmlString('Dengan keterangan pengaju:<br/>Nama Sesuai KTP: <b>' . $this->calculation->name . '</b><br/>Nomor KTP: <b>' . $this->calculation->ktp . '</b><br/>Nomor Ponsel: <b>' . $this->calculation->phone . '</b><br/>Email: <b> ' . $this->calculation->email . ' </b><br/>Alamat: <br/><b>' . $this->calculation->address . ' RT' . $this->calculation->rt . '/RW' . $this->calculation->rw . ', Kelurahan ' . $this->calculation->kelurahan . ', Kecamatan ' . $this->calculation->kecamatan . ', Kota ' . $this->calculation->city . ', ' . $this->calculation->postcode . '</b>'))
                    ->line('Tim MPM Finance akan menghubungi Anda untuk proses selanjutnya.')
                    ->line('Silakan pantau akun debitur Anda untuk produk MyCash di')
                    ->action('Masuk Dasbor MyCash', url('login'))
                    ->line('Demikian kami sampaikan.')
                    ->line(new HtmlString('Ada pertanyaan?<br/>Silakan tanyakan pertanyaan Anda melalui <a href="' . url('contact') . '">' . str_remove_protocol(url('')) . '</a>'))
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
