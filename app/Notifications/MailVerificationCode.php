<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class MailVerificationCode extends Notification implements ShouldQueue
{
    use Queueable;

    public $calculation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($calculation)
    {
        $this->calculation = $calculation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Kode Verifikasi Simulasi Kredit')
                    ->greeting('Halo ' . $this->calculation->name . ',')
                    ->line('Terima kasih sudah melakukan simulasi kredit. Gunakan kode dibawah ini untuk melihat hasil penghitungan')
                    ->line(new HtmlString('<b>' . $this->calculation->verification_code . '</b>'))
                    ->line('Demikian kami sampaikan.')
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
