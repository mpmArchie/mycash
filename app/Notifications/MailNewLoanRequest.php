<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class MailNewLoanRequest extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pelanggan Mengajukan Pinjaman Baru')
                    ->greeting('Halo Tim MPM Finance,')
                    ->line(new HtmlString('Pelanggan dengan nama <b>' . $this->data['name'] . '</b> ingin mengajukan pinjaman baru dengan detail:'))
                    ->line(new HtmlString('Email: <b> ' . $this->data['email'] . ' </b><br/>Nomor KTP: <b>' . $this->data['ktp'] . '</b><br/>Jenis Kendaraan: <b>' . $this->data['type'] . '</b><br/>Unit / Brand: <b>' . $this->data['brand'] . '</b><br/>Tahun Kendaraan: <b>' . $this->data['year'] . '</b><br/>Nomor Plat: <b>' . $this->data['license_plate'] . '</b><br/>Total Pembiayaan: <b>' . str_currency($this->data['total']) . '</b><br/>Jangka Waktu: <b>' . $this->data['installment'] . '</b>'))
                    ->salutation('Salam');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
