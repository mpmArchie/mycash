function onUpdateEncouragementWords() {
    $('#___product-words').hide();
    $('#___product-words-form').show();
}

function onDeleteProduct(id, deleteUrl) {
    swal({
        title: 'Yakin Ingin Menghapus?',
        text: 'Data akan dihapus. Aksi ini tidak dapat dikembalikan',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Batalkan',
        confirmButtonClass: 'btn btn-warning',
        confirmButtonText: 'Ya, Hapus',
        showLoaderOnConfirm: true,
        preConfirm: () => {
            return fetch(deleteUrl, {
                method: 'DELETE',
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
                },
            }).then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            }).catch(error => {
                swal.showValidationMessage('Gagal menghapus data. Silakan ulang lagi')
            });
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then(result => {
        if (result.value) {
            $(`#product-${id}`).remove();

            swal({
                title: 'Dihapus!',
                text: 'Data telah sukses dihapus dari sistem',
                type: 'success',
                confirmButtonClass: 'btn btn-success'
            });
        }
    });
}

$(document).ready(function () {
    $('#___product-words-form').hide();
});
