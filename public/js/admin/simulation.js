function onDeleteInstallment(id, deleteUrl) {
    swal({
        title: 'Yakin Ingin Menghapus?',
        text: 'Data akan dihapus. Aksi ini tidak dapat dikembalikan',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Batalkan',
        confirmButtonClass: 'btn btn-warning',
        confirmButtonText: 'Ya, Hapus',
        showLoaderOnConfirm: true,
        preConfirm: () => {
            return fetch(deleteUrl, {
                method: 'DELETE',
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
                },
            }).then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            }).catch(error => {
                swal.showValidationMessage('Gagal menghapus data. Silakan ulangi lagi')
            });
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then(result => {
        if (result.value) {
            $(`#installment-${id}`).remove();

            var content = $('.___table .___table__installment tbody');
            var count = content.children().length;
            if (count == 0) {
                content.append('<tr><td class="empty" colspan="5">Tidak ada data</td></tr>');
            }

            swal({
                title: 'Dihapus!',
                text: 'Data telah sukses dihapus dari sistem',
                type: 'success',
                confirmButtonClass: 'btn btn-success'
            });
        }
    });
}

function onDeleteSimulation(id, deleteUrl) {
    swal({
        title: 'Yakin Ingin Menghapus?',
        text: 'Data akan dihapus. Aksi ini tidak dapat dikembalikan',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Batalkan',
        confirmButtonClass: 'btn btn-warning',
        confirmButtonText: 'Ya, Hapus',
        showLoaderOnConfirm: true,
        preConfirm: () => {
            return fetch(deleteUrl, {
                method: 'DELETE',
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
                },
            }).then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            }).catch(error => {
                swal.showValidationMessage('Gagal menghapus data. Silakan ulangi lagi')
            });
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then(result => {
        if (result.value) {
            $(`#simulation-${id}`).remove();

            var content = $('.___table .___table__simulation tbody');
            var count = content.children().length;
            if (count == 0) {
                content.append('<tr><td class="empty" colspan="5">Tidak ada data</td></tr>');
            }

            swal({
                title: 'Dihapus!',
                text: 'Data telah sukses dihapus dari sistem',
                type: 'success',
                confirmButtonClass: 'btn btn-success'
            });
        }
    });
}
