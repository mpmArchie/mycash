$(document).ready(function () {
    tinymce.init({
        selector: '#description',
        min_height: 300,
        max_height: 300,
        resize: false,
        menubar: false,
        toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image table',
        plugins: 'paste',
        paste_as_text: true,
        skin_url: '/css/plugins/tinymce',
        plugins: 'lists advlist link image table',
        relative_urls: false,
        remove_script_host: false,
    });
});
