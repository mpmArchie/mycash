function onUploadClick() {
    $('#form-upload-input').click();
    $('#form-upload-input').on('change', () => {
        $('#form-upload').submit();
    });
}
