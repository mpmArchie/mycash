function onDeleteFaq(id, deleteUrl) {
    swal({
        title: 'Yakin Ingin Menghapus?',
        text: 'Data akan dihapus. Aksi ini tidak dapat dikembalikan',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Batalkan',
        confirmButtonClass: 'btn btn-warning',
        confirmButtonText: 'Ya, Hapus',
        showLoaderOnConfirm: true,
        preConfirm: () => {
            return fetch(deleteUrl, {
                method: 'DELETE',
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
                },
            }).then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            }).catch(error => {
                swal.showValidationMessage('Gagal menghapus data. Silakan ulang lagi')
            });
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then(result => {
        if (result.value) {
            $(`#faq-${id}`).remove();

            swal({
                title: 'Dihapus!',
                text: 'Data telah sukses dihapus dari sistem',
                type: 'success',
                confirmButtonClass: 'btn btn-success'
            });
        }
    });
}


$(document).ready(function () {
    $('#sortable-faq .sortable').sortable('option', 'cursor', 'move');
    $('#sortable-faq .sortable').on('sortupdate', function (event, ui) {
        var data = $(this).sortable('serialize');
        var orderingUrl = $('#sortable-faq').data('url');
        $.ajax({
            url: orderingUrl,
            type: 'PUT',
            headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
            },
            data,
        }).done(() => {
            $.toast({
                heading: 'Permintaan Berhasil',
                text: 'Urutan berhasil diperbarui',
                hideAfter: 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        }).fail(error => {
            $.toast({
                heading: 'Permintaan Gagal',
                text: 'Urutan gagal diperbarui. Silakan ulangi lagi',
                hideAfter: 7000,
                position: 'top-right',
                icon: 'error',
                stack: false
            });
        });
    });
});
