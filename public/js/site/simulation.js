function format(value) {
    var str = _.toUpper(value);
    return str.replace(/\w+/g, _.capitalize);
}

function generateOptions(element, placeholder, options) {
    element.find('option').remove().end();
    element.append($('<option>', {
        value: '',
        text: placeholder,
        hidden: 'hidden',
    }));
    options.forEach(option => {
        element.append($('<option>', {
            value: option.value.replace(/\//g, '!'),
            text: option.text,
        }));
    });
}

$(document).ready(() => {
    $('.___simulation-page__form .___simulation-page__form__input--city').on('change', function () {
        generateOptions($('.___simulation-page__form .___simulation-page__form__input--kecamatan'), 'Pilih kecamatan', []);
        generateOptions($('.___simulation-page__form .___simulation-page__form__input--kelurahan'), 'Pilih kelurahan', []);

        var city = this.value;
        var url = `/api/references/${_.toUpper(city)}`;
        $.get(url, function (result) {
            var data = result.data.map((item) => ({
                value: format(item.kecamatan),
                text: format(item.kecamatan),
            }));

            generateOptions($('.___simulation-page__form .___simulation-page__form__input--kecamatan'), 'Pilih kecamatan', data);
        });
    });

    $('.___simulation-page__form .___simulation-page__form__input--kecamatan').on('change', function () {
        generateOptions($('.___simulation-page__form .___simulation-page__form__input--kelurahan'), 'Pilih kelurahan', []);

        var city = $('.___simulation-page__form .___simulation-page__form__input--city').val();
        var kecamatan = this.value;
        var url = `/api/references/${_.toUpper(city)}/${_.toUpper(kecamatan)}`;
        $.get(url, function (result) {
            var data = result.data.map((item) => ({
                value: format(item.kelurahan),
                text: format(item.kelurahan),
            }));

            generateOptions($('.___simulation-page__form .___simulation-page__form__input--kelurahan'), 'Pilih kelurahan', data);
        });
    });

    $('.___simulation-page__form .___simulation-page__form__input--kelurahan').on('change', function () {
        var city = $('.___simulation-page__form .___simulation-page__form__input--city').val();
        var kecamatan = $('.___simulation-page__form .___simulation-page__form__input--kecamatan').val();
        var kelurahan = this.value;
        var url = `/api/references/${_.toUpper(city)}/${_.toUpper(kecamatan)}/${_.toUpper(kelurahan)}`;
        $.get(url, function (result) {
            var data = _.first(result.data);
            if (data) {
                $('.___simulation-page__form .___simulation-page__form__input--postcode').val(data.postcode).change();
            }
        });
    });

    $('.___simulation-page__form .___simulation-page__form__input--postcode').on('change', function () {
        var simulation = $('.___simulation-page__form .___simulation-page__form__input--simulation').val();
        var postcode = this.value;
        var url = `/api/branches-for-postcode/${simulation}/${postcode}`;
        $.get(url, function (result) {
            if (result.data.length > 0) {
                $('.___simulation-page__form .___simulation-page__form__input__container--area').show();
                $('.___simulation-page__form .___simulation-page__form__input--is-area-mandatory').val('1');

                var data = result.data.map((item) => ({
                    value: item.code,
                    text: format(item.name),
                }));

                generateOptions($('.___simulation-page__form .___simulation-page__form__input--area'), 'Pilih cabang', data);
            } else {
                $('.___simulation-page__form .___simulation-page__form__input__container--area').hide();
                $('.___simulation-page__form .___simulation-page__form__input--is-area-mandatory').val('0');

                generateOptions($('.___simulation-page__form .___simulation-page__form__input--area'), 'Pilih cabang', []);
            }
        });
    });
});
