function generateYears(startYear, endYear) {
    var array = [{
        value: startYear,
        text: startYear,
    }];
    var currYear = startYear + 1;
    while (currYear <= endYear) {
        array.push({
            value: currYear,
            text: currYear,
        });
        currYear++;
    }
    return array;
}

function generateInstallments(data) {
    var array = [];
    data.forEach(element => {
        array.push({
            value: element.id,
            text: element.label_duration,
        });
    });
    return array;
}

function generateOptions(element, placeholder, options) {
    element.find('option').remove().end();
    element.append($('<option>', {
        value: '',
        text: placeholder,
        hidden: 'hidden',
    }));
    options.forEach(option => {
        element.append($('<option>', {
            value: option.value,
            text: option.text,
        }));
    });
}

$(document).ready(() => {
    $('.___simulation-form .___simulation-form__input--simulation').on('change', function () {
        var simulation = this.value;
        var data = simulationData.find(function (item) {
            return item.id == simulation;
        });

        var yearData = generateYears(data.start_year, data.end_year);
        generateOptions($('.___simulation-form .___simulation-form__input--year'), 'Pilih tahun', yearData);

        var installmentsData = generateInstallments(data.installments);
        generateOptions($('.___simulation-form .___simulation-form__input--installment'), 'Pilih waktu', installmentsData);
    });
});
