@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'simulation',
    ],
    'title' => 'Edit Tenor',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Simulasi Kredit',
            'href' => route('admin.simulations.index'),
        ],
        [
            'title' => 'Tenor',
            'href' => route('admin.simulations.index'),
        ],
        [
            'title' => 'Edit Tenor',
        ]
    ],
])

@section('title', 'Edit Tenor')

@section('content')
    <div class="container-fluid ___simulation-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Edit Tenor',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.simulations.installment.update', ['installment' => $installment['id']]) }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'duration',
                            'name' => 'duration',
                            'horizontal' => true,
                            'label' => 'LAMA TENOR',
                            'placeholder' => 'Masukkan lama tenor',
                            'type' => 'number',
                            'step' => '.01',
                            'value' => old('duration', $installment['duration']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'rate',
                            'name' => 'rate',
                            'horizontal' => true,
                            'label' => 'PERSENTASE TENOR',
                            'placeholder' => 'Masukkan persentase tenor',
                            'type' => 'number',
                            'step' => '.01',
                            'value' => old('rate', $installment['rate']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.simulations.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Simpan Tenor</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
