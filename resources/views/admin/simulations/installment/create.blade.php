@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'simulation',
    ],
    'title' => 'Tambah Tenor',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Simulasi Kredit',
            'href' => route('admin.simulations.index'),
        ],
        [
            'title' => 'Tenor',
            'href' => route('admin.simulations.index'),
        ],
        [
            'title' => 'Tambah Tenor',
        ]
    ],
])

@section('title', 'Tambah Tenor')

@section('content')
    <div class="container-fluid ___simulation-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Tenor',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.simulations.installment.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'duration',
                            'name' => 'duration',
                            'horizontal' => true,
                            'label' => 'LAMA TENOR',
                            'placeholder' => 'Masukkan lama tenor',
                            'type' => 'number',
                            'step' => '.01',
                            'value' => old('duration'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'rate',
                            'name' => 'rate',
                            'horizontal' => true,
                            'label' => 'PERSENTASE TENOR',
                            'placeholder' => 'Masukkan persentase tenor',
                            'type' => 'number',
                            'step' => '.01',
                            'value' => old('rate'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.simulations.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Tenor</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
