@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'simulation',
    ],
    'title' => 'Simulasi Kredit',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Simulasi Kredit',
        ]
    ],
])

@section('title', 'Simulasi Kredit')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/simulation.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___simulation-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Cabang',
                'addons' => null,
            ])
                <div class="mr-b-10">Terdapat {{ $branches['count'] }} cabang terdaftar. {{ $branches['update'] }}</div>
                <a class="btn btn-rounded btn-primary ripple" href="{{ route('admin.simulations.branch.update') }}">Perbarui Daftar Cabang</a>
            @endcomponent

            @component('elements.container', [
                'title' => 'Daftar Simulasi Kredit',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.simulations.create') }}">Tambah Simulasi Kredit</a>
                @endslot

                <div class="___table">
                    <table class="table table-bordered ___table__simulation">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Jenis Agunan</th>
                                <th>Tenor (Bulan)</th>
                                <th>Perubahan Terakhir</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($simulations) == 0)
                                <tr><td class="empty" colspan="5">Tidak ada data</td></tr>
                            @endif
                            @foreach ($simulations as $simulation)
                                <tr id="simulation-{{ $simulation['id'] }}">
                                    <td>{{ $simulation['name'] }}</td>
                                    <td>{{ $simulation['code_formatted'] }}</td>
                                    <td class="___table__simulation__content--installments">
                                        @foreach ($simulation['installments'] as $installment)
                                            <span>{{ $installment['duration'] }}</span>
                                        @endforeach
                                    </td>
                                    <td>{{ $simulation['update'] }}</td>
                                    <td>
                                        <a class="text-primary" href="{{ route('admin.simulations.edit', ['simulation' => $simulation['id']]) }}"><i class="material-icons">edit</i></a>
                                        <a class="text-muted" onclick="onDeleteSimulation({{ $simulation['id'] }}, '{{ route('admin.simulations.destroy', ['simulation' => $simulation['id']]) }}');"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent

            @component('elements.container', [
                'title' => 'Daftar Tenor',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.simulations.installment.create') }}">Tambah Tenor</a>
                @endslot

                <div class="___table">
                    <table class="table table-bordered ___table__installment">
                        <thead>
                            <tr>
                                <th>Lama Tenor</th>
                                <th>Persentase</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($installments) == 0)
                                <tr><td class="empty" colspan="5">Tidak ada data</td></tr>
                            @endif
                            @foreach ($installments as $installment)
                                <tr id="installment-{{ $installment['id'] }}">
                                    <td>{{ $installment['duration'] }} bulan</td>
                                    <td>{{ $installment['rate'] }}%</td>
                                    <td>
                                        <a class="text-primary" href="{{ route('admin.simulations.installment.edit', ['installment' => $installment['id']]) }}"><i class="material-icons">edit</i></a>
                                        <a class="text-muted" onclick="onDeleteInstallment({{ $installment['id'] }}, '{{ route('admin.simulations.installment.destroy', ['installment' => $installment['id']]) }}');"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/simulation.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
