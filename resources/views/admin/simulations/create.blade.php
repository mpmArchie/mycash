@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'simulation',
    ],
    'title' => 'Tambah Simulasi Kredit',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Simulasi Kredit',
            'href' => route('admin.simulations.index'),
        ],
        [
            'title' => 'Tambah Simulasi Kredit',
        ]
    ],
])

@section('title', 'Tambah Simulasi Kredit')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___simulation-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Simulasi Kredit',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.simulations.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA PRODUK',
                            'placeholder' => 'Masukkan nama produk',
                            'value' => old('name'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'code',
                            'name' => 'code',
                            'horizontal' => true,
                            'label' => 'JENIS AGUNAN',
                            'placeholder' => 'Pilih jenis agunan',
                            'value' => old('code'),
                            'options' => [
                                [
                                    'value' => '8',
                                    'label' => 'Mobil',
                                ],
                                [
                                    'value' => '7',
                                    'label' => 'Motor',
                                ]
                            ],
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'start-year',
                            'name' => 'start_year',
                            'horizontal' => true,
                            'label' => 'TAHUN KENDARAAN TERLAMA',
                            'placeholder' => 'Masukkan tahun kendaraan terlama',
                            'type' => 'number',
                            'value' => old('start_year'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'end-year',
                            'name' => 'end_year',
                            'horizontal' => true,
                            'label' => 'TAHUN KENDARAAN TERBARU',
                            'placeholder' => 'Masukkan tahun kendaraan terbaru',
                            'type' => 'number',
                            'value' => old('end_year'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'max-payment',
                            'name' => 'max_payment',
                            'horizontal' => true,
                            'label' => 'MAKSIMUM PEMBIAYAAN',
                            'placeholder' => 'Masukkan maksimum pembiayaan',
                            'help' => 'Maksimum pembiayaan N% dari Nilai Kendaraan',
                            'type' => 'number',
                            'step' => '.01',
                            'value' => old('max_payment'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select-multiple', [
                            'id' => 'installments',
                            'name' => 'installments',
                            'horizontal' => true,
                            'label' => 'LAMA TENOR & RATE',
                            'placeholder' => 'Pilih lama tenor dan rate',
                            'value' => old('installments', []),
                            'options' => $installments->map(function ($installment) {
                                return [
                                    'value' => $installment['id'],
                                    'label' => $installment['label'],
                                ];
                            }),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.simulations.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Simulasi Kredit</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@endpush
