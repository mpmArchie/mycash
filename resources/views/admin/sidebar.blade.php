@extends('elements.sidebar', [
    'sidebar' => [
        'menu' => [
            [
                'icon' => 'star',
                'name' => 'Produk',
                'href' => route('admin.products.index'),
                'active' => $sidebar['active'] == 'products',
            ],
            [
                'icon' => 'library_books',
                'name' => 'Konten',
                'href' => 'javascript:void(0);',
                'active' => $sidebar['active'] == 'contents',
                'children' => [
                    [
                        'name' => 'FAQ',
                        'href' => route('admin.contents.faq.index'),
                    ],
                    [
                        'name' => 'Syarat dan Ketentuan',
                        'href' => route('admin.contents.term.index'),
                    ],
                    [
                        'name' => 'Cara Pembayaran',
                        'href' => route('admin.contents.payment.index'),
                    ],
                    [
                        'name' => 'Partner',
                        'href' => route('admin.contents.partner.index'),
                    ],
                    [
                        'name' => 'Testimoni',
                        'href' => route('admin.contents.testimony.index'),
                    ],
                    [
                        'name' => 'Promo',
                        'href' => route('admin.contents.promo.index'),
                    ],
                    [
                        'name' => 'Banner',
                        'href' => route('admin.contents.banner.index'),
                    ],
                ],
            ],
            [
                'icon' => 'web',
                'name' => 'Simulasi Kredit',
                'href' => route('admin.simulations.index'),
                'active' => $sidebar['active'] == 'simulation',
            ],
            [
                'icon' => 'pageview',
                'name' => 'Meta Tag',
                'href' => route('admin.metas.index'),
                'active' => $sidebar['active'] == 'metas',
            ],
            [
                'icon' => 'group',
                'name' => 'Atur Pengguna',
                'href' => route('admin.users.index'),
                'active' => $sidebar['active'] == 'users',
            ],
        ],
    ],
])
