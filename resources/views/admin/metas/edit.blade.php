@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'metas',
    ],
    'title' => 'Edit Meta Tag',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Meta Tag',
            'href' => route('admin.metas.index'),
        ],
        [
            'title' => 'Edit Meta Tag',
        ]
    ],
])

@section('title', 'Edit Meta Tag')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___metas-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Edit Meta Tag',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.metas.update', ['meta' => $meta['id']]) }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAME',
                            'placeholder' => 'Masukkan name',
                            'value' => old('name', $meta['name']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'property',
                            'name' => 'property',
                            'horizontal' => true,
                            'label' => 'PROPERTY',
                            'placeholder' => 'Masukkan property',
                            'value' => old('property', $meta['property']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'content',
                            'name' => 'content',
                            'horizontal' => true,
                            'label' => 'CONTENT',
                            'placeholder' => 'Masukkan content',
                            'value' => old('content', $meta['content']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select-multiple', [
                            'id' => 'pages',
                            'name' => 'pages',
                            'horizontal' => true,
                            'label' => 'BERLAKU UNTUK HALAMAN',
                            'placeholder' => 'Pilih halaman',
                            'value' => old('pages', $meta['pages']->map(function ($page) {
                                return $page['id'];
                            })->toArray()),
                            'options' => $pages->map(function ($page) {
                                return [
                                    'value' => $page->id,
                                    'label' => $page->name,
                                ];
                            }),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.metas.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Simpan Meta Tag</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@endpush
