@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'metas',
    ],
    'title' => 'Tambah Meta Tag',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Meta Tag',
            'href' => route('admin.metas.index'),
        ],
        [
            'title' => 'Tambah Meta Tag',
        ]
    ],
])

@section('title', 'Tambah Meta Tag')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___metas-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Meta Tag',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.metas.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAME',
                            'placeholder' => 'Masukkan name',
                            'value' => old('name'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'property',
                            'name' => 'property',
                            'horizontal' => true,
                            'label' => 'PROPERTY',
                            'placeholder' => 'Masukkan property',
                            'value' => old('property'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'content',
                            'name' => 'content',
                            'horizontal' => true,
                            'label' => 'CONTENT',
                            'placeholder' => 'Masukkan content',
                            'value' => old('content'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select-multiple', [
                            'id' => 'pages',
                            'name' => 'pages',
                            'horizontal' => true,
                            'label' => 'BERLAKU UNTUK HALAMAN',
                            'placeholder' => 'Pilih halaman',
                            'value' => old('pages', []),
                            'options' => $pages->map(function ($page) {
                                return [
                                    'value' => $page->id,
                                    'label' => $page->name,
                                ];
                            }),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.metas.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Meta Tag</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@endpush
