@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'metas',
    ],
    'title' => 'Meta Tag',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Meta Tag',
        ]
    ],
])

@section('title', 'Meta Tag')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/metas.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___metas-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Meta Tag',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.metas.create') }}">Tambah Meta Tag</a>
                @endslot

                <div class="___table">
                    <table class="table table-bordered ___table__metas">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Property</th>
                                <th>Content</th>
                                <th>Halaman</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($metas) == 0)
                                <tr><td class="empty" colspan="5">Tidak ada data</td></tr>
                            @endif
                            @foreach ($metas as $meta)
                                <tr id="meta-{{ $meta['id'] }}">
                                    <td>{{ $meta['name'] ?? '' }}</td>
                                    <td>{{ $meta['property'] ?? '' }}</td>
                                    <td>{{ $meta['content'] ?? '' }}</td>
                                    <td class="___table__metas__content--pages">
                                        @foreach ($meta['pages'] as $page)
                                            <span>{{ $page['name'] }}</span>
                                        @endforeach
                                    </td>
                                    <td>
                                        <a class="text-primary" href="{{ route('admin.metas.edit', ['meta' => $meta['id']]) }}"><i class="material-icons">edit</i></a>
                                        <a class="text-muted" onclick="onDeleteMeta({{ $meta['id'] }}, '{{ route('admin.metas.destroy', ['meta' => $meta['id']]) }}');"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/metas.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
