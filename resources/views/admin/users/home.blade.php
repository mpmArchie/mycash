@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'users',
    ],
    'title' => 'Atur Pengguna',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Atur Pengguna',
        ]
    ],
])

@section('title', 'Atur Pengguna')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/users.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___users-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Atur Akun Saya',
                'addons' => null,
            ])
                <div class="">
                    <p class="mr-b-10">Lakukan perubahan profil dan kata sandi Anda disini</p>
                    <div class="d-flex flex-column flex-sm-row">
                        <a class="___btn-change-profile btn btn-rounded btn-primary ripple" href="{{ route('admin.users.profile') }}">Ubah Profil</a>
                        <a class="___btn-change-password btn btn-rounded btn-success ripple mt-2 mt-sm-0" href="{{ route('admin.users.password') }}">Ubah Kata Sandi</a>
                    </div>
                </div>
            @endcomponent

            @component('elements.container', [
                'title' => 'Daftar Pengguna',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.users.create') }}">Tambah Pengguna</a>
                @endslot

                <div class="___table">
                    <table class="table table-bordered ___table__users">
                        <thead>
                            <tr>
                                <th>Nama Pengguna</th>
                                <th>Email Pengguna</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($users) == 0)
                                <tr><td class="empty" colspan="5">Tidak ada data</td></tr>
                            @endif
                            @foreach ($users as $user)
                                <tr id="user-{{ $user['id'] }}">
                                    <td>{{ $user['name'] }}</td>
                                    <td>{{ $user['email'] }}</td>
                                    @if ($user['id'] == auth()->user()->id)
                                        <td></td>
                                    @else
                                        <td>
                                            <a class="text-primary" href="{{ route('admin.users.edit', ['user' => $user['id']]) }}"><i class="material-icons">edit</i></a>
                                            <a class="text-muted" onclick="onDeleteUser({{ $user['id'] }}, '{{ route('admin.users.destroy', ['user' => $user['id']]) }}');"><i class="material-icons">delete</i></a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/users.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
