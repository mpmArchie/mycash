@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'users',
    ],
    'title' => 'Ubah Profil',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Atur Pengguna',
            'href' => route('admin.users.index'),
        ],
        [
            'title' => 'Ubah Profil',
        ]
    ],
])

@section('title', 'Ubah Profil')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___users-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Atur Akun Saya',
                'addons' => null,
            ])
                <h6 class="fw-700 mr-b-20">Gambar Profil</h6>
                <div class="___account-page__profile__image flex-column flex-sm-row">
                    <img class="img-thumbnail" src="{{ $user['photo']['uri'] }}" alt="">
                    <form id="form-upload" style="display: none;" enctype="multipart/form-data" method="POST" action="{{ route('admin.users.photo.update') }}">
                        @csrf
                        @method('PUT')
                        <input id="form-upload-input" type="file" name="photo" accept="image/x-png,image/jpeg" />
                    </form>
                    <button class="btn btn-rounded btn-primary ripple ml-0 ml-sm-4 mt-3 mt-sm-0" onclick="onUploadClick();">Ganti Gambar Profil</button>
                </div>
                <hr class="mr-tb-20">
                <h6 class="fw-700 mr-b-20">Data Diri</h6>
                <form class="row" method="POST" action="{{ route('admin.users.profile.update') }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'placeholder' => 'Nama pengguna',
                            'value' => old('name', $user['name']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'email',
                            'name' => 'email',
                            'horizontal' => true,
                            'label' => 'ALAMAT EMAIL',
                            'placeholder' => 'Alamat email pengguna',
                            'value' => old('email', $user['email']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Ubah Data Diri</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/users.profile.js') }}"></script>
    <script>
        @if($errors->has('photo'))
            $.toast({
                heading: 'Permintaan Gagal',
                text: "{{ $errors->first('photo') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'error',
                stack: false
            });
        @endif
    </script>
@endpush
