@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'users',
    ],
    'title' => 'Tambah Pengguna',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Atur Pengguna',
            'href' => route('admin.users.index'),
        ],
        [
            'title' => 'Tambah Pengguna',
        ]
    ],
])

@section('title', 'Tambah Pengguna')

@section('content')
    <div class="container-fluid ___users-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Pengguna',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.users.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'placeholder' => 'Masukkan nama pengguna',
                            'value' => old('name'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'email',
                            'name' => 'email',
                            'horizontal' => true,
                            'label' => 'EMAIL',
                            'placeholder' => 'Masukkan email pengguna',
                            'value' => old('email'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'password',
                            'name' => 'password',
                            'horizontal' => true,
                            'label' => 'KATA SANDI',
                            'placeholder' => 'Masukkan kata sandi pengguna',
                            'type' => 'password',
                            'value' => old('password'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        <h5 class="box-title mr-t-10 mr-b-0">KONFIRMASI PENGGUNA</h5>
                        <div class="mr-b-10">Kata sandi Anda diperlukan untuk melakukan penambahan pengguna</div>
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'password-user',
                            'name' => 'password_user',
                            'horizontal' => true,
                            'label' => 'KATA SANDI ANDA',
                            'placeholder' => 'Masukkan kata sandi Anda',
                            'type' => 'password',
                            'value' => old('password_user'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.users.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Tambah Pengguna</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
