@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Tambah Pertanyaan',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'F.A.Q.',
            'href' => route('admin.contents.faq.index'),
        ],
        [
            'title' => 'Tambah Pertanyaan',
        ]
    ],
])

@section('title', 'Tambah Pertanyaan')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Pertanyaan',
                'addons' => null,
            ])
                <form class="row" method="POST" action="{{ route('admin.contents.faq.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'question',
                            'name' => 'question',
                            'horizontal' => true,
                            'label' => 'PERTANYAAN',
                            'placeholder' => 'Masukkan pertanyaan',
                            'value' => old('question'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'answer',
                            'name' => 'answer',
                            'horizontal' => true,
                            'label' => 'JAWABAN',
                            'placeholder' => 'Masukkan jawaban dari pertanyaan',
                            'value' => old('answer'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.faq.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Pertanyaan</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
