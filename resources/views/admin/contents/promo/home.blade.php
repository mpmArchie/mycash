@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Promo',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Promo',
        ]
    ],
])

@section('title', 'Promo')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___promo-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Promo',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.contents.promo.create') }}">Tambah Promo</a>
                @endslot

                <div class="row">
                    @foreach ($promos as $promo)
                        @include('elements.card-image', [
                            'id' => 'promo-' . $promo['id'],
                            'image' => $promo['image']['uri'],
                            'title' => $promo['name'],
                            'subtitle' => $promo['update'],
                            'edit' => route('admin.contents.promo.edit', ['promo' => $promo['id']]),
                            'delete' => 'onDeletePromo(' . $promo['id'] . ", '" . route('admin.contents.promo.destroy', ['promo' => $promo['id']]) . "');",
                        ])
                    @endforeach
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.promo.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
