@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Tambah Promo',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Promo',
            'href' => route('admin.contents.promo.index'),
        ],
        [
            'title' => 'Tambah Promo',
        ]
    ],
])

@section('title', 'Tambah Promo')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Promo',
                'addons' => null,
            ])
                <form class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.contents.promo.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'GAMBAR PROMO',
                            'help' => 'Rekomendasi ukuran gambar 1600 x 900 px',
                            'value' => old('image'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA PROMO',
                            'placeholder' => 'Masukkan nama promo',
                            'value' => old('name'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.editor', [
                            'id' => 'description',
                            'name' => 'description',
                            'horizontal' => true,
                            'label' => 'DESKRIPSI PROMO',
                            'placeholder' => 'Masukkan deskripsi promo',
                            'value' => old('description'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.promo.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Promo</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/tinymce.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.promo.create.js') }}"></script>
@endpush
