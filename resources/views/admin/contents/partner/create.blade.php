@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Tambah Partner',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Partner',
            'href' => route('admin.contents.partner.index'),
        ],
        [
            'title' => 'Tambah Partner',
        ]
    ],
])

@section('title', 'Tambah Partner')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Partner',
                'addons' => null,
            ])
                <form class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.contents.partner.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'LOGO PARTNER',
                            'help' => 'Rekomendasi ukuran gambar 400 x 300 px',
                            'value' => old('image'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA PARTNER',
                            'placeholder' => 'Masukkan nama partner',
                            'value' => old('name'),
                            ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'website',
                            'name' => 'website',
                            'horizontal' => true,
                            'label' => 'WEBSITE PARTNER',
                            'placeholder' => 'Masukkan website partner',
                            'value' => old('website'),
                            ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.partner.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Partner</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
