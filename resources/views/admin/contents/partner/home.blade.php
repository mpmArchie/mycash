@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Partner',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Partner',
        ]
    ],
])

@section('title', 'Partner')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/contents.partner.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Partner',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.contents.partner.create') }}">Tambah Partner</a>
                @endslot

                <div class="___table">
                    <table class="table table-bordered ___table__partner">
                        <thead>
                            <tr>
                                <th>Logo</th>
                                <th>Nama</th>
                                <th>Perubahan Terakhir</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($partners) == 0)
                                <tr><td class="empty" colspan="5">Tidak ada data</td></tr>
                            @endif
                            @foreach ($partners as $partner)
                                <tr id="{{ 'partner-' . $partner['id'] }}">
                                    <td><img src="{{ $partner['image']['uri'] }}" alt="logo" /></td>
                                    <td>{{ $partner['name'] }}</td>
                                    <td>{{ $partner['update'] }}</td>
                                    <td>
                                        <a class="text-primary" href="{{ route('admin.contents.partner.edit', ['partner' => $partner['id']]) }}"><i class="material-icons">edit</i></a>
                                        <a class="text-muted" onclick="{{ 'onDeletePartner(' . $partner['id'] . ", '" . route('admin.contents.partner.destroy', ['partner' => $partner['id']]) . "');" }}"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.partner.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
