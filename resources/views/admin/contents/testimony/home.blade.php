@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Testimoni',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Testimoni',
        ]
    ],
])

@section('title', 'Testimoni')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/contents.testimony.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Testimoni',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.contents.testimony.create') }}">Tambah Testimoni</a>
                @endslot

                <div class="___table">
                    <table class="table table-bordered ___table__testimony">
                        <thead>
                            <tr>
                                <th>Foto</th>
                                <th>Nama</th>
                                <th>Isi Testimoni</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($testimonies) == 0)
                                <tr><td class="empty" colspan="5">Tidak ada data</td></tr>
                            @endif
                            @foreach ($testimonies as $testimony)
                                <tr id="{{ 'testimony-' . $testimony['id'] }}">
                                    <td><img src="{{ $testimony['image']['uri'] }}" alt="photo" /></td>
                                    <td>{{ $testimony['name'] }}</td>
                                    <td>{{ $testimony['content'] }}</td>
                                    <td>
                                        <a class="text-primary" href="{{ route('admin.contents.testimony.edit', ['testimony' => $testimony['id']]) }}"><i class="material-icons">edit</i></a>
                                        <a class="text-muted" onclick="{{ 'onDeleteTestimony(' . $testimony['id'] . ", '" . route('admin.contents.testimony.destroy', ['testimony' => $testimony['id']]) . "');" }}"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.testimony.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
