@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Edit Testimoni',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Testimoni',
            'href' => route('admin.contents.testimony.index'),
        ],
        [
            'title' => 'Edit Testimoni',
        ]
    ],
])

@section('title', 'Edit Testimoni')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Edit Testimoni',
                'addons' => null,
            ])
                <form id="form-testimony" class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.contents.testimony.update', ['testimony' => $testimony['id']]) }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'FOTO PEMBERI TESTIMONI',
                            'help' => 'Biarkan kosong jika tidak ingin mengubah. Rekomendasi ukuran gambar 200 x 200 px',
                            'value' => old('image', $testimony['image']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA PEMBERI TESTIMONI',
                            'placeholder' => 'Masukkan nama pemberi testimoni',
                            'value' => old('name', $testimony['name']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'content',
                            'name' => 'content',
                            'horizontal' => true,
                            'label' => 'ISI TESTIMONI',
                            'placeholder' => 'Masukkan isi testimoni',
                            'value' => old('content', $testimony['content']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.testimony.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Simpan Testimoni</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
