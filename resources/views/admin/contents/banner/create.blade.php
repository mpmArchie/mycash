@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Tambah Banner',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Banner',
            'href' => route('admin.contents.banner.index'),
        ],
        [
            'title' => 'Tambah Banner',
        ]
    ],
])

@section('title', 'Tambah Banner')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Banner',
                'addons' => null,
            ])
                <form class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.contents.banner.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'GAMBAR BANNER',
                            'help' => 'Rekomendasi ukuran gambar 1920 x 1080 px',
                            'value' => old('image'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'title',
                            'name' => 'title',
                            'horizontal' => true,
                            'label' => 'JUDUL BANNER',
                            'placeholder' => 'Masukkan judul banner',
                            'value' => old('title'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'subtitle',
                            'name' => 'subtitle',
                            'horizontal' => true,
                            'label' => 'SUB-JUDUL BANNER',
                            'placeholder' => 'Masukkan sub-judul banner',
                            'value' => old('subtitle'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'page',
                            'name' => 'page',
                            'horizontal' => true,
                            'label' => 'BERLAKU UNTUK HALAMAN',
                            'placeholder' => 'Pilih halaman',
                            'value' => old('page'),
                            'options' => $pages->map(function ($page) {
                                return [
                                    'value' => $page->id,
                                    'label' => $page->name,
                                ];
                            }),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.banner.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Banner</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@endpush
