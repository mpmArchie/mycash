@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Banner',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Banner',
        ]
    ],
])

@section('title', 'Banner')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/contents.banner.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @foreach ($pages as $page)
                @component('elements.container', [
                    'title' => 'Halaman ' . $page['name'],
                ])
                    @slot('addons')
                        <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.contents.banner.create') }}">Tambah Banner</a>
                    @endslot

                    <div class="___table">
                        <table class="table table-bordered ___table__banner">
                            <thead>
                                <tr>
                                    <th>Banner</th>
                                    <th>Judul</th>
                                    <th>Sub-Judul</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($banners as $banner)
                                    @if ($page['id'] == $banner['page_id'])
                                        <tr id="{{ 'banner-' . $banner['id'] }}">
                                            <td><img src="{{ $banner['image']['uri'] }}" alt="banner" /></td>
                                            <td>{{ $banner['title'] }}</td>
                                            <td>{{ $banner['subtitle'] }}</td>
                                            <td>
                                                <a class="text-primary" href="{{ route('admin.contents.banner.edit', ['banner' => $banner['id']]) }}"><i class="material-icons">edit</i></a>
                                                <a class="text-muted" onclick="{{ 'onDeleteBanner(' . $banner['id'] . ", '" . route('admin.contents.banner.destroy', ['banner' => $banner['id']]) . "');" }}"><i class="material-icons">delete</i></a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endcomponent
            @endforeach
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.banner.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
