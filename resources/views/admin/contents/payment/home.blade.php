@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Cara Pembayaran',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Cara Pembayaran',
        ]
    ],
])

@section('title', 'Cara Pembayaran')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Cara Pembayaran',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.contents.payment.create') }}">Tambah Cara Pembayaran</a>
                @endslot

                @include('elements.sortable', [
                    'id' => 'sortable-payment',
                    'url' => route('admin.contents.payment.ordering'),
                    'items' => $payments->map(function ($payment) {
                        return [
                            'id' => 'payment-' . $payment['id'],
                            'label' => $payment['name'],
                            'icon' => $payment['image']['uri'],
                            'edit' => route('admin.contents.payment.edit', ['payment' => $payment['id']]),
                            'delete' => 'onDeletePayment(' . $payment['id'] . ", '" . route('admin.contents.payment.destroy', ['payment' => $payment['id']]) . "');",
                        ];
                    }),
                ])
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.payment.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
