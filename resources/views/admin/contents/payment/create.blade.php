@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Tambah Cara Pembayaran',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Cara Pembayaran',
            'href' => route('admin.contents.payment.index'),
        ],
        [
            'title' => 'Tambah Cara Pembayaran',
        ]
    ],
])

@section('title', 'Tambah Cara Pembayaran')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Cara Pembayaran',
                'addons' => null,
            ])
                <form class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.contents.payment.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'LOGO METODE PEMBAYARAN',
                            'help' => 'Rekomendasi ukuran gambar 100 x 56 px',
                            'value' => old('image'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA METODE PEMBAYARAN',
                            'placeholder' => 'Masukkan nama metode pembayaran',
                            'value' => old('name'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.editor', [
                            'id' => 'description',
                            'name' => 'description',
                            'horizontal' => true,
                            'label' => 'LANGKAH-LANGKAH PEMBAYARAN',
                            'placeholder' => 'Masukkan langkah-langkah pembayaran',
                            'value' => old('description'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.payment.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Cara Pembayaran</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/tinymce.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.payment.create.js') }}"></script>
@endpush
