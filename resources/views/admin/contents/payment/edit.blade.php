@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'contents',
    ],
    'title' => 'Edit Cara Pembayaran',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Konten',
            'href' => 'javascript:void(0);',
        ],
        [
            'title' => 'Cara Pembayaran',
            'href' => route('admin.contents.payment.index'),
        ],
        [
            'title' => 'Edit Cara Pembayaran',
        ]
    ],
])

@section('title', 'Edit Cara Pembayaran')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Edit Cara Pembayaran',
                'addons' => null,
            ])
                <form class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.contents.payment.update', ['payment' => $payment['id']]) }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'LOGO METODE PEMBAYARAN',
                            'help' => 'Biarkan kosong jika tidak ingin mengubah. Rekomendasi ukuran gambar 100 x 56 px',
                            'value' => old('image', $payment['image']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA METODE PEMBAYARAN',
                            'placeholder' => 'Masukkan nama metode pembayaran',
                            'value' => old('name', $payment['name']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.editor', [
                            'id' => 'description',
                            'name' => 'description',
                            'horizontal' => true,
                            'label' => 'LANGKAH-LANGKAH PEMBAYARAN',
                            'placeholder' => 'Masukkan langkah-langkah pembayaran',
                            'value' => old('description', $payment['description']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.contents.payment.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Simpan Cara Pembayaran</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/tinymce.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/contents.payment.create.js') }}"></script>
@endpush
