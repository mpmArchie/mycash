@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'products',
    ],
    'title' => 'Produk',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Produk',
        ]
    ],
])

@section('title', 'Produk')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@push('styles-app')
    <link href="{{ asset('css/admin/products.css?v=1.0.5') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Encouragement Words',
                'addons' => null,
            ])
                <div id="___product-words" class="col-xs-12 col-md-10 col-lg-8">
                    <div class="___container row flex-column flex-sm-row">
                        @include('elements.forms.static', [
                            'class' => 'flex-fill',
                            'horizontal' => true,
                            'label' => 'KATA-KATA',
                            'value' => $encouragementWords,
                        ])
                        <button class="btn btn-rounded btn-outline-default align-self-start ml-sm-3" onclick="onUpdateEncouragementWords();">UBAH</button>
                    </div>
                </div>
                <div id="___product-words-form" class="col-xs-12 col-md-10 col-lg-8">
                    <form class="___container row flex-column flex-sm-row" method="POST" action="{{ route('admin.settings.update', ['setting' => 'encouragement_words']) }}">
                        @csrf
                        @method('PUT')

                        @include('elements.forms.textarea', [
                            'class' => 'flex-fill',
                            'id' => '___product-words-form-value',
                            'name' => 'value',
                            'horizontal' => true,
                            'required' => true,
                            'label' => 'KATA-KATA',
                            'placeholder' => 'Masukkan kata-kata penyemangat',
                            'help' => 'Gunakan <br/> untuk membuat baris baru',
                            'value' => old('value', $encouragementWords),
                        ])
                        <button class="btn btn-rounded btn-primary align-self-start ml-sm-3" type="submit">UBAH</button>
                    </form>
                </div>
            @endcomponent

            @component('elements.container', [
                'title' => 'Produk-produk',
            ])
                @slot('addons')
                    <a class="btn btn-rounded btn-primary ripple mt-2 mt-sm-0" href="{{ route('admin.products.create') }}">Tambah Produk</a>
                @endslot

                <div class="row">
                    @foreach ($products as $product)
                        @include('elements.card-image', [
                            'id' => 'product-' . $product['id'],
                            'image' => $product['image']['uri'],
                            'title' => $product['name'],
                            'subtitle' => $product['update'],
                            'edit' => route('admin.products.edit', ['product' => $product['id']]),
                            'delete' => 'onDeleteProduct(' . $product['id'] . ", '" . route('admin.products.destroy', ['product' => $product['id']]) . "');",
                        ])
                    @endforeach
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/products.js') }}"></script>
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
