@extends('layouts.main', [
    'sidebar' => [
        'name' => 'admin.sidebar',
        'active' => 'products',
    ],
    'title' => 'Tambah Produk',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('admin.home'),
        ],
        [
            'title' => 'Produk',
            'href' => route('admin.products.index'),
        ],
        [
            'title' => 'Tambah Produk',
        ]
    ],
])

@section('title', 'Tambah Produk')

@section('content')
    <div class="container-fluid ___product-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Tambah Produk Baru',
                'addons' => null,
            ])
                <form class="row" method="POST" enctype="multipart/form-data" action="{{ route('admin.products.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA PRODUK',
                            'placeholder' => 'Nama produk baru',
                            'help' => 'Gunakan <br/> untuk membuat baris baru',
                            'value' => old('name'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'image',
                            'name' => 'image',
                            'horizontal' => true,
                            'label' => 'GAMBAR PRODUK',
                            'help' => 'Rekomendasi ukuran gambar 1920 x 1080 px',
                            'value' => old('image'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.uploader', [
                            'id' => 'banner',
                            'name' => 'banner',
                            'horizontal' => true,
                            'label' => 'GAMBAR BANNER',
                            'help' => 'Rekomendasi ukuran gambar 1920 x 1080 px',
                            'value' => old('banner'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.editor', [
                            'id' => 'description',
                            'name' => 'description',
                            'horizontal' => true,
                            'label' => 'DESKRIPSI PRODUK',
                            'placeholder' => 'Tambahkan deskripsi produk',
                            'value' => old('description'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'summary',
                            'name' => 'summary',
                            'horizontal' => true,
                            'label' => 'RINGKASAN PRODUK',
                            'placeholder' => 'Tambahkan ringkasan produk (maks. 140 karakter)',
                            'value' => old('summary'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 mr-t-10 d-flex justify-content-end flex-column flex-sm-row">
                        <a href="{{ route('admin.products.index') }}"><button class="btn btn-rounded btn-outline-default ripple mr-sm-3 mb-2 mb-sm-0" type="button">Batalkan</button></a>
                        <button class="btn btn-rounded btn-primary ripple" type="submit">Tambah Produk</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/tinymce.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/admin/products.create.js') }}"></script>
@endpush
