@extends('layouts.full')

@section('body-class', 'error-page')
@section('title', 'Sistem Tidak Tersedia')

@section('content')
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            <main class="main-wrapper">
                <div class="page-title">
                    <h1 class="color-white">503</h1>
                </div>
                <h3 class="mr-b-5 color-white">Sistem Tidak Tersedia!</h3>
                <p class="mr-b-30 color-white fs-18 fw-200 heading-font-family">Sistem sedang tidak dapat diakses. Silakan coba beberapa saat lagi.</p><a href="javascript: history.back();" class="btn btn-outline-white btn-rounded btn-block fw-700 text-uppercase">Kembali</a>
            </main>
        </div>
    </div>
@endsection
