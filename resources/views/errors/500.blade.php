@extends('layouts.full')

@section('body-class', 'error-page')
@section('title', 'Gangguan Sistem')

@section('content')
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            <main class="main-wrapper">
                <div class="page-title">
                    <h1 class="color-white">500</h1>
                </div>
                <h3 class="mr-b-5 color-white">Gangguan Sistem!</h3>
                <p class="mr-b-30 color-white fs-18 fw-200 heading-font-family">Terjadi gangguan pada sistem dan permintaan Anda tidak dapat diselesaikan.</p><a href="javascript: history.back();" class="btn btn-outline-white btn-rounded btn-block fw-700 text-uppercase">Kembali</a>
            </main>
        </div>
    </div>
@endsection
