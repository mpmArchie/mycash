<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('templates/backend/css/pace.css') }}">

    @stack('metas')

    <title>@yield('title', 'Selamat Datang') | {{ config('app.name') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('templates/backend/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('templates/backend/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('templates/backend/vendors/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">

    @stack('styles')

    <link href="{{ asset('templates/backend/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css?v=1.0.5') }}" rel="stylesheet">

    @stack('styles-app')

    <script src="{{ asset('templates/backend/js/modernizr.min.js') }}"></script>
    <script data-pace-options='{ "ajax": false, "selectors": [ "img" ]}' src="{{ asset('templates/backend/js/pace.min.js') }}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="{{ 'https://www.googletagmanager.com/gtag/js?id=' . config('setting.analytics.google') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config('setting.analytics.google') }}');
    </script>



    
@stack('scripts-head')
</head>

<body class="sidebar-dark sidebar-expand navbar-brand-dark header-light">
    <div id="wrapper" class="wrapper">
        @include('elements.header')
        <div class="content-wrapper">
            @include($sidebar['name'])
            <main class="main-wrapper clearfix">
                @include('elements.title')
                @yield('content')

                <aside class="right-sidebar"></aside>
                <div class="chat-panel"></div>
            </main>
        </div>

        <footer class="footer bg-primary text-inverse text-center">
            <div class="container-fluid"><span class="fs-13 heading-font-family">MPM Finance &copy; 2019. All right reserved.</span></div>
        </footer>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.9/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('templates/backend/js/bootstrap.min.js') }}"></script>

    @stack('scripts')

    <script src="{{ asset('templates/backend/js/template.js') }}"></script>
    <script src="{{ asset('templates/backend/js/custom.js') }}"></script>

    @stack('scripts-app')
</body>

</html>
