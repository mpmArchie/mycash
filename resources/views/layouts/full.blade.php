<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('templates/backend/css/pace.css') }}">

    @stack('metas')

    <title>@yield('title', 'Selamat Datang') | {{ config('app.name') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('templates/backend/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('templates/backend/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('templates/backend/vendors/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">

    @stack('styles')

    <link href="{{ asset('templates/backend/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css?v=1.0.5') }}" rel="stylesheet">

    <script src="{{ asset('templates/backend/js/modernizr.min.js') }}"></script>
    <script data-pace-options='{ "ajax": false, "selectors": [ "img" ]}' src="{{ asset('templates/backend/js/pace.min.js') }}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="{{ 'https://www.googletagmanager.com/gtag/js?id=' . config('setting.analytics.google') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config('setting.analytics.google') }}');
    </script>
</head>

<body class="body-bg-full profile-page @yield('body-class', '')" style="background-image: url({{ asset('images/site-bg.jpg') }})">
    <div id="wrapper" class="row wrapper">
        @yield('content')
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('templates/backend/js/bootstrap.min.js') }}"></script>

    @stack('scripts')
</body>

</html>
