<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        @isset($metas)
            @if (count($metas) > 0)
                @foreach ($metas as $meta)
                    {{-- <meta {{ isset($meta['name']) ? 'name=' . $meta['name'] : '' }} {{ isset($meta['property']) ? 'property=' . $meta['property'] : '' }} {{ isset($meta['content']) ? 'content=' . $meta['content'] : '' }}> --}}
                    <meta name="{{ $meta['name'] }}" property="{{ $meta['property'] }}" content="{{ $meta['content'] }}">
                @endforeach
            @endif
        @endisset

        <title>@yield('title', 'Selamat Datang') | {{ config('app.name') }}</title>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ asset('templates/frontend/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('templates/frontend/plugins/revolution/css/settings.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('templates/frontend/plugins/revolution/css/layers.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('templates/frontend/plugins/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('templates/frontend/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('templates/frontend/css/responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('css/site/main.css?v=1.0.5') }}" rel="stylesheet">

        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

        <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="{{ asset('templates/frontend/js/respond.js') }}"></script><![endif]-->

        @stack('styles')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="{{ 'https://www.googletagmanager.com/gtag/js?id=' . config('setting.analytics.google') }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{ config('setting.analytics.google') }}');
        </script>
        <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '549164588944373');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=549164588944373&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

        @stack('scripts-head')
    </head>

    <body>
        <div class="page-wrapper">
            <div class="preloader"></div>

            @include('site.elements.header')

            @yield('content')

            @include('site.elements.footer')
        </div>

        <div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>

        <script src="{{ asset('templates/frontend/js/jquery.js') }}"></script>

        <script src="{{ asset('templates/frontend/plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/plugins/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script src="{{ asset('templates/frontend/js/main-slider-script.js') }}"></script>

        <script src="{{ asset('templates/frontend/js/bootstrap.min.js') }}"></script>

        @stack('scripts')

        <script src="{{ asset('templates/frontend/js/script.js') }}"></script>

        @stack('scripts-app')

    </body>
</html>
