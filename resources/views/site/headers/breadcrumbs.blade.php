<section class="page-title" style="background-image:url({{ $background }})">
    <div class="auto-container">
        <h1>{{ $title }}</h1>
        <ul class="page-breadcrumb">
            @foreach ($breadcrumbs as $item)
                @if (isset($item['link']))
                    <li><a href="{{ $item['link'] }}">{{ $item['title'] }}</a></li>
                @else
                    <li>{{ $item['title'] }}</li>
                @endif
            @endforeach
        </ul>
    </div>
</section>
