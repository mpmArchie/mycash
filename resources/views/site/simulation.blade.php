@extends('site.layouts.main', [ 'header' => 'product'])

@section('title', 'Simulasi Kredit')

@push('scripts-head')
   <script> gtag('event', 'conversion', {'send_to': 'AW-754614875/8QFzCIrP0ZcBENuE6ucC'}); </script>
   <script>
  fbq('track', 'CompleteRegistration');
</script>
@endpush


@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-2.jpg'),
        'title' => 'Simulasi Kredit',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Produk',
                'link' => route('product'),
            ],
            [
                'title' => 'Simulasi Kredit',
            ]
        ],
    ])

    <section class="checkout-page">
    	<div class="auto-container">
            <div class="billing-details">
                <div class="shop-form"> 
                    <form class="___simulation-page__form" method="POST" action="{{ route('simulation.create') }}">
                        @csrf
                        <input class="___simulation-page__form__input--is-area-mandatory" type="hidden" name="is-area-mandatory" value="0" >
                        <input class="___simulation-page__form__input--simulation" type="hidden" name="simulation" value="{{ old('simulation') }}" >
                        <input type="hidden" name="year" value="{{ old('year') }}" >
                        <input type="hidden" name="total" value="{{ old('total') }}" >
                        <input type="hidden" name="installment" value="{{ old('installment') }}" >
                        <div class="row clearfix">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <div class="sec-title-two"><h2>Data Diri</h2></div>
                        		<div class="billing-inner">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div class="field-label">Nama Sesuai KTP</div>
                                            <input type="text" name="name" value="{{ old('name') }}" placeholder="Masukkan nama" required>
                                            @if ($errors->has('name'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">Nomor KTP</div>
                                            <input type="text" name="ktp" value="{{ old('ktp') }}" placeholder="Masukkan nomor KTP" required>
                                            @if ($errors->has('ktp'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('ktp') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">Nomor Ponsel</div>
                                            <input type="text" name="phone" value="{{ old('phone') }}" placeholder="Masukkan nomor ponsel" required>
                                            @if ($errors->has('phone'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div class="field-label">Email</div>
                                            <input type="email" name="email" value="{{ old('email') }}" placeholder="Masukkan alamat email" required>
                                            @if ($errors->has('email'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div class="field-label">Alamat Domisili</div>
                                            <input type="text" name="address" value="{{ old('address') }}" placeholder="Masukkan alamat domisili" required>
                                            @if ($errors->has('address'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">RT</div>
                                            <input type="number" name="rt" value="{{ old('rt') }}" placeholder="Masukkan RT" required>
                                            @if ($errors->has('rt'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('rt') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">RW</div>
                                            <input type="number" name="rw" value="{{ old('rw') }}" placeholder="Masukkan RW" required>
                                            @if ($errors->has('rw'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('rw') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">Kota</div>
                                            <select class="___simulation-page__form__input--city" name="city" value="{{ old('city') }}" required>
                                                <option value="" hidden>Pilih kota</option>
                                                @foreach ($cities as $item)
                                                    <option value="{{ title_case($item['city']) }}">{{ title_case($item['city']) }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('city'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">Kecamatan</div>
                                            <select class="___simulation-page__form__input--kecamatan" name="kecamatan" value="{{ old('kecamatan') }}" required>
                                                <option value="" hidden>Pilih kecamatan</option>
                                            </select>
                                            @if ($errors->has('kecamatan'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('kecamatan') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">Kelurahan</div>
                                            <select class="___simulation-page__form__input--kelurahan" name="kelurahan" value="{{ old('kelurahan') }}" required>
                                                <option value="" hidden>Pilih kelurahan</option>
                                            </select>
                                            @if ($errors->has('kelurahan'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('kelurahan') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="field-label">Kode Pos</div>
                                            <input class="___simulation-page__form__input--postcode" type="text" name="postcode" value="{{ old('postcode') }}" placeholder="Masukkan kode pos" required>
                                            @if ($errors->has('postcode'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('postcode') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ___simulation-page__form__input__container--area">
                                            <div class="field-label">Cabang</div>
                                            <select class="___simulation-page__form__input--area" name="area" value="{{ old('area') }}" required>
                                                <option value="" hidden>Pilih cabang</option>
                                            </select>
                                            @if ($errors->has('area'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('area') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12 submit-button">
                                            <button type="button" class="theme-btn btn-style-seven" data-toggle="modal" data-target="#disclaimer-modal">Lihat Hasil Hitungan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="sec-title-two"><h2>Data Kredit</h2></div>
                                <div class="shop-order-box">
                                	<div class="information-list">
                                        <div class="information-item row">
                                            <span class="col-xs-12 col-sm-6">Jenis Agunan/Produk</span>
                                            <span class="dark col-xs-12 col-sm-6">{{ $information['name'] }}</span>
                                        </div>
                                        <div class="information-item row">
                                            <span class="col-xs-12 col-sm-6">Tahun Kendaraan</span>
                                            <span class="dark col-xs-12 col-sm-6">{{ $information['year'] }}</span>
                                        </div>
                                        <div class="information-item row">
                                            <span class="col-xs-12 col-sm-6">Total Pembiayaan</span>
                                            <span class="dark col-xs-12 col-sm-6">{{ $information['total'] }}</span>
                                        </div>
                                        <div class="information-item row">
                                            <span class="col-xs-12 col-sm-6">Jangka Waktu</span>
                                            <span class="dark col-xs-12 col-sm-6">{{ $information['installment'] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </section>

    <div class="modal fade ___disclaimer-modal" id="disclaimer-modal" tabindex="-1" role="dialog" aria-labelledby="disclaimer-modal-label">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="disclaimer-modal-label">Pernyataan Pemohon</h3>
                </div>
                <div class="modal-body ___html">
                    <ol>
                        <li>
                            Dengan ini saya menyatakan, bahwa Setuju Tidak Setuju
                            <ul>
                                <li>MPM Finance berhak menyimpan, menggunakan, dan/atau memberikan data-data pribadi saya kepada group, konsultan, Pihak Ketiga lainnya yang bekerjasama dengan MPM Finance dan/atau menerima pekerjaan baik dari MPM Finance dan/atau afiliasi MPM Finance ("Pihak yang Diizinkan"), baik untuk tujuan komersil yang berkaitan dengan produk dan/atau layanan dari MPM Group dan afiliasinya dan/atau tujuan lain sepanjang tidak bertentangan dengan peraturan yang berlaku. Atas persetujuan dan penggunaan data pribadi saya kepada "Pihak yang Diizinkan", dengan ini saya menyatakan bahwa saya memahami segala konsekuensi yang terjadi di kemudian hari.</li>
                                <li>Menerima penawaran promosi produk dan/atua layanan dari MPM Group dan afiliasinya melalui sarana komunikasi pribadi, antara lain SMS, email, voice mail, telepon dan/atau menggunakan sarana komunikasi lainnya.</li>
                            </ul>
                        </li>
                        <li>Saya menyatakan telah menerima, memahami dan mengerti semua informasi yang tertera dan mengakui bahwa data yang tertulis adalah benar.</li>
                        <li>Saya memberikan kuasa kepada MPM Finance untuk memeriksa informasi tersebut dengan cara yang layak menurut MPM Finance. Bahwa data yang saya berikan adalah data yang benar, valid, dan dapat dipertanggung jawabkan secara hukum.</li>
                        <li>Dengan melengkapi informasi dan data-data sebagaimana tersebut diatas, saya menyatakan setuju dan tunduk pada ketentuan yang berlaku pada MPM Finance</li>
                    </ol>
                </div>
                <div class="modal-footer">
                    <button type="button" class="theme-btn btn-style-seven btn-rounded" data-dismiss="modal" onclick="submitForm();">Setuju, Lanjutkan Pengajuan</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.11/lodash.min.js"></script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/site/simulation.js') }}"></script>
    <script>
        function submitForm() {
            $('.___simulation-page__form').submit();
        }
    </script>
@endpush
