@extends('site.layouts.main', [ 'header' => 'product'])

@section('title', 'Verifikasi Data')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-2.jpg'),
        'title' => 'Verifikasi Data',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Produk',
                'link' => route('product'),
            ],
            [
                'title' => 'Simulasi Kredit',
                'link' => route('product'),
            ],
            [
                'title' => 'Verifikasi Data',
            ]
        ],
    ])

    <section class="checkout-page ___simulation-verification-page">
    	<div class="auto-container">
            <div class="billing-details">
                <div class="shop-form">
                    <form method="POST" action="{{ route('simulation.verify', ['calculation' => $calculation['id']]) }}">
                        @csrf
                        @method('PUT')
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="sec-title-two"><h2>Verifikasi Data</h2></div>
                        		<div class="billing-inner">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div class="field-label">Kode Verifikasi</div>
                                            <input type="text" name="verification_code" value="{{ old('verification_code') }}" placeholder="Masukkan kode verifikasi">
                                            <div class="text-help">
                                                <span>Silakan masukkan kode verifikasi yang sudah dikirimkan ke nomer <strong>{{ $calculation['phone'] }}</strong></span>
                                                <span>Tidak menerima kode verifikasi? <a class="button-resend">Kirim Ulang Kode</a></span>
                                            </div>
                                            @if ($errors->has('verification_code'))
                                                <span class="form-error">
                                                    <strong>{{ $errors->first('verification_code') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <button type="submit" class="theme-btn btn-style-seven">Lihat Hasil Hitungan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form class="form-resend" method="POST" action="{{ route('simulation.resend', ['calculation' => $calculation['id']]) }}">
                        @csrf
                        @method('PUT')
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    <script type="text/javascript">
        @if(session('status'))
            $.toast({
                heading: 'Kode Berhasil Dikirim',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/site/simulation-verification.js') }}"></script>
@endpush
