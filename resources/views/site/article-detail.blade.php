@extends('site.layouts.main', [ 'header' => 'info'])

@section('title', $article['title'])

@push('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
@endpush

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-1.jpg'),
        'title' => $article['title'],
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Informasi Pelanggan',
                'link' => route('info'),
            ],
            [
                'title' => 'Artikel',
                'link' => route('info.article'),
            ],
            [
                'title' => $article['title'],
            ]
        ],
    ])

    @include('site.elements.content', [
        'data' => $article,
        'sidelabel' => 'Artikel Terbaru',
        'items' => $articles,
    ])
@endsection
