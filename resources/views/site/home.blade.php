@extends('site.layouts.main', [ 'header' => 'home'])

@push('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    @include('site.headers.slider-subtitle', ['items' => $banners])

    @include('site.elements.products', ['words' => $encouragementWords, 'items' => $products])

    @include('site.elements.howtos')

    @include('site.elements.why')

    @if (count($promos) > 0)
        @include('site.elements.promos', ['items' => $promos])
    @endif

    @if (count($testimonies) > 0)
        @include('site.elements.testimonies', ['items' => $testimonies])
    @endif

    @include('site.elements.subscribe')
@endsection

@push('scripts')
    <script src="{{ asset('templates/frontend/js/owl.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script type="text/javascript">
        var simulationData = JSON.parse('{!! json_encode($simulations) !!}');

        $(document).ready(function(){
            new AutoNumeric('.___simulation-form .___simulation-form__input--total', {
                currencySymbol: "Rp ",
                decimalCharacter: ",",
                decimalPlaces: 0,
                digitGroupSeparator: ".",
                unformatOnSubmit: true,
            });

            $('.___promo-contents').slick({
                adaptiveHeight: true,
                variableWidth: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToShow: Math.min(3, Math.max(1, {{ count($promos)-1 }})),
                slidesToScroll: 1,
                centerMode: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 750,
                        settings: {
                            adaptiveHeight: true,
                            variableWidth: false,
                            centerMode: false,
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        });
        @if(session('status'))
            $.toast({
                heading: 'Berhasil Berlangganan',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
        @if ($errors->has('email'))
            $.toast({
                heading: 'Gagal Berlangganan',
                text: "{{ $errors->first('email') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'error',
                stack: false
            });
        @endif
    </script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/simulation.js') }}"></script>
@endpush
