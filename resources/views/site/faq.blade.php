@extends('site.layouts.main', [ 'header' => 'info'])

@section('title', 'Tanya Jawab')

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-1.jpg'),
        'title' => 'Tanya Jawab',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Informasi Pelanggan',
                'link' => route('info'),
            ],
            [
                'title' => 'Tanya Jawab',
            ]
        ],
    ])

    <section class="faq-section">
    	<div class="auto-container">
        	<div class="title">
        		<h2>Tanya Jawab</h2>
            	<div class="title">Pertanyaan-pertanyaan yang sering diajukan.</div>
            </div>
            <!-- faq Form -->
            <div class="faq-search-box">
                <form method="GET" action="{{ route('info.faq') }}">
                    <div class="form-group">
                        <input type="search" name="search" value="{{ request()->search }}" placeholder="Cari pertanyaan" required>
                        <button type="submit"><span class="icon fa fa-search"></span></button>
                    </div>
                </form>
                @if (count($faqs) == 0)
                    <div class="text faq-form-error">Tidak ada pertanyaan dengan kata kunci <span>"{{ request()->search }}"</span></div>
                @endif
            </div>

            @if (count($faqs) > 0)
                <div class="row clearfix">
                    <div class="column col-xs-12">
                        <ul class="accordion-box faq">
                            @foreach ($faqs as $faq)
                                <li class="accordion block">
                                    <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div>{{ $faq['question'] }}</div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">{{ $faq['answer'] }}</div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
