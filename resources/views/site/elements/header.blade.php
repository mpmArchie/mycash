<header class="main-header">
    <!--Header-Upper-->
    <div class="header-upper">
        <div class="clearfix">
            <div class="pull-left logo-outer">
                <div class="logo"><a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="" title=""></a></div>
            </div>

            <div class="pull-right upper-right clearfix">
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="{{ isset($header) && $header == 'home' ? 'current' : '' }}"><a href="{{ route('home') }}">Beranda</a></li>
                                <li class="{{ isset($header) && $header == 'about' ? 'current' : '' }}"><a href="{{ route('about') }}">Tentang Kami</a></li>
                                <li class="{{ isset($header) && $header == 'product' ? 'current' : '' }}"><a href="{{ route('product') }}">Produk</a></li>
                                <li class="{{ isset($header) && $header == 'contact' ? 'current' : '' }}"><a href="{{ route('contact') }}">Hubungi Kami</a></li>
                                <li class="dropdown {{ isset($header) && $header == 'info' ? 'current' : '' }}">
                                    <a href="#">Informasi Pelanggan</a>
                                    <ul>
                                        <li><a href="{{ route('info') }}">Informasi Pelanggan</a></li>
                                        <li><a href="{{ route('info.article') }}">Artikel</a></li>
                                        <li><a href="{{ route('info.terms') }}">Syarat dan Ketentuan</a></li>
                                        <li><a href="{{ route('info.faq') }}">Tanya jawab</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <!-- Main Menu End-->
                    <div class="outer-box">
                        <!--Search Box-->
                        <div class="search-box-outer">
                            <div class="dropdown">
                                @if (Auth::check())
                                    <a href="{{ Auth::user()->isAdmin() ? route('admin.home') : route('customer.home') }}" class="search-box-btn dropdown-toggle"><span class="fa fa-sign-in"></span></a>
                                @else
                                    <a class="search-box-btn dropdown-toggle" href="{{ route('login') }}"><span class="fa fa-sign-in"></span></a>
                                @endif
                            </div>
                        </div>
                        <div class="btn-box">
                            <div class="btn-box">
                                @if (Auth::check())
                                    <a href="{{ Auth::user()->isAdmin() ? route('admin.home') : route('customer.home') }}" class="theme-btn btn-style-one">Dasbor</a>
                                @else
                                    <a href="{{ route('login') }}" class="theme-btn btn-style-one">Masuk</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Header Upper-->

    <!--Sticky Header-->
    <div class="sticky-header">
        <div class="sticky-inner-container clearfix">
            <!--Logo-->
            <div class="logo pull-left">
                <a href="{{ route('home') }}" class="img-responsive"><img src="{{ asset('images/logo-small.png') }}" alt="" title=""></a>
            </div>

            <!--Right Col-->
            <div class="right-col pull-right">
                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li class="{{ isset($header) && $header == 'home' ? 'current' : '' }}"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="{{ isset($header) && $header == 'about' ? 'current' : '' }}"><a href="{{ route('about') }}">Tentang Kami</a></li>
                            <li class="{{ isset($header) && $header == 'product' ? 'current' : '' }}"><a href="{{ route('product') }}">Produk</a></li>
                            <li class="{{ isset($header) && $header == 'contact' ? 'current' : '' }}"><a href="{{ route('contact') }}">Hubungi Kami</a></li>
                            <li class="dropdown {{ isset($header) && $header == 'info' ? 'current' : '' }}">
                                <a href="#">Informasi Pelanggan</a>
                                <ul>
                                    <li><a href="{{ route('info') }}">Informasi Pelanggan</a></li>
                                    <li><a href="{{ route('info.article') }}">Artikel</a></li>
                                    <li><a href="{{ route('info.terms') }}">Syarat dan Ketentuan</a></li>
                                    <li><a href="{{ route('info.faq') }}">Tanya jawab</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- Main Menu End-->

                <!-- Main Menu End-->
                <div class="outer-box">
                    <!--Search Box-->
                    <div class="search-box-outer">
                        <div class="dropdown">
                            @if (Auth::check())
                                <a href="{{ Auth::user()->isAdmin() ? route('admin.home') : route('customer.home') }}" class="search-box-btn dropdown-toggle"><span class="fa fa-sign-in"></span></a>
                            @else
                                <a class="search-box-btn dropdown-toggle" href="{{ route('login') }}"><span class="fa fa-sign-in"></span></a>
                            @endif
                        </div>
                    </div>
                    <div class="btn-box">
                        @if (Auth::check())
                            <a href="{{ Auth::user()->isAdmin() ? route('admin.home') : route('customer.home') }}" class="theme-btn btn-style-one">Dasbor</a>
                        @else
                            <a href="{{ route('login') }}" class="theme-btn btn-style-one">Masuk</a>
                        @endif
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!--End Sticky Header-->

</header>
