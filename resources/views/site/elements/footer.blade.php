<footer class="main-footer">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">
                <!--Big column-->
                <div class="big-column col-md-7 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--Footer Column-->
                        <div class="footer-column col-md-8 col-sm-6 col-xs-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo">
                                    <a href="{{ route('home') }}"><img src="{{ asset('images/footer-logo.png') }}" alt="" /></a>
                                </div>
                                <div class="text">
                                    <p>PT Mitra Pinasthika Mustika Finance</p>
                                    <p style="margin-bottom: 0px;">Terdaftar dan diawasi oleh:</p>
                                    <img srcset="{{ asset('images/icon-ojk.png') }}, {{ asset('images/icon-ojk@2x.png') }} 2x, {{ asset('images/icon-ojk@3x.png') }} 3x" alt="OJK" />
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h2>Perusahaan</h2>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="{{ route('about') }}">Tentang Kami</a></li>
                                        <li><a href="https://www.mpm-finance.com/content/acc/hubungi">Jaringan MPMF</a></li>
                                        <li><a href="{{ route('contact') }}">Hubungi Kami</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Big column-->
                <div class="big-column col-md-5 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h2>Produk</h2>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="{{ route('product') }}">Produk</a></li>
                                        <li><a href="{{ route('info') . '#payment' }}">Cara Pembayaran</a></li>
                                        <li><a href="{{ route('info.terms') }}">Syarat dan Ketentuan</a></li>
                                        <li><a href="{{ route('info.faq') }}">Tanya Jawab</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget info-widget">
                                <h2>Social Media</h2>
                                <ul class="list">
                                    <li><a href="https://www.facebook.com/mpmfinanceofficial" target="_blank" rel="noopener">Facebook</a></li>
                                    <li><a href="https://www.instagram.com/mpm_finance" target="_blank" rel="noopener">Instagram</a></li>
                                    <li><a href="https://www.linkedin.com/company/mpmfinance" target="_blank" rel="noopener">LinkedIn </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Bottom-->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="clearfix">
                <div class="pull-left">
                    <div class="copyright">MPM Finance &copy; 2019. All right reserved.</div>
                </div>
            </div>
        </div>
    </div>
</footer>
