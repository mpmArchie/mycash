<section class="featured-section bg-white ___section">
    <div class="auto-container">
        <div class="sec-title no-border centered">
            <h2>Informasi Cara Menggunakan<br />MyCash Dalam 3 Langkah</h2>
        </div>
        <div class="row clearfix ___equal-column-height">
            <!--Block-->
            <div class="featured-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="{{ asset('images/icon-fill-simulation.svg') }}" alt="Step 1" />
                    </div>
                    <h3>Coba Simulasi Kredit</h3>
                    <div class="text">Isi simulasi kredit dengan informasi pinjaman yang ingin Anda lakukan.</div>
                </div>
            </div>
            <!--Block-->
            <div class="featured-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="{{ asset('images/icon-fill-data.svg') }}" alt="Step 2" />
                    </div>
                    <h3>Isi Data Diri Anda</h3>
                    <div class="text">Lengkapi form dengan data diri Anda.</div>
                </div>
            </div>
            <!--Block-->
            <div class="featured-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="{{ asset('images/icon-credit.svg') }}" alt="Step 3" />
                    </div>
                    <h3>Ajukan Kredit</h3>
                    <div class="text">Tim kami akan melakukan pengecekan dan memproses pengajuan kredit Anda.</div>
                </div>
            </div>
        </div>
    </div>
</section>
