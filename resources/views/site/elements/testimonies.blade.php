<section class="testimonial-section ___section" style="background-image:url({{ asset('images/bg-testimonies.jpg') }})">
    <div class="auto-container" style="padding: 0 34px;">
        <div class="sec-title light centered">
            <h2>Testimoni</h2>
        </div>
        <div class="two-item-carousel owl-carousel owl-theme">
            @foreach ($items as $item)
                <div class="testimonial-block">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{ asset($item['image']['uri']) }}" alt="" />
                        </div>
                        <div class="content">
                            <h3>{{ $item['name'] }}</h3>
                            <div class="text">{{ $item['content'] }}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
