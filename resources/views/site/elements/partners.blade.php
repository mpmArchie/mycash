<section class="clients-section ___section">
    <div class="auto-container">
        <div class="sec-title no-border centered ___m-0">
            <h2>Partner Kami</h2>
        </div>
        <div class="sponsors-outer">
            <ul class="sponsors-carousel owl-carousel owl-theme">
                @foreach ($items as $item)
                    <li class="slide-item"><figure class="image-box"><a href="{{ $item['website'] }}" target="_blank" rel="noopener"><img src="{{ $item['image']['uri'] }}" alt="{{ $item['name'] }}"></a></figure></li>
                @endforeach
            </ul>
        </div>

    </div>
</section>
