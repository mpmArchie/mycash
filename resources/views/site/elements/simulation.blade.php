<div class="___simulation">
    <h3>Simulasi Kredit</h3>
    <div class="shop-form">
        <form id="simulation-form" class="___simulation-form" method="POST" action="{{ route('simulation.calculate') }}" autocomplete="off">
            @csrf
            <div class="row clearfix">
                <div class="form-group col-xs-12">
                    <div class="field-label">Jenis Agunan/Produk</div>
                    <select class="___simulation-form__input--simulation" name="simulation" required>
                        <option value="" hidden>Pilih jenis agunan/produk</option>
                        @foreach ($items as $item)
                            <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <div class="field-label">Tahun Kendaraan</div>
                    <select class="___simulation-form__input--year" name="year" required>
                        <option value="" hidden>Pilih tahun</option>
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <div class="field-label">Total Pembiayaan</div>
                    <input class="___simulation-form__input--total" type="text" name="total" value="" placeholder="Masukkan total biaya dalam rupiah" required>
                </div>
                <div class="form-group col-xs-12">
                    <div class="field-label">Jangka Waktu</div>
                    <select class="___simulation-form__input--installment" name="installment" required>
                        <option value="" hidden>Pilih waktu</option>
                    </select>
                </div>
                <div class="form-group text-right col-xs-12">
                    <button type="submit" class="theme-btn btn-style-seven">Hitung Angsuran</button>
                </div>
            </div>
        </form>
    </div>
</div>
