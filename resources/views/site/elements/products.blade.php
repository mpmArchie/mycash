<section class="services-page-section ___section{{ isset($words) ? '' : ' no-border' }}">
    <div class="auto-container">
        @isset($words)
            <div class="sec-title centered">
                <h2>{!! $words !!}</h2>
            </div>
        @endisset
        <div class="services-outer-blocks">
            <div class="services-blocks">
                <div class="clearfix ___equal-column-height">
                    @foreach ($items as $item)
                        <div class="services-block-five col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image-container"><img src="{{ asset($item['image']['uri']) }}" alt="" /></div>
                                <h2><a href="{{ route('product.detail', ['slug' => $item['slug']]) }}">{!! $item['name'] !!}</a></h2>
                                <div class="text">{{ $item['summary'] }}</div>
                                <a href="{{ route('product.detail', ['slug' => $item['slug']]) }}" class="take-now">PELAJARI LEBIH LANJUT</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
