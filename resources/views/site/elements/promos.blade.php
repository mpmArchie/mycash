<section class="promo-section ___section">
    <div class="auto-container">
        <div class="sec-title no-border centered ___m-0">
            <h2>Promo</h2>
        </div>
        <div class="clearfix ___promo-contents">
            @foreach ($items as $item)
                <div class="___promo-item"><a href="{{ route('promo.detail', ['slug' => $item['slug']]) }}"><img src="{{ asset($item['image']['uri']) }}"></a></div>
            @endforeach
        </div>
    </div>
</section>
