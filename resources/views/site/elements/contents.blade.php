<section class="blog-page-section">
    <div class="auto-container">
        <div class="row clearfix ___equal-column-height">
            @foreach ($items as $item)
                <div class="news-block-two col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <a href="{{ route('info.article.detail', ['id' => $item['id']]) }}"><img src="{{ $item['image'] }}" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <div class="lower-box">
                                <h3><a href="{{ route('info.article.detail', ['id' => $item['id']]) }}">{{ $item['title'] }}</a></h3>
                                <div class="text">{{ $item['summary'] }}</div>
                                <a href="{{ route('info.article.detail', ['id' => $item['id']]) }}" class="theme-btn btn-style-seven">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
