<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <div class="our-blog padding-right">
                    <div class="news-block-two">
                        <div class="inner-box">
                            <div class="lower-content">
                                <img src="{{ $data['image'] }}" alt="Image" />
                                <h2>{{ $data['title'] }}</h2>
                                <div class="text">{!! $data['content'] !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <aside class="sidebar default-sidebar">
                    <div class="sidebar-widget popular-posts">
                        <div class="sidebar-title"><h2>{{ $sidelabel }}</h2></div>
                        @foreach ($items as $item)
                            <article class="post">
                                <div class="text"><a href="{{ route('info.article.detail', ['id' => $item['id']]) }}">{{ $item['title'] }}</a></div>
                                <div class="post-info">{{ \Carbon\Carbon::parse($item['updated_at'])->locale('id')->isoFormat('D MMM YYYY') }}</div>
                            </article>
                        @endforeach
                    </div>
                </aside>
            </div>

        </div>
    </div>
</div>
