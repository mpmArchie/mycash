<section class="featured-section ___section" style="padding-bottom: 40px;">
    <div class="auto-container">
        <div class="row clearfix ___equal-column-height">
            <!--Block-->
            <div class="featured-block orange col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="{{ asset('images/icon-trusted.svg') }}" alt="Trusted" />
                    </div>
                    <h3>Terpercaya</h3>
                </div>
            </div>
            <!--Block-->
            <div class="featured-block orange col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="{{ asset('images/icon-easy.svg') }}" alt="Easy" />
                    </div>
                    <h3>Mudah</h3>
                </div>
            </div>
            <!--Block-->
            <div class="featured-block orange col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="{{ asset('images/icon-fast.svg') }}" alt="Fast" />
                    </div>
                    <h3>Cepat</h3>
                </div>
            </div>
        </div>
    </div>
</section>
