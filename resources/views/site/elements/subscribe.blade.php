<section class="subscribe-section ___section" style="background-image:url({{ asset('images/bg-subscribe.jpg') }}">
    <div class="auto-container">
        <div class="inner-container">
            <h2>Berlangganan Newsletter</h2>
            <div class="text">Dapatkan informasi terbaru untuk kebutuhan finansial Anda</div>
            <div class="subscribe-form">
                <form method="POST" action="{{ route('home.subscribe') }}">
                    @csrf
                    <div class="form-group">
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Alamat email Anda" required>
                        <button type="submit" class="theme-btn">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
