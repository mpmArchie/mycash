@extends('site.layouts.main', [ 'header' => 'info'])

@section('title', 'Artikel')

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-1.jpg'),
        'title' => 'Artikel',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Informasi Pelanggan',
                'link' => route('info'),
            ],
            [
                'title' => 'Artikel',
            ]
        ],
    ])

    @include('site.elements.contents', ['items' => $articles])
@endsection
