@extends('site.layouts.main', [ 'header' => 'product'])

@section('title', 'Produk')

@push('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
@endpush

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-product.jpg'),
        'title' => 'Produk',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Produk',
            ]
        ],
    ])

    <section class="simulation-product-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-8">
                    <h1>{!! $encouragementWords !!}</h1>
                </div>
                <div class="col-md-4 ___simulation-container">
                    @include('site.elements.simulation', ['items' => $simulations])
                </div>
            </div>
        </div>
    </section>

    @include('site.elements.products', ['words' => null, 'items' => $products])

    @include('site.elements.why')

    @include('site.elements.promos', ['items' => $promos])

    @if (count($partners) > 0)
        @include('site.elements.partners', ['items' => $partners])
    @endif
@endsection

@push('scripts')
    <script src="{{ asset('templates/frontend/js/owl.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script type="text/javascript">
        var simulationData = JSON.parse('{!! json_encode($simulations) !!}');

        $(document).ready(function(){
            new AutoNumeric('.___simulation-form .___simulation-form__input--total', {
                currencySymbol: "Rp ",
                decimalCharacter: ",",
                decimalPlaces: 0,
                digitGroupSeparator: ".",
                unformatOnSubmit: true,
            });

            $('.___promo-contents').slick({
                adaptiveHeight: true,
                variableWidth: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToShow: Math.min(3, Math.max(1, {{ count($promos)-1 }})),
                slidesToScroll: 1,
                centerMode: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 750,
                        settings: {
                            adaptiveHeight: true,
                            variableWidth: false,
                            centerMode: false,
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        });
    </script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/simulation.js') }}"></script>
@endpush
