@extends('site.layouts.main', [ 'header' => 'product'])

@section('title', $product['escaped_name'])

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => $product['banner']['uri'],
        'title' => $product['escaped_name'],
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Produk',
                'link' => route('product'),
            ],
            [
                'title' => $product['escaped_name'],
            ]
        ],
    ])

    <section class="product-detail-section ___section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-8">
                    <h2>{{ $product['escaped_name'] }}</h2>
                    <div class="text ___html">
                        {!! $product['description'] !!}
                    </div>
                </div>
                <div class="col-md-4">
                    @include('site.elements.simulation', ['items' => $simulations])
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script type="text/javascript">
        var simulationData = JSON.parse('{!! json_encode($simulations) !!}');

        $(document).ready(function(){
            new AutoNumeric('.___simulation-form .___simulation-form__input--total', {
                currencySymbol: "Rp ",
                decimalCharacter: ",",
                decimalPlaces: 0,
                digitGroupSeparator: ".",
                unformatOnSubmit: true,
            });
        });
    </script>
@endpush

@push('scripts-app')
    <script src="{{ asset('js/simulation.js') }}"></script>
@endpush
