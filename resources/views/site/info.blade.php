@extends('site.layouts.main', [ 'header' => 'info'])

@section('title', 'Informasi Pelanggan')

@push('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
@endpush

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-1.jpg'),
        'title' => 'Informasi Pelanggan',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Informasi Pelanggan',
            ]
        ],
    ])

    @if (count($promos) > 0)
        @include('site.elements.promos', ['items' => $promos])
    @endif

    <section id="payment" class="payment-section ___section">
    	<div class="auto-container">
        	<div class="sec-title no-border centered ___m-0">
                <h2>Cara Pembayaran</h2>
                <div class="text">Bayar kredit Anda melalui channel pembayaran dibawah ini</div>
            </div>
            <ul class="accordion-box">
                @foreach ($payments as $payment)
                    <li class="accordion block wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="acc-btn">
                            <div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div>
                            @isset($payment['image'])
                                <img src="{{ $payment['image']['uri'] }}" alt="" />
                            @endisset
                            @isset($payment['name'])
                                {{ $payment['name'] }}
                            @endisset
                        </div>
                        <div class="acc-content">
                            <div class="content">
                                <div class="text ___html">{!! $payment['description'] !!}</div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="{{ asset('templates/frontend/js/wow.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.___promo-contents').slick({
                adaptiveHeight: true,
                variableWidth: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToShow: Math.min(3, Math.max(1, {{ count($promos)-1 }})),
                slidesToScroll: 1,
                centerMode: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 750,
                        settings: {
                            adaptiveHeight: true,
                            variableWidth: false,
                            centerMode: false,
                            slidesToShow: 1,
                        }
                    },
                ]
            });

            if(window.location.hash && $(window.location.hash).length > 0) {
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top - 20
                }, 'slow');
            }
        });
    </script>
@endpush
