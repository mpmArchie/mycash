@extends('site.layouts.main', [ 'header' => 'promo'])

@section('title', $promo['name'])

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-1.jpg'),
        'title' => $promo['name'],
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Promo',
                'link' => route('home'),
            ],
            [
                'title' => $promo['name'],
            ]
        ],
    ])

    <section class="promo-detail-section ___section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xs-12">
                    <div class="text-center">
                        <img src="{{ $promo['image']['uri'] }}" alt="Promo" />
                    </div>
                    <h2>{{ $promo['name'] }}</h2>
                    <div class="text ___html">
                        {!! $promo['description'] !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
