@extends('site.layouts.main', [ 'header' => 'product'])

@section('title', 'Hasil Penghitungan')

@push('scripts-head')
   <script> gtag('event', 'conversion', {'send_to': 'AW-754614875/8QFzCIrP0ZcBENuE6ucC'}); </script>
@endpush


@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-2.jpg'),
        'title' => 'Hasil Penghitungan',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Produk',
                'link' => route('product'),
            ],
            [
                'title' => 'Simulasi Kredit',
                'link' => route('product'),
            ],
            [
                'title' => 'Hasil Penghitungan',
            ]
        ],
    ])

    <section class="checkout-page ___simulation-result-page">
    	<div class="auto-container">
            <div class="billing-details">
                <div class="shop-form">
                    <form method="POST" action="{{ route('simulation.request', ['calculation' => $calculation['id']]) }}">
                        @csrf
                        @method('PUT')
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="sec-title-two"><h2>Hasil Penghitungan</h2></div>
                        		<div class="billing-inner">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div>Hasil penghitungan angsuran dengan total pembiayaan {{ str_currency($calculation['total']) }} selama {{ $installment['duration'] }} bulan</div>
                                            <div class="calculation-result">{{ str_currency($result) }} / bulan</div>
                                        </div>
                                        @if ($calculation->area != '')
                                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                <button type="submit" class="theme-btn btn-style-seven">Ajukan Pinjaman</button>
                                            </div>
                                        @else
                                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                <button type="button" class="theme-btn btn-style-seven btn-disabled">Ajukan Pinjaman</button><br />
                                                <small class="btn-disabled-hint">Mohon maaf, cabang kami belum tersedia di area Anda. Anda belum dapat melakukan pengajuan kredit.</small>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    <script type="text/javascript">
        @if(session('status'))
            $.toast({
                heading: 'Pinjaman Diajukan',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'bottom-center',
                icon: 'success',
                stack: false
            });
        @endif
        @if(session('status-error'))
            $.toast({
                heading: 'Permintaan Gagal',
                text: "{{ session('status-error') }}",
                hideAfter : 7000,
                position: 'bottom-center',
                icon: 'error',
                stack: false
            });
        @endif
    </script>
@endpush
