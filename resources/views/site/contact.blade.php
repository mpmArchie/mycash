@extends('site.layouts.main', [ 'header' => 'contact'])

@section('title', 'Hubungi Kami')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-2.jpg'),
        'title' => 'Hubungi Kami',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Hubungi Kami',
            ]
        ],
    ])

    <section class="contact-page-section">
    	<div class="auto-container">
        	<div class="title">
            	<h2>Hubungi Kami</h2>
            </div>
            <div class="row clearfix">
            	<!--Form Column-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                        <!-- Contact Form -->
                        <div class="contact-form">
                            <!--Comment Form-->
                            <form method="POST" action="{{ route('contact.submit') }}">
                                @csrf
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="name" placeholder="Nama" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                            <div class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="email" name="email" placeholder="Alamat email" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <input type="text" name="phone" placeholder="Nomor telepon" value="{{ old('phone') }}">
                                        @if ($errors->has('phone'))
                                            <div class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="message" placeholder="Pesan">{{ old('message') }}</textarea>
                                        @if ($errors->has('message'))
                                            <div class="error">{{ $errors->first('message') }}</div>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        {!! htmlFormSnippet() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <div class="error">{{ $errors->first('g-recaptcha-response') }}</div>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn btn-style-seven" type="submit" name="submit-form">Kirimkan Pesan</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                        <!--End Contact Form -->
                    </div>
                </div>
                <!--Info Column-->
                <div class="info-column col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<ul>
                            <li><h3>MPM Finance</h3></li>
                        	<li><span>Alamat:</span>Gedung Lippo Kuningan Lantai 23 & 25<br>Jl. H.R. Rasuna Said Kav. B-12<br />Jakarta 12910</li>
                            <li><span>Telepon:</span><strong>1500309</strong></li>
                            <li>
                                <span>Social:</span>
                                <div class="socmed">
                                    <a class="socmed-item" href="https://www.facebook.com/mpmfinanceofficial" target="_blank" rel="noopener"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                    <a class="socmed-item" href="https://www.instagram.com/mpm_finance" target="_blank" rel="noopener"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a class="socmed-item" href="https://www.linkedin.com/company/mpmfinance" target="_blank" rel="noopener"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts-head')
    {!! htmlScriptTagJsApi() !!}
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    <script type="text/javascript">
        @if(session('status'))
            $.toast({
                heading: 'Pesan Berhasil Dikirim',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
