@extends('site.layouts.main', [ 'header' => 'info'])

@section('title', 'Syarat dan Ketentuan')

@section('content')
    @include('site.headers.breadcrumbs', [
        'background' => asset('images/banner-default-1.jpg'),
        'title' => 'Syarat dan Ketentuan',
        'breadcrumbs' => [
            [
                'title' => 'Beranda',
                'link' => route('home'),
            ],
            [
                'title' => 'Informasi Pelanggan',
                'link' => route('info'),
            ],
            [
                'title' => 'Syarat dan Ketentuan',
            ]
        ],
    ])

    <section class="terms-section ___section">
    	<div class="auto-container">
            <h2>Syarat dan Ketentuan<br />MyCash</h2>
            <div class="text ___html">
                {!! $termsConditions !!}
            </div>
        </div>
    </section>
@endsection
