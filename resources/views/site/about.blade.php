@extends('site.layouts.main', [ 'header' => 'about'])

@section('title', 'Tentang Kami')

@section('content')
    @include('site.headers.slider', ['items' => $banners])

    <section class="___about-page ___section">
    	<div class="auto-container">
            <div class="row clearfix">
                <div class="col-xs-12">
                    <h2>Tentang Kami</h2>
                    <p>Ditengah era yang membutuhkan mobilitas tinggi, MPM Finance senantiasa berupaya menjadi yang terdepan dalam menyikapi perubahan terutama dalam memberikan solusi pembiayaan terbaik bagi setiap konsumen.</p>
                    <h3>Profil Perusahaan</h3>
                    <p>PT Mitra Pinasthika Mustika Finance merupakan sebuah perusahaan multifinance yang bergerak dalam bidang pembiayaan modal kerja, multiguna dan investasi pada sektor pembiayaan konsumen untuk kendaraan bermotor roda empat, roda dua dan multiguna. MPM Finance juga bergerak dalam sektor pembiayaan korporasi melalui produk sewa pembiayaan untuk alat berat, mesin maupun property.</p>
                    <p>Selain memberikan produk pembiayaan yang dirancang sesuai kebutuhan konsumen, MPM Finance juga memastikan kewajaran dan keterjangkauan harga serta faktor kenyamanan yang terus ditingkatkan. Memiliki lebih dari 100 cabang yang tersebar di beberapa wilayah di Indonesia dan didukung oleh lebih dari 2000 karyawan yang berpengalaman dan berdedikasi tingg, MPM Finance berkomitmen untuk terus menyediakan kecepatan dan kemudahan bagi kebutuhan pembiayaan masyarakat Indonesia.</p>
                    <p>Melalui tagline ‘Fast Friendly Financing’ MPM Finance berkomitmen untuk selalu menjadi perusahaan penyedia jasa pembiayaan bagi seluruh masyarakat Indonesia dengan pelayanan yang cepat dan bersahabat.</p>
                    <h3>Visi</h3>
                    <p>Menjadi perusahaan ternama yang digemari setiap insan yang diciptakan oleh sumber daya manusia yang terampil dan penuh semangat dibawah para pemimpin yang berwibawa dan bersahaja</p>
                    <h3>Misi</h3>
                    <p>Menyediakan produk dan layanan keuangan berkualitas prima dan ramah sehingga menyenangkan para konsumen.<br/><span class="quote">"Through our vision and mission, we continuously provide the best financial solutions for every customer"</span></p>
                    <h3>Kenapa MPM Finance?</h3>
                    <ul>
                        <li>Berpengalaman lebih dari 27 tahun dalam menyediakan jasa pembiayaan di Indonesia</li>
                        <li>Jaringan luas dengan lebih dari 100 kantor cabang yang tersebar di seluruh Indonesia</li>
                        <li>Produk pembiayaan yang beragam dengan struktur yang kompetitif</li>
                        <li>Kemudahan pembayaran angsuran melalui jaringan Indomaret, Alfamart, Lawson, Pos Indonesia, ATM BCA dan Tokopedia</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @include('site.elements.products', ['words' => $encouragementWords, 'items' => $products])
@endsection
