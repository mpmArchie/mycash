@extends('layouts.full')

@section('title', 'Lupa Kata Sandi')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-min-full-height d-flex justify-content-center flex-column align-items-center">
        <div class="login-center">
            <div class="navbar-header text-center mt-2 mb-4">
                <a href="{{ route('home') }}">
                    <img alt="" src="{{ asset('images/logo-light.png') }}">
                </a>
            </div>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="form-group">
                    <label for="email">Email</label>
                    <input
                        type="email"
                        placeholder="Masukkan alamat email Anda"
                        class="form-control form-control-line{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        id="email"
                        value="{{ old('email') }}"
                        required
                    >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-lg btn-primary text-uppercase fs-12 fw-600" type="submit">Kirimkan Link Pembaruan Kata Sandi</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    <script>
        @if(Session::has('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ Session::get('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
