@extends('layouts.full')

@section('title', 'Pembaruan Kata Sandi')

@section('content')
    <div class="container-min-full-height d-flex justify-content-center flex-column align-items-center">
         @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="login-center">
            <div class="navbar-header text-center mt-2 mb-4">
                <a href="{{ route('home') }}">
                    <img alt="" src="{{ asset('images/logo-light.png') }}">
                </a>
            </div>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input
                        type="email"
                        placeholder="Masukkan alamat email Anda"
                        class="form-control form-control-line{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        id="email"
                        value="{{ $email ?? old('email') }}"
                        required
                    >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Kata Sandi Baru</label>
                    <input
                        type="password"
                        placeholder="Masukkan kata sandi baru Anda"
                        id="password"
                        name="password"
                        class="form-control form-control-line{{ $errors->has('password') ? ' is-invalid' : '' }}"
                        required
                    >
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password-confirm">Konfirmasi Kata Sandi Baru</label>
                    <input
                        type="password"
                        placeholder="Ulangi kata sandi baru Anda"
                        id="password-confirm"
                        name="password_confirmation"
                        class="form-control form-control-line"
                        required
                    >
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-lg btn-primary text-uppercase fs-12 fw-600" type="submit">Perbarui Kata Sandi</button>
                </div>
            </form>
        </div>
    </div>
@endsection
