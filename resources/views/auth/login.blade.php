@extends('layouts.full')

@section('title', 'Masuk')

@section('content')
    <div class="container-min-full-height d-flex justify-content-center align-items-center">
        <div class="login-center">
            <div class="navbar-header text-center mt-2 mb-4">
                <a href="{{ route('home') }}">
                    <img alt="" src="{{ asset('images/logo-light.png') }}">
                </a>
            </div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email">Email</label>
                    <input
                        type="email"
                        placeholder="Masukkan alamat email Anda"
                        class="form-control form-control-line{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        id="email"
                        value="{{ old('email') }}"
                        required
                    >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Kata Sandi</label>
                    <input
                        type="password"
                        placeholder="Masukkan kata sandi Anda"
                        id="password"
                        name="password"
                        class="form-control form-control-line{{ $errors->has('password') ? ' is-invalid' : '' }}"
                        required
                    >
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-lg btn-primary text-uppercase fs-12 fw-600" type="submit">Masuk</button>
                </div>
                <div class="form-group no-gutters mb-0">
                    <div class="col-md-12 d-flex">
                        <div class="checkbox checkbox-primary mr-auto mr-0-rtl ml-auto-rtl">
                            <label class="d-flex">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span class="label-text">Ingat Saya</span>
                            </label>
                        </div><a href="{{ route('password.request') }}" id="to-recover" class="my-auto pb-2 text-right"><i class="material-icons mr-2 fs-18">lock</i> Lupa Kata Sandi?</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
