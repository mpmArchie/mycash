@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'account',
    ],
    'title' => 'Ubah Profil',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Pengaturan Akun',
            'href' => route('customer.account'),
        ],
        [
            'title' => 'Ubah Profil',
        ]
    ],
])

@section('title', 'Ubah Profil')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___account-profile-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Ubah Profil Anda',
                'addons' => null
            ])
                <h6 class="fw-700 mr-b-20">Gambar Profil</h6>
                <div class="___account-page__profile__image flex-column flex-sm-row">
                    <img class="img-thumbnail" src="{{ $user['photo']['uri'] }}" alt="">
                    <form id="form-upload" style="display: none;" enctype="multipart/form-data" method="POST" action="{{ route('customer.account.photo.update') }}">
                        @csrf
                        @method('PUT')
                        <input id="form-upload-input" type="file" name="photo" accept="image/x-png,image/jpeg" />
                    </form>
                    <button class="btn btn-rounded btn-primary ripple ml-0 ml-sm-4 mt-3 mt-sm-0" onclick="onUploadClick();">Ganti Gambar Profil</button>
                </div>
                <hr class="mr-tb-20">
                <h6 class="fw-700 mr-b-20">Data Diri</h6>
                <form class="row" method="POST" action="{{ route('customer.account.profile.update') }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'name',
                            'name' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA SESUAI KTP',
                            'placeholder' => 'Masukkan nama sesuai KTP',
                            'value' => old('name', $user['name']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'phone',
                            'name' => 'phone',
                            'horizontal' => true,
                            'label' => 'NOMOR PONSEL',
                            'placeholder' => 'Masukkan nomor handphone',
                            'value' => old('phone', $user['phone']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'horizontal' => true,
                            'label' => 'ALAMAT EMAIL',
                            'value' => $user['email'],
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'horizontal' => true,
                            'label' => 'NOMOR KTP',
                            'value' => $user['ktp'] ?? '0000000000000000',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        <h5 class="box-title mr-b-10">DOMISILI</h5>
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'address',
                            'name' => 'address',
                            'horizontal' => true,
                            'label' => 'ALAMAT',
                            'placeholder' => 'Masukkan Alamat',
                            'value' => old('address', $user['address']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'rt',
                            'name' => 'rt',
                            'horizontal' => true,
                            'label' => 'RT',
                            'placeholder' => 'Masukkan RT',
                            'value' => old('rt', $user['rt']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'rw',
                            'name' => 'rw',
                            'horizontal' => true,
                            'label' => 'RW',
                            'placeholder' => 'Masukkan RW',
                            'value' => old('rw', $user['rw']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'kelurahan',
                            'name' => 'kelurahan',
                            'horizontal' => true,
                            'label' => 'KELURAHAN',
                            'placeholder' => 'Masukkan kelurahan',
                            'value' => old('kelurahan', $user['kelurahan']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'kecamatan',
                            'name' => 'kecamatan',
                            'horizontal' => true,
                            'label' => 'KECAMATAN',
                            'placeholder' => 'Masukkan kecamatan',
                            'value' => old('kecamatan', $user['kecamatan']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'city',
                            'name' => 'city',
                            'horizontal' => true,
                            'label' => 'KOTA',
                            'placeholder' => 'Masukkan kota',
                            'value' => old('city', $user['city']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'postcode',
                            'name' => 'postcode',
                            'horizontal' => true,
                            'label' => 'KODE POS',
                            'placeholder' => 'Masukkan kode pos',
                            'value' => old('postcode', $user['postcode']),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Ubah Data Diri</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script>
        function onUploadClick() {
            $('#form-upload-input').click();
            $('#form-upload-input').on('change', () => {
                $('#form-upload').submit();
            });
        }

        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif

        @if($errors->has('photo'))
            $.toast({
                heading: 'Permintaan Gagal',
                text: "{{ $errors->first('photo') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'error',
                stack: false
            });
        @endif
    </script>
@endpush
