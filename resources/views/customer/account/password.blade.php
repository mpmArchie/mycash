@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'account',
    ],
    'title' => 'Ubah Kata Sandi',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Pengaturan Akun',
            'href' => route('customer.account'),
        ],
        [
            'title' => 'Ubah Kata Sandi',
        ]
    ],
])

@section('title', 'Ubah Kata Sandi')

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Ubah Kata Sandi Anda',
                'addons' => null
            ])
                <form class="row" method="POST" action="{{ route('customer.account.password.update') }}">
                    @csrf
                    @method('PUT')
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'password-old',
                            'name' => 'password_old',
                            'horizontal' => true,
                            'label' => 'KATA SANDI LAMA',
                            'placeholder' => 'Masukkan kata sandi lama Anda',
                            'type' => 'password',
                            'value' => old('password_old'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'password',
                            'name' => 'password',
                            'horizontal' => true,
                            'label' => 'KATA SANDI BARU',
                            'placeholder' => 'Masukkan kata sandi baru Anda',
                            'type' => 'password',
                            'value' => old('password'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'password-confirmation',
                            'name' => 'password_confirmation',
                            'horizontal' => true,
                            'label' => 'ULANGI KATA SANDI BARU',
                            'placeholder' => 'Masukkan kembali kata sandi baru Anda',
                            'type' => 'password',
                            'value' => old('password_confirmation'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Ubah Kata Sandi</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
