@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'credit',
    ],
    'title' => 'Histori Transaksi',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Akun Kredit',
            'href' => route('customer.credit'),
        ],
        [
            'title' => 'Histori Transaksi',
        ]
    ],
])

@section('title', 'Histori Transaksi')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___transaction-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Histori Transaksi Anda',
                'addons' => null
            ])
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        @include('elements.forms.select', [
                            'id' => 'agreement-number',
                            'name' => 'agreement_number',
                            'label' => 'NOMOR KONTRAK',
                            'placeholder' => 'Pilih nomor kontrak',
                            'value' => $agreement,
                            'options' => $agreements->map(function ($agreement) {
                                return [
                                    'value' => $agreement['agreement_number'],
                                    'label' => $agreement['agreement_number'],
                                ];
                            }),
                        ])
                    </div>
                </div>
                <div class="___table">
                    <table class="table table-bordered ___table__transaction">
                        <thead>
                            <tr>
                                <th>Tanggal Transaksi</th>
                                <th>Angsuran Ke-</th>
                                <th>Cara Pembayaran</th>
                                <th>Jumlah Angsuran</th>
                                <th>Denda Keterlambatan</th>
                                <th>Pokok Hutang</th>
                                <th>Sisa Hutang</th>
                                <th>Tanggal Jatuh Tempo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($transactions) == 0)
                                <tr><td class="empty" colspan="8">Tidak ada data</td></tr>
                            @endif
                            @foreach ($transactions as $transaction)
                                <tr>
                                    <td>{{ \Carbon\Carbon::createFromFormat('d/m/Y', $transaction['transaction_date'])->locale('id')->isoFormat('D MMMM YYYY') }}</td>
                                    <td>{{ $transaction['installment'] }}</td>
                                    <td>{{ $transaction['payment_method'] }}</td>
                                    <td>{{ str_currency($transaction['installment_amount']) }}</td>
                                    <td>{{ str_currency($transaction['installment_charge']) }}</td>
                                    <td>{{ str_currency($transaction['total']) }}</td>
                                    <td>{{ str_currency($transaction['total_outstanding']) }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('d/m/Y', $transaction['due_date'])->locale('id')->isoFormat('D MMMM YYYY') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(() => {
            $('#agreement-number').on('change', function () {
                var agreement = this.value;
                window.location.href = `{{ route('customer.credit.transaction') }}/${agreement}`;
            });
        });
    </script>
@endpush
