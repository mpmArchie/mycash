@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'credit',
    ],
    'title' => 'Histori Aktivitas',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Akun Kredit',
            'href' => route('customer.credit'),
        ],
        [
            'title' => 'Histori Aktivitas',
        ]
    ],
])

@section('title', 'Histori Aktivitas')

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Histori Aktivitas Anda',
                'addons' => null
            ])
                <div class="___table">
                    <table class="table table-bordered ___table__activity">
                        <thead>
                            <tr>
                                <th>Aktivitas</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($activities) == 0)
                                <tr><td class="empty" colspan="3">Tidak ada data</td></tr>
                            @endif
                            @foreach ($activities as $activity)
                                <tr>
                                    <td>{{ $activity['name'] }}</td>
                                    <td>{{ $activity['date'] }}</td>
                                    <td><a href="{{ route('customer.credit.activity.detail', ['activity' => $activity['id']]) }}">Lihat Detail</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>
@endsection
