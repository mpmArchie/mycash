@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'credit',
    ],
    'title' => 'Status Pengajuan Baru',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Akun Kredit',
            'href' => route('customer.credit'),
        ],
        [
            'title' => 'Status Pengajuan Baru',
        ]
    ],
])

@section('title', 'Status Pengajuan Baru')

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @foreach ($agreements as $agreement)
                @component('elements.container', [
                    'title' => 'Status Pengajuan Baru Anda',
                    'addons' => null
                ])
                    @include('elements.card-request', ['agreement' => $agreement])
                @endcomponent
            @endforeach
        </div>
    </div>
@endsection
