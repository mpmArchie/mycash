@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'credit',
    ],
    'title' => 'Detail Aktivitas',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Akun Kredit',
            'href' => route('customer.credit'),
        ],
        [
            'title' => 'Histori Aktivitas',
            'href' => route('customer.credit.activity'),
        ],
        [
            'title' => 'Detail Aktivitas',
        ]
    ],
])

@section('title', 'Detail Aktivitas')

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => $activity['name'] . ' - ' . $activity['date'],
                'addons' => null
            ])
                <div>{{ $activity['detail'] }}</div>
                <h5 class="box-title mr-t-20 mr-b-5">DETAIL AKTIVITAS ANDA</h5>
                <div class="row">
                    <div class="col-12 col-md-10">
                        @foreach (json_decode($activity['data']) as $key => $value)
                            <div class="row mr-t-10">
                                <div class="col-xs-12 col-md-6 col-lg-4 text-uppercase">{{ $key }}</div>
                                <div class="col text-black">{{ $value }}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endcomponent
        </div>
    </div>
@endsection
