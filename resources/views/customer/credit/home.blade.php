@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'credit',
    ],
    'title' => 'Akun Kredit',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Akun Kredit',
        ]
    ],
])

@section('title', 'Akun Kredit')

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @include('elements.greeting', ['greeting' => 'Apa yang bisa kami bantu hari ini?'])

            @if (count($agreements) > 0)
                @include('elements.card-list-credit')
            @else
                @include('elements.card-empty', ['icon' => 'credit_card', 'message' => 'Anda dapat melihat Histori Transaksi, Histori Aktifitas, dan Status Pengajuan Baru di halaman ini setelah kredit Anda disetujui.'])
            @endif
        </div>
    </div>
@endsection
