@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'credit',
    ],
    'title' => 'Kredit Berjalan',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Akun Kredit',
            'href' => route('customer.credit'),
        ],
        [
            'title' => 'Kredit Berjalan',
        ]
    ],
])

@section('title', 'Kredit Berjalan')

@section('content')
    <div class="container-fluid ___credit-all-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Daftar Kredit Berjalan',
                'addons' => null
            ])
                @slot('header')
                    <div class="row mr-b-40">
                        <div class="col-12 col-md-6">
                            <div>Jumlah Kredit Berjalan</div>
                            <div class="___home-page__credit__deadline text-black fw-500 mr-b-10">{{ $aggregate['count'] }} kredit</div>
                        </div>
                        <div class="col col-md-6">
                            <div class="fw-700">Total Sisa Hutang</div>
                            <div class="d-flex ___home-page__credit__price">
                                <span class="___sup">Rp</span>
                                <span class="text-primary">{{ str_currency($aggregate['total'], false) }}</span>
                            </div>
                        </div>
                    </div>
                @endslot

                <div class="___credit-all-page__list">
                    @foreach ($summaries as $summary)
                        @include('elements.card-credit', ['data' => $summary])
                    @endforeach
                </div>
            @endcomponent
        </div>
    </div>
@endsection
