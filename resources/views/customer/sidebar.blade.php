@extends('elements.sidebar', [
    'sidebar' => [
        'menu' => [
            [
                'icon' => 'home',
                'name' => 'Beranda',
                'href' => route('customer.home'),
                'active' => $sidebar['active'] == 'home',
            ],
            [
                'icon' => 'credit_card',
                'name' => 'Akun Kredit',
                'href' => route('customer.credit'),
                'active' => $sidebar['active'] == 'credit',
            ],
            [
                'icon' => 'assignment',
                'name' => 'Layanan',
                'href' => route('customer.service'),
                'active' => $sidebar['active'] == 'service',
            ],
            [
                'icon' => 'account_box',
                'name' => 'Pengaturan Akun',
                'href' => route('customer.account'),
                'active' => $sidebar['active'] == 'account',
            ],
            [
                'icon' => 'info',
                'name' => 'Informasi',
                'href' => route('customer.info'),
                'active' => $sidebar['active'] == 'information',
            ],
        ],
    ],
])
