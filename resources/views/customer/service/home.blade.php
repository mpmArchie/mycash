@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'service',
    ],
    'title' => 'Layanan',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Layanan',
        ]
    ],
])

@section('title', 'Layanan')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @include('elements.greeting', ['greeting' => 'Apa yang bisa kami bantu hari ini?'])
            @if (count($agreements) > 0)
                @include('elements.card-list-service')
            @else
                @include('elements.card-empty', ['icon' => 'account_box', 'message' => 'Anda dapat melakukan Top Up, Pengajuan Baru, Klaim Asuransi, dan Pengambilan BPKB di halaman ini setelah kredit Anda disetujui.'])
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
@endpush

@push('scripts-app')
    <script>
        @if(session('status'))
            $.toast({
                heading: 'Permintaan Berhasil',
                text: "{{ session('status') }}",
                hideAfter : 7000,
                position: 'top-right',
                icon: 'success',
                stack: false
            });
        @endif
    </script>
@endpush
