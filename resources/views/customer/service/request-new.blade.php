@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'service',
    ],
    'title' => 'Top Up atau Pengajuan Baru',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Layanan',
            'href' => route('customer.service'),
        ],
        [
            'title' => 'Top Up atau Pengajuan Baru',
        ]
    ],
])

@section('title', 'Top Up atau Pengajuan Baru')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___service-request-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Formulir Top Up atau Pengajuan Baru',
                'addons' => null
            ])
                <form id="form-request-new" class="row" method="POST" action="{{ route('customer.service.request.new.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'request-type',
                            'name' => 'request_type',
                            'horizontal' => true,
                            'label' => 'JENIS PENGAJUAN',
                            'placeholder' => 'Pilih jenis pengajuan',
                            'value' => 'new',
                            'options' => [
                                [
                                    'value' => 'new',
                                    'label' => 'Pengajuan Baru'
                                ],
                                [
                                    'value' => 'topup',
                                    'label' => 'Top Up'
                                ]
                            ]
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'value' => $userData['name'],
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'type',
                            'name' => 'type',
                            'horizontal' => true,
                            'label' => 'JENIS KENDARAAN',
                            'placeholder' => 'Pilih jenis kendaraan',
                            'options' => [
                                [
                                    'value' => '8',
                                    'label' => 'Mobil'
                                ],
                                [
                                    'value' => '7',
                                    'label' => 'Motor'
                                ]
                            ],
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'brand',
                            'name' => 'brand',
                            'horizontal' => true,
                            'label' => 'UNIT / BRAND',
                            'placeholder' => 'Nama unit atau merk',
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'year',
                            'name' => 'year',
                            'horizontal' => true,
                            'label' => 'TAHUN KENDARAAN',
                            'placeholder' => 'Tahun kendaraan (YYYY)',
                            'type' => 'number',
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'license-plate',
                            'name' => 'license_plate',
                            'horizontal' => true,
                            'label' => 'NOMOR PLAT',
                            'placeholder' => 'Nomor plat kendaraan',
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'total',
                            'name' => 'total',
                            'horizontal' => true,
                            'label' => 'NILAI PEMBIAYAAN',
                            'placeholder' => 'Jumlah nilai pembiayaan',
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'installment',
                            'name' => 'installment',
                            'horizontal' => true,
                            'label' => 'JANGKA WAKTU',
                            'placeholder' => 'Pilih jangka waktu',
                            'options' => [],
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'area',
                            'name' => 'area',
                            'horizontal' => true,
                            'label' => 'AREA PENGAJUAN',
                            'placeholder' => 'Pilih area pengajuan',
                            'options' => [],
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'installment-amount',
                            'horizontal' => true,
                            'label' => 'ANGSURAN',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Ajukan Pengajuan Baru</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.id"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script type="text/javascript">
        $(document).ready(() => {
            var simulationData = JSON.parse('{!! json_encode($simulations) !!}');

            var totalField = new AutoNumeric('#form-request-new #total', {
                currencySymbol: "Rp ",
                decimalCharacter: ",",
                decimalPlaces: 0,
                digitGroupSeparator: ".",
                unformatOnSubmit: true,
            });

            $('#request-type').on('change', function () {
                var request = this.value;
                window.location.href = `{{ route('customer.service.request') }}/${request}`;
            });

            $('#type').on('change', function () {
                var type = this.value;
                var data = simulationData.find(function (item) {
                    return item.code == type;
                });

                $('#installment').empty();
                data.installments.forEach(installmet => {
                    var newOption = new Option(installmet.label_duration, installmet.id, false, false);
                    $('#installment').append(newOption);
                });
                $('#installment').val(null).trigger('change');

                var url = `/api/branches/${type}`;
                $.get(url, function (result) {
                    var data = result.data.map((item) => ({
                        value: item.code,
                        text: item.name,
                    }));

                    $('#area').empty();
                    data.forEach(branch => {
                        var newOption = new Option(branch.text, branch.value, false, false);
                        $('#area').append(newOption);
                    });
                    $('#area').val(null).trigger('change');
                });
            });

            $('#installment').on('change', function () {
                var installment = this.value;
                if (installment) {
                    var total = totalField.getNumericString() || 0;
                    var url = `/api/calculate/${installment}/${total}`;
                    $('#installment-amount').text('Menghitung....');
                    $.get(url, function (result) {
                        var value = result.data;
                        var formatter = new Intl.NumberFormat('id-ID', {
                            style: 'currency',
                            currency: 'IDR',
                            maximumSignificantDigits: 3,
                        });
                        $('#installment-amount').text(`${formatter.format(value)} / bulan`);
                    });
                }
            });
        });
    </script>
@endpush
