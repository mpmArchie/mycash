@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'service',
    ],
    'title' => 'Top Up atau Pengajuan Baru',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Layanan',
            'href' => route('customer.service'),
        ],
        [
            'title' => 'Top Up atau Pengajuan Baru',
        ]
    ],
])

@section('title', 'Top Up atau Pengajuan Baru')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___service-request-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Formulir Top Up atau Pengajuan Baru',
                'addons' => null
            ])
                <form id="form-request-topup" class="row" method="POST" action="{{ route('customer.service.request.topup.store') }}">
                    @csrf
                    <input id="email" type="hidden" name="email" value="" />
                    <input id="plafon-number" type="hidden" value="" />
                    <input id="total-outstanding-number" type="hidden" value="" />
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'request-type',
                            'name' => 'request_type',
                            'horizontal' => true,
                            'label' => 'JENIS PENGAJUAN',
                            'placeholder' => 'Pilih jenis pengajuan',
                            'value' => 'topup',
                            'options' => [
                                [
                                    'value' => 'new',
                                    'label' => 'Pengajuan Baru'
                                ],
                                [
                                    'value' => 'topup',
                                    'label' => 'Top Up'
                                ]
                            ]
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'agreement-number',
                            'name' => 'agreement_number',
                            'horizontal' => true,
                            'label' => 'NOMOR KONTRAK',
                            'placeholder' => 'Pilih nomor kontrak',
                            'options' => $agreements->map(function ($agreement) {
                                return [
                                    'value' => $agreement['agreement_number'],
                                    'label' => $agreement['agreement_number'],
                                ];
                            }),
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'type',
                            'horizontal' => true,
                            'label' => 'JENIS KENDARAAN',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'brand',
                            'horizontal' => true,
                            'label' => 'UNIT / BRAND',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'year',
                            'horizontal' => true,
                            'label' => 'TAHUN KENDARAAN',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'license-plate',
                            'horizontal' => true,
                            'label' => 'NOMOR PLAT',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'total',
                            'name' => 'total',
                            'horizontal' => true,
                            'label' => 'NILAI PEMBIAYAAN',
                            'placeholder' => 'Jumlah nilai pembiayaan',
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'total-outstanding',
                            'horizontal' => true,
                            'label' => 'SISA HUTANG',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'received',
                            'horizontal' => true,
                            'label' => 'JUMLAH DITERIMA',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <div id="form-error" class="row">
                            <div class="col-md-9 offset-md-3">
                                <span class="text-danger">Tidak dapat melakukan Top Up</span>
                            </div>
                        </div>
                        <button id="form-button" class="btn btn-rounded btn-primary ripple align-self-end" type="button" onclick="submitForm();">Ajukan Top Up</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script type="text/javascript">
        $('#form-error').hide();

        var totalField = new AutoNumeric('#form-request-topup #total', {
            currencySymbol: "Rp ",
            decimalCharacter: ",",
            decimalPlaces: 0,
            digitGroupSeparator: ".",
            unformatOnSubmit: true,
        });

        function updateForm(agreementNumber) {
            var url = `/api/topup/${agreementNumber}`;

            $('#name').text('Memuat....');
            $('#type').text('Memuat....');
            $('#brand').text('Memuat....');
            $('#year').text('Memuat....');
            $('#license-plate').text('Memuat....');
            $('#total-outstanding').text('Memuat....');
            $('#received').text('Memuat....');

            $.get(url, function (result) {
                var data = result.data;

                $('#email').val(data.email);
                $('#name').text(data.name);
                $('#type').text(data.type);
                $('#brand').text(data.brand);
                $('#year').text(data.year);
                $('#license-plate').text(data.license_plate);

                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0,
                });
                $('#total-outstanding').text(formatter.format(data.total_outstanding));
                $('#total-outstanding-number').val(data.total_outstanding);
                $('#plafon-number').val(data.total);

                var total = totalField.getNumericString();
                if (total) {
                    $('#received').text(formatter.format(parseInt(total) - data.total_outstanding));
                } else {
                    $('#received').text('-');
                }

                if (data.topup_enable === 'no') {
                    $('#form-error').show();
                    $('#form-button').hide();
                }
            });
        }

        function submitForm() {
            var total = totalField.getNumericString();
            var plafon = $('#plafon-number').val();

            if (parseInt(total) > parseInt(plafon)) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0,
                });
                swal({
                    title: 'Top Up Gagal!',
                    text: `Nilai pembiayaan melebihi plafon (${formatter.format(parseInt(plafon))})`,
                    type: 'error',
                    confirmButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Tutup',
                });
            } else {
                totalField.formSubmitNumericString();
            }
        }

        $(document).ready(() => {
            $('#request-type').on('change', function () {
                var request = this.value;
                window.location.href = `{{ route('customer.service.request') }}/${request}`;
            });

            $('#agreement-number').on('change', function () {
                var agreementNumber = this.value;
                updateForm(agreementNumber);
            });

            $('#total').keyup(function () {
                var total = totalField.getNumericString();
                var totalOutstanding = $('#total-outstanding-number').val();

                if (total && totalOutstanding) {
                    var formatter = new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR',
                        minimumFractionDigits: 0,
                    });
                    $('#received').text(formatter.format(parseInt(total) - parseInt(totalOutstanding)));
                } else {
                    $('#received').text('-');
                }
            });
        });
    </script>
@endpush
