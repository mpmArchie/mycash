@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'service',
    ],
    'title' => 'Klaim Asuransi',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Layanan',
            'href' => route('customer.service'),
        ],
        [
            'title' => 'Klaim Asuransi',
        ]
    ],
])

@section('title', 'Klaim Asuransi')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Formulir Klaim Asuransi',
                'addons' => null
            ])
                <form class="row" method="POST" action="{{ route('customer.service.claim.store') }}">
                    @csrf
                    <input id="email" type="hidden" name="email" value="" />
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'claim-type',
                            'name' => 'claim_type',
                            'horizontal' => true,
                            'label' => 'JENIS KLAIM',
                            'placeholder' => 'Pilih jenis Klaim',
                            'options' => [
                                [
                                    'value' => 'Unit',
                                    'label' => 'Unit'
                                ],
                                [
                                    'value' => 'Life',
                                    'label' => 'Life'
                                ]
                            ],
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'agreement-number',
                            'name' => 'agreement_number',
                            'horizontal' => true,
                            'label' => 'NOMOR KONTRAK',
                            'placeholder' => 'Pilih nomor kontrak',
                            'options' => $agreements->map(function ($agreement) {
                                return [
                                    'value' => $agreement['agreement_number'],
                                    'label' => $agreement['agreement_number'],
                                ];
                            }),
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'brand',
                            'horizontal' => true,
                            'label' => 'UNIT / BRAND',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'license-plate',
                            'horizontal' => true,
                            'label' => 'NOMOR PLAT',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'insco',
                            'horizontal' => true,
                            'label' => 'INSCO',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'insco-phone',
                            'horizontal' => true,
                            'label' => 'NO. TELP. INSCO',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.datepicker', [
                            'id' => 'date',
                            'name' => 'date',
                            'horizontal' => true,
                            'label' => 'TANGGAL KEJADIAN',
                            'value' => old('date', \Carbon\Carbon::parse(now())->isoFormat('DD-MM-YYYY')),
                            'required' => true,
                            'all' => false,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.text', [
                            'id' => 'location',
                            'name' => 'location',
                            'horizontal' => true,
                            'label' => 'LOKASI KEJADIAN',
                            'placeholder' => 'Nama lokasi kejadian',
                            'value' => old('location'),
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => 'detail',
                            'name' => 'detail',
                            'horizontal' => true,
                            'label' => 'DETAIL KEJADIAN',
                            'placeholder' => 'Informasi detail kejadian',
                            'value' => old('detail'),
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Kirim Klaim Asuransi</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        function updateForm(claimType, agreementNumber) {
            var url = `/api/claim/${claimType}/${agreementNumber}`;

            $('#name').text('Memuat....');
            $('#brand').text('Memuat....');
            $('#license-plate').text('Memuat....');
            $('#insco').text('Memuat....');
            $('#insco-phone').text('Memuat....');

            $.get(url, function (result) {
                var data = result.data;

                $('#email').val(data.email);
                $('#name').text(data.name);
                $('#brand').text(data.brand);
                $('#license-plate').text(data.license_plate);
                $('#insco').text(data.insco);
                $('#insco-phone').text(data.insco_phone);
            });
        }

        $(document).ready(() => {
            $('#claim-type').on('change', function () {
                var claimType = this.value;
                var agreementNumber = $('#agreement-number').val();
                if (claimType && agreementNumber) {
                    updateForm(claimType, agreementNumber);
                }
            });

            $('#agreement-number').on('change', function () {
                var claimType = $('#claim-type').val();
                var agreementNumber = this.value;
                if (claimType && agreementNumber) {
                    updateForm(claimType, agreementNumber);
                }
            });
        });
    </script>
@endpush
