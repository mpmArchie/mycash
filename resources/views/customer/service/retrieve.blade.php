@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'service',
    ],
    'title' => 'Pengambilan BPKB',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Layanan',
            'href' => route('customer.service'),
        ],
        [
            'title' => 'Pengambilan BPKB',
        ]
    ],
])

@section('title', 'Pengambilan BPKB')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Formulir Pengambilan BPKB',
                'addons' => null
            ])
                <form class="row" method="POST" action="{{ route('customer.service.retrieve.store') }}">
                    @csrf
                    <input id="email" type="hidden" name="email" value="" />
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'agreement-number',
                            'name' => 'agreement_number',
                            'horizontal' => true,
                            'label' => 'NOMOR KONTRAK',
                            'placeholder' => 'Pilih nomor kontrak',
                            'options' => $agreements->map(function ($agreement) {
                                return [
                                    'value' => $agreement['agreement_number'],
                                    'label' => $agreement['agreement_number'],
                                ];
                            }),
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'name',
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'brand',
                            'horizontal' => true,
                            'label' => 'UNIT / BRAND',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'license-plate',
                            'horizontal' => true,
                            'label' => 'NOMOR PLAT',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'id' => 'branch',
                            'horizontal' => true,
                            'label' => 'CABANG',
                            'value' => '-',
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.datepicker', [
                            'id' => 'date',
                            'name' => 'date',
                            'horizontal' => true,
                            'label' => 'TANGGAL PENGAMBILAN',
                            'value' => \Carbon\Carbon::parse(now())->addDays(7)->isoFormat('DD-MM-YYYY'),
                            'required' => true,
                            'all' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.timepicker', [
                            'id' => 'time',
                            'name' => 'time',
                            'horizontal' => true,
                            'label' => 'WAKTU PENGAMBILAN',
                            'value' => '10:00',
                            'required' => true,
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <div id="form-error" class="row">
                            <div class="col-md-9 offset-md-3">
                                <span class="text-danger">Tidak dapat melakukan pengambilan BPKB</span>
                            </div>
                        </div>
                        <button id="form-button" class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Ambil BPKB</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        $('#form-error').hide();

        function updateForm(agreementNumber) {
            var url = `/api/retrieve/${agreementNumber}`;

            $('#name').text('Memuat....');
            $('#brand').text('Memuat....');
            $('#license-plate').text('Memuat....');
            $('#branch').text('Memuat....');

            $.get(url, function (result) {
                var data = result.data;

                $('#email').val(data.email);
                $('#name').text(data.name);
                $('#brand').text(data.brand);
                $('#license-plate').text(data.license_plate);
                $('#branch').text(data.branch);

                if (data.release_enable === 'NO') {
                    $('#form-error').show();
                    $('#form-button').hide();
                }
            });
        }

        $(document).ready(() => {
            $('#agreement-number').on('change', function () {
                var agreementNumber = this.value;
                if (agreementNumber) {
                    updateForm(agreementNumber);
                }
            });
        });
    </script>
@endpush
