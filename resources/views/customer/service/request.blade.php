@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'service',
    ],
    'title' => 'Top Up atau Pengajuan Baru',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Layanan',
            'href' => route('customer.service'),
        ],
        [
            'title' => 'Top Up atau Pengajuan Baru',
        ]
    ],
])

@section('title', 'Top Up atau Pengajuan Baru')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="container-fluid ___service-request-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Formulir Top Up atau Pengajuan Baru',
                'addons' => null
            ])
                <form class="row">
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.select', [
                            'id' => 'request-type',
                            'name' => 'request_type',
                            'horizontal' => true,
                            'label' => 'JENIS PENGAJUAN',
                            'placeholder' => 'Pilih jenis pengajuan',
                            'options' => [
                                [
                                    'value' => 'new',
                                    'label' => 'Pengajuan Baru'
                                ],
                                [
                                    'value' => 'topup',
                                    'label' => 'Top Up'
                                ]
                            ]
                        ])
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(() => {
            $('#request-type').on('change', function () {
                var request = this.value;
                window.location.href = `{{ route('customer.service.request') }}/${request}`;
            });
        });
    </script>
@endpush
