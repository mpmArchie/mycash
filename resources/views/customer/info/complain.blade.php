@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'information',
    ],
    'title' => 'Pengaduan',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Informasi',
            'href' => route('customer.info'),
        ],
        [
            'title' => 'Pengaduan',
        ]
    ],
])

@section('title', 'Pengaduan')

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @component('elements.container', [
                'title' => 'Formulir Pengaduan',
                'addons' => null
            ])
                <form class="row" method="POST" action="{{ route('customer.info.complain.store') }}">
                    @csrf
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.static', [
                            'horizontal' => true,
                            'label' => 'NAMA',
                            'value' => $userData['name'],
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        @include('elements.forms.textarea', [
                            'id' => '4',
                            'name' => 'detail',
                            'horizontal' => true,
                            'label' => 'DETAIL PENGADUAN',
                            'placeholder' => 'Rincian pengaduan Anda (min. 200 kata)',
                            'value' => old('detail'),
                        ])
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-8 d-flex flex-column">
                        <button class="btn btn-rounded btn-primary ripple align-self-end" type="submit">Kirim Pengaduan</button>
                    </div>
                </form>
            @endcomponent
        </div>
    </div>
@endsection
