@extends('layouts.main', [
    'sidebar' => [
        'name' => 'customer.sidebar',
        'active' => 'home',
    ],
    'title' => 'Beranda',
    'breadcrumbs' => [
        [
            'title' => 'Dashboard',
            'href' => route('customer.home'),
        ],
        [
            'title' => 'Beranda',
        ]
    ],
])

@section('title', 'Beranda')

@push('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
@endpush

@section('content')
    <div class="container-fluid ___home-page">
        <div class="widget-list row">
            @include('elements.greeting', ['greeting' => 'Lihat ringkasan dari aktivitas kredit kamu disini'])

            @if (count($requests) > 0)
                @foreach ($requests as $request)
                    @component('elements.container', [
                        'title' => 'Status Pengajuan Baru Anda',
                        'addons' => null
                    ])
                        @include('elements.card-request', ['agreement' => $request])
                    @endcomponent
                @endforeach
            @endif

            @if (count($summaries) > 0)
                @component('elements.container', [
                    'title' => 'Kredit Berjalan'
                ])
                    @slot('addons')
                        @if (count($summaries) > 1)
                            <a class="btn btn-rounded btn-success ripple mt-2 mt-sm-0" href="{{ route('customer.credit.all') }}">Lihat Semua</a>
                        @endif
                    @endslot

                    <div class="___home-page__credit__carousel">
                        <a class="___home-page__credit__carousel--prev page-link">
                            <span aria-hidden="true"><i class="feather feather-chevron-left"></i></span>
                        </a>
                        <div class="___home-page__credit__carousel--container {{ count($summaries) == 1 ? 'single' : '' }}">
                            <div>
                                @foreach ($summaries as $summary)
                                    @include('elements.card-credit', ['data' => $summary])
                                @endforeach
                            </div>
                        </div>
                        <a class="___home-page__credit__carousel--next page-link">
                            <span aria-hidden="true"><i class="feather feather-chevron-right"></i></span>
                        </a>
                    </div>
                @endcomponent

                @include('elements.card-list-credit', ['classname' => 'mr-b-0'])
                @include('elements.card-list-service')
            @endif

        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
@endpush

@push('scripts-app')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.___home-page__credit__carousel--container > div').slick({
                infinite: false,
                adaptiveHeight: true,
                dots: false,
                arrows: true,
                draggable: false,
                autoplay: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: $('.___home-page__credit__carousel--prev'),
                nextArrow: $('.___home-page__credit__carousel--next'),
                responsive: [
                    {
                        breakpoint: 750,
                        settings: {
                            dots: true,
                            arrows: false,
                        }
                    },
                ]
            });
        });
    </script>
@endpush
