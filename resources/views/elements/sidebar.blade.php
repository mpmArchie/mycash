<aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
    <div class="side-user">
        <figure class="side-user-bg" style="background-image: url({{ asset('templates/backend/demo/user-image-cropped.jpg') }}">
            <img src="{{ asset('templates/backend/demo/user-image-cropped.jpg') }}" alt="" class="d-none">
        </figure>
        <div class="col-sm-12 text-center p-0 clearfix">
            <div class="d-inline-block pos-relative mr-b-10">
                <span class="avatar-text">{{ substr($userData['name'], 0, 1) }}</span>
                <figure class="avatar-img thumb-sm mr-b-0 d-none">
                    <img src="{{ $userData['photo']['uri'] }}" class="rounded-circle" alt="">
                </figure>
            </div>
            <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                <h6 class="hide-menu side-user-heading">{{ $userData['name'] }}</h6>
                <small class="hide-menu">{{ $userData['email'] }}</small>
            </div>
        </div>
    </div>
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
            @foreach ($sidebar['menu'] as $item)
                <li class="menu-item{{ isset($item['children']) ? '-has-children' : '' }}  {{ $item['active'] ? 'active' : '' }}">
                    <a href="{{ $item['href'] }}"><i class="list-icon material-icons">{{ $item['icon'] }}</i> <span class="hide-menu">{{ $item['name'] }}</span></a>
                    @isset($item['children'])
                        <ul class="list-unstyled sub-menu">
                            @foreach ($item['children'] as $item)
                                <li><a href="{{ $item['href'] }}">{{ $item['name'] }}</a></li>
                            @endforeach
                        </ul>
                    @endisset
                </li>
            @endforeach
        </ul>
    </nav>
</aside>
