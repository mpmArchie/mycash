<nav class="navbar">
    <div class="container-fluid px-0 align-items-stretch">
        <div class="navbar-header">
            <a href="{{ route('home') }}" class="navbar-brand">
                <img class="logo-expand" alt="" src="{{ asset('images/logo-dark.png') }}">
                <img class="logo-collapse" alt="" src="{{ asset('images/logo-collapse.png') }}">
            </a>
        </div>
        <ul class="nav navbar-nav">
            <li class="sidebar-toggle dropdown">
                <a href="javascript:void(0)" class="ripple"><i class="material-icons list-icon md-24">menu</i></a>
            </li>
        </ul>
        <div class="spacer"></div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple" data-toggle="dropdown">
                    <span class="avatar thumb-xs2"><img src="{{ $userData['photo']['uri'] }}" class="rounded-circle" alt=""></span>
                </a>
                <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                    <div class="card">
                        <ul class="list-unstyled card-body">
                            <li><a href="{{ route(auth()->user()->isAdmin() ? 'admin.users.index' : 'customer.account') }}"><span><span class="align-middle">Pengaturan Akun</span></span></a></li>
                            <li>
                                <form action="{{ route('logout') }}">
                                    <button type="submit"><span><span class="align-middle">Keluar</span></span></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
