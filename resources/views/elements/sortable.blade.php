<div class="___sortable" id="{{ $id }}" {{ isset($url) ? 'data-url=' . $url : '' }}>
    <div class="___sortable__hint">Drag & drop untuk mengubah urutan</div>
    <ol class="___sortable__container list-group sortable ui-sortable">
        @foreach ($items as $item)
            <li class="___sortable__item list-group-item d-flex ui-sortable-handle" id="{{ $item['id'] }}">
                <div class="mr-auto mr-0-rtl ml-auto-rtl">
                    <i class="material-icons list-icon">swap_vert</i>
                    @isset($item['icon'])
                        <img class="___sortable__item--icon" src="{{ $item['icon'] }}" alt="icon" />
                    @endisset
                    {{ $item['label'] }}
                </div>
                <div>
                    <a href="{{ $item['edit'] }}"><i class="material-icons list-icon text-primary">edit</i></a>
                    <a onclick="{{ $item['delete'] }}"><i class="material-icons list-icon">delete</i></a>
                </div>
            </li>
        @endforeach
    </ol>
</div>
