<div class="widget-holder col-12">
    <div class="widget-bg">
        <div class="widget-body d-flex flex-column align-items-center">
            <i class="list-icon material-icons mr-b-10 fs-40">{{ $icon }}</i>
            <div class="">{{ $message }}</div>
        </div>
    </div>
</div>
