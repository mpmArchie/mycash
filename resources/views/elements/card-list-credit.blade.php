<div class="widget-holder col-12 {{ isset($classname) ? $classname : '' }}">
    <div class="widget-list row">
        @include('elements.card-icon', [
            'icon' => 'cached',
            'iconColor' => '#aaa',
            'title' => 'Histori Transaksi',
            'button' => [
                'class' => 'btn-success',
                'href' => route('customer.credit.transaction'),
                'content' => 'Lihat Transaksi',
            ],
        ])
        @include('elements.card-icon', [
            'icon' => 'history',
            'iconColor' => '#aaa',
            'title' => 'Histori Aktivitas',
            'button' => [
                'class' => 'btn-primary',
                'href' => route('customer.credit.activity'),
                'content' => 'Lihat Aktivitas',
            ],
        ])
        @include('elements.card-icon', [
            'icon' => 'note_add',
            'iconColor' => '#aaa',
            'title' => 'Status Pengajuan Baru',
            'button' => [
                'class' => 'btn-outline-default',
                'href' => route('customer.credit.status'),
                'content' => 'Lihat Status',
            ],
        ])
    </div>
</div>
