<div class="widget-holder col-12">
    <div class="widget-list row">
        @include('elements.card', [
            'title' => 'Tanya Jawab',
            'subtitle' => 'Lihat kumpulan pertanyaan disini',
            'button' => [
                'class' => 'btn-primary',
                'href' => route('info.faq'),
                'target' => '_blank',
                'content' => 'Lihat Tanya Jawab',
            ],
        ])
        @include('elements.card', [
            'title' => 'Pengaduan',
            'subtitle' => 'Hubungi CS MyCash melalui menu ini',
            'button' => [
                'class' => 'btn-primary',
                'href' => route('customer.info.complain'),
                'content' => 'Pengaduan',
            ],
        ])
    </div>
</div>
