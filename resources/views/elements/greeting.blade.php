<div class="widget-holder widget-full-height col-12">
    <div class="widget-bg zi-1" style="background-color: #eff2f7">
        <div class="widget-body py-2">
            <div class="pos-0 zi-n-1 d-none d-lg-block" style="background-repeat: no-repeat;
                    background-size: auto 100%;
                    background-position: 90% center;
                    background-image: url('{{ asset('templates/backend/demo/horizontal-demo-bg.png') }}')"></div>
            <div class="media">
                <div class="media-body">
                    <h4 class="fw-300">Hi, {{ $userData['name'] }}!</h4>
                    <p class="text-muted">{{ $greeting }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
