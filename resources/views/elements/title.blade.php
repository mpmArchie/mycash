<div class="container-fluid">
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h6 class="page-title-heading mr-0 mr-r-5">{{ $title }}</h6>
            @isset($subtitle)
                <p class="page-title-description mr-0 d-none d-md-inline-block">{{ $subtitle }}</p>
            @endisset
        </div>
        <div class="page-title-right d-none d-sm-inline-flex">
            <ol class="breadcrumb">
                @foreach ($breadcrumbs as $item)
                    @if (isset($item['href']))
                        <li class="breadcrumb-item"><a href="{{ $item['href'] }}">{{ $item['title'] }}</a></li>
                    @else
                        <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                    @endif
                @endforeach
            </ol>
        </div>
    </div>
</div>
