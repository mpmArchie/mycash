<div class="widget-holder col-12">
    <div class="widget-bg">
        <div class="widget-body">
            @isset($header)
                {{ $header }}
            @endisset
            <div class="d-flex flex-wrap flex-column flex-sm-row box-title">
                <h5 class="box-title mb-0">{{ $title }}</h5>
                {{ $addons }}
            </div>
            {{ $slot }}
        </div>
    </div>
</div>
