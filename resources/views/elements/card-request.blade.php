<div class="form-group mr-b-20">
    <label>PENGAJUAN ANDA</label>
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="d-flex flex-column flex-sm-row mr-b-5">
                <div style="max-width: 180px; min-width: 180px">Nomer Pengajuan</div>
                <div class="text-black fw-700">{{ $agreement['request_number'] }}</div>
            </div>
            <div class="d-flex flex-column flex-sm-row mr-b-5">
                <div style="max-width: 180px; min-width: 180px">Tanggal Pengajuan</div>
                <div class="text-black fw-700">{{ \Carbon\Carbon::parse($agreement['created_at'])->locale('id')->isoFormat('D MMMM YYYY, HH:mm') }}</div>
            </div>
            <div class="d-flex flex-column flex-sm-row mr-b-5">
                <div style="max-width: 180px; min-width: 180px">Status Pengajuan</div>
                <div class="text-black fw-700">{{ $agreement['request_status_name'] }}</div>
            </div>
        </div>
    </div>
</div>

@include('elements.steps', ['active' => $agreement['request_status_id']])
