<div class="widget-holder col-12">
    <div class="widget-list row">
        @include('elements.card-icon', [
            'icon' => 'add_circle_outline',
            'iconColor' => '#fdb44d',
            'title' => 'Top Up / Pengajuan Baru',
            'button' => [
                'class' => 'btn-success',
                'href' => route('customer.service.request'),
                'content' => 'Ajukan Kredit',
            ],
        ])
        @include('elements.card-icon', [
            'icon' => 'security',
            'iconColor' => '#fdb44d',
            'title' => 'Klaim Asuransi',
            'button' => [
                'class' => 'btn-primary',
                'href' => route('customer.service.claim'),
                'content' => 'Klaim Asuransi',
            ],
        ])
        @include('elements.card-icon', [
            'icon' => 'directions_car',
            'iconColor' => '#fdb44d',
            'title' => 'Pengambilan BPKB',
            'button' => [
                'class' => 'btn-outline-default',
                'href' => route('customer.service.retrieve'),
                'content' => 'Ambil BPKB',
            ],
        ])
    </div>
</div>
