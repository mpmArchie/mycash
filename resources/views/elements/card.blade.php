<div class="widget-holder col ___card">
    <div class="widget-bg">
        <div class="widget-body d-flex flex-column justify-content-between">
            <h6 class="m-0">{{ $title }}</h6>
            <p class="mr-tb-20">{{ $subtitle }}</p>
            <a class="btn btn-rounded {{ $button['class'] }} ripple align-self-end" href="{{ $button['href'] }}"{{ isset($button['target']) ? ' target=' . $button['target'] : '' }}>{{ $button['content'] }}</a>
        </div>
    </div>
</div>
