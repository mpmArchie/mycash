<div class="col-12 widget-holder widget-full-content ___card ___card-account">
    <div class="widget-bg">
        <div class="widget-body clearfix">
            <div class="widget-user-profile">
                <figure class="profile-wall-img">
                    <img src="{{ asset('templates/backend/demo/user-widget-bg.jpeg') }}" alt="User Wall">
                </figure>
                <div class="profile-body">
                    <figure class="profile-user-avatar thumb-md">
                        <img src="{{ $userData['photo']['uri'] }}" alt="User Wall">
                    </figure>
                    <h6 class="h3 profile-user-name">{{ $userData['name'] }}</h6>
                    <div class="mb-5 mt-3">
                        <a href="{{ route('customer.account.profile') }}" class="btn btn-primary btn-rounded ripple mr-sm-2 mb-2 mb-sm-0">Ubah Profil</a>
                        <a href="{{ route('customer.account.password') }}" class="btn btn-outline-primary btn-rounded ripple">Ubah Kata Sandi</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
