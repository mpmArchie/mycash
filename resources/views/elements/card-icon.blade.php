<div class="widget-holder col ___card-icon">
    <div class="widget-bg">
        <div class="widget-body text-center d-flex flex-column justify-content-between">
            <i class="list-icon material-icons" style="color: {{ $iconColor }};">{{ $icon }}</i>
            <h6 class="mr-tb-20">{{ $title }}</h6>
            <a class="btn btn-block btn-rounded {{ $button['class'] }} ripple" href="{{ $button['href'] }}">{{ $button['content'] }}</a>
        </div>
    </div>
</div>
