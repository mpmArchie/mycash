<div id="{{ $id }}" class="col col-md-6 col-lg-4 col-xl-3 mr-b-20">
    <div class="___card-image">
        <img src="{{ $image }}" alt="{{ $title }}" />
        <div class="___card-image__content">
            <h5 class="___card-image__content--title">{!! $title !!}</h5>
            <div class="___card-image__content--subtitle">{{ $subtitle }}</div>
            <div class="___card-image__content--buttons">
                <a class="text-primary" href="{{ $edit }}"><i class="material-icons">edit</i></a>
                <a class="text-muted" onclick="{{ $delete }}"><i class="material-icons">delete</i></a>
            </div>
        </div>
    </div>
</div>
