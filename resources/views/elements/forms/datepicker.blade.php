@if (isset($horizontal) && $horizontal)
    <div class="form-group row input-has-value {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9 input-group input-has-value">
            <input type="text" class="form-control datepicker" id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" data-masked-input="99-99-9999" data-plugin-options='{"autoclose": true, "format": "dd-mm-yyyy"}' {{ isset($all) && $all ? '' : 'data-date-end-date="0d"' }}>
            <div class="input-group-append">
                <div class="input-group-text"><i class="list-icon material-icons">date_range</i></div>
            </div>
        </div>
    </div>
@else
    <div class="form-group input-has-value {{ isset($class) ? $class : '' }}">
        <label class="form-control-label" for="{{ $id }}">{{ $label }}</label>
        <div class="input-group input-has-value">
            <input type="text" class="form-control datepicker" id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" data-masked-input="99-99-9999" data-plugin-options='{"autoclose": true, "format": "dd-mm-yyyy"}' {{ isset($all) && $all ? '' : 'data-date-end-date="0d"' }}>
            <div class="input-group-append">
                <div class="input-group-text"><i class="list-icon material-icons">date_range</i></div>
            </div>
        </div>
    </div>
@endif
