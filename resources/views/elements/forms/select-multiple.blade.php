@if (isset($horizontal) && $horizontal)
    <div class="form-group row {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9">
            <select class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}[]" multiple="multiple" data-placeholder="{{ $placeholder }}" data-toggle="select2">
                @unless (isset($value))
                    <option></option>
                @endunless
                @foreach ($options as $option)
                    <option value="{{ $option['value'] }}"{{ isset($value) && in_array($option['value'], $value) ? ' selected' : ''}}>{{ $option['label'] }}</option>
                @endforeach
            </select>
            @if ($errors->has($name))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif
        </div>
    </div>
@else
    <div class="form-group {{ isset($class) ? $class : '' }}">
        <label class="form-control-label" for="{{ $id }}">{{ $label }}</label>
        <select class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}[]" multiple="multiple" data-placeholder="{{ $placeholder }}" data-toggle="select2">
            @unless (isset($value))
                <option></option>
            @endunless
            @foreach ($options as $option)
                <option value="{{ $option['value'] }}"{{ isset($value) && in_array($option['value'], $value) ? ' selected' : ''}}>{{ $option['label'] }}</option>
            @endforeach
        </select>
        @if ($errors->has($name))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
@endif
