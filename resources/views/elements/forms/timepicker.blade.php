@if (isset($horizontal) && $horizontal)
    <div class="form-group row {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9">
            <div class="input-group clockpicker">
                <input type="text" class="form-control" data-masked-input="99:99" id="{{ $id }}" name="{{ $name }}" value="{{ $value ?? '' }}">
                <div class="input-group-append">
                    <div class="input-group-text"><i class="material-icons">watch_later</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="form-group {{ isset($class) ? $class : '' }}">
        <label class="form-control-label" for="{{ $id }}">{{ $label }}</label>
        <div class="input-group clockpicker">
            <input type="text" class="form-control" data-masked-input="99:99" id="{{ $id }}" name="{{ $name }}" value="{{ $value ?? '' }}">
            <div class="input-group-append">
                <div class="input-group-text"><i class="material-icons">watch_later</i>
                </div>
            </div>
        </div>
    </div>
@endif
