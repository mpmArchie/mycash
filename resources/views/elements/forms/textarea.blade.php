@if (isset($horizontal) && $horizontal)
    <div class="form-group row {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9">
            <textarea class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }} noresize" rows="4" id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder }}" {{ isset($required) ? 'required': '' }}>{{ $value ?? '' }}</textarea>
            @isset($help)
                <small class="text-muted">{{ $help }}</small>
            @endisset
            @if ($errors->has($name))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif
        </div>
    </div>
@else
    <div class="form-group {{ isset($class) ? $class : '' }}">
        <label for="{{ $id }}">{{ $label }}</label>
        <textarea class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }} noresize" rows="4" id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder }}" {{ isset($required) ? 'required': '' }}>{{ $value ?? '' }}</textarea>
        @isset($help)
            <small class="text-muted">{{ $help }}</small>
        @endisset
        @if ($errors->has($name))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
@endif
