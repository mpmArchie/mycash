@if (isset($horizontal) && $horizontal)
    <div class="form-group row {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label">{{ $label }}</label>
        <div class="col-md-9">
            <p {{ isset($id) ? 'id=' . $id  : '' }} class="form-control-plaintext">{!! $value !!}</p>
        </div>
    </div>
@else
    Not Supported
@endif
