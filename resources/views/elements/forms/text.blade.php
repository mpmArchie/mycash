@if (isset($horizontal) && $horizontal)
    <div class="form-group row {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9">
            <input class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder }}" type="{{ $type ?? 'text' }}" value="{{ $value ?? '' }}" {{ isset($mask) ? 'data-masked-input=' . $mask : '' }} {{ isset($required) ? 'required': '' }} {{ isset($step) ? 'step=' . $step : '' }}>
            @isset($help)
                <small class="text-muted">{{ $help }}</small>
            @endisset
            @if ($errors->has($name))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif
        </div>
    </div>
@else
    <div class="form-group {{ isset($class) ? $class : '' }}">
        <label for="{{ $id }}">{{ $label }}</label>
        <input class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder }}" type="{{ $type ?? 'text' }}" value="{{ $value ?? '' }}" {{ isset($mask) ? 'data-masked-input=' . $mask : '' }} {{ isset($required) ? 'required': '' }} {{ isset($step) ? 'step=' . $step : '' }}>
        @isset($help)
            <small class="text-muted">{{ $help }}</small>
        @endisset
        @if ($errors->has($name))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
@endif
