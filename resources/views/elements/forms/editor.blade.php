@if (isset($horizontal) && $horizontal)
    <div class="form-group row{{ $errors->has($name) ? ' is-invalid' : '' }} {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9">
            <textarea class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}" {{ isset($required) ? 'required': '' }}>{{ $value ?? ''}}</textarea>
            @if ($errors->has($name))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif
        </div>
    </div>
@else
    <div class="form-group{{ $errors->has($name) ? ' is-invalid' : '' }} {{ isset($class) ? $class : '' }}">
        <label for="{{ $id }}">{{ $label }}</label>
        <textarea class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}" {{ isset($required) ? 'required': '' }}>{{ $value ?? ''}}</textarea>
        @if ($errors->has($name))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
@endif
