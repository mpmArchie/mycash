@if (isset($horizontal) && $horizontal)
    <div class="form-group row {{ isset($class) ? $class : '' }}">
        <label class="col-md-3 col-form-label" for="{{ $id }}">{{ $label }}</label>
        <div class="col-md-9">
            @isset($value)
                <a href="{{ $value['uri'] }}" target="_blank" rel="noopener">{{ $value['filename'] }}</a>
            @endisset
            <input class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" id="{{ $id }}" name="{{ $name }}" type="file" {{ isset($multiple) ? 'multiple': '' }} accept="{{ isset($accept) ? $accept : 'image/x-png,image/png,image/jpeg' }}">
            @isset($help)
                <small class="text-muted">{{ $help }}</small>
            @endisset
            @if ($errors->has($name))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif
        </div>
    </div>
@else
    Not Supported
@endif
