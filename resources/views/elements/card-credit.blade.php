<div class="___home-page__credit {{ isset($classname) ? $classname : '' }}">
    <div class="___home-page__credit__number mr-b-10">Aggreement Number: <span class="text-black fw-700">{{ $data['agreement_number'] }}</span></div>
    <div class="row">
        <div class="col-12 col-md-6 mr-b-10">
            <div class="d-flex ___home-page__credit__price">
                <span class="___sup">Rp</span>
                <span class="text-black">{{ str_currency($data['total_outstanding'], false) }}</span>
            </div>
            <div class="text-success">Sisa hutang</div>
        </div>
        <div class="col col-md-6 mr-b-10 ___border">
            <div class="d-flex ___home-page__credit__price">
                <span class="___sup">Rp</span>
                <span>{{ str_currency($data['total'], false) }}</span>
            </div>
            <div>Pokok hutang</div>
        </div>
    </div>
    <div class="progress progress-md my-2">
        <div class="progress-bar bg-success" style="width: {{ ($data['total_outstanding']/$data['total']) * 100 }}%" role="progressbar">
            <span class="sr-only">{{ number_format(100 - (($data['total_outstanding']/$data['total']) * 100), 2, ',', '.') }}% Complete</span>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 mr-t-10">
            <div class="fw-700">Tagihan per Bulan</div>
            <div class="d-flex ___home-page__credit__price">
                <span class="___sup">Rp</span>
                <span class="text-primary">{{ str_currency($data['installment_amount'], false) }}</span>
            </div>
        </div>
        @if ($data['due_date'] != '-')
            <div class="col col-md-6 mr-t-10">
                <div>Tanggal Jatuh Tempo</div>
                <div class="___home-page__credit__deadline text-black fw-500">{{ \Carbon\Carbon::createFromFormat('d/m/Y', $data['due_date'])->locale('id')->isoFormat('D MMMM YYYY') }}</div>
            </div>
        @endif
    </div>
</div>
