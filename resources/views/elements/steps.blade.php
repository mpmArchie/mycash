<div class="row ___steps flex-sm-nowrap">
    <div class="col-12 offset-sm-1 col-sm-2 ___steps__item {{ $active == '1' ? 'active' : '' }} {{ $active > 1 ? 'done' : '' }}">
        <div class="___steps__item--number">1</div>
        <div class="___steps__item--text">Kredit Diajukan</div>
    </div>
    <div class="col-12 col-sm-2 ___steps__item {{ $active == '2' ? 'active' : '' }} {{ $active > 2 ? 'done' : '' }}">
        <div class="___steps__item--number">2</div>
        <div class="___steps__item--text">Pemeriksaan oleh Petugas</div>
    </div>
    <div class="col-12 col-sm-2 ___steps__item {{ $active == '3' ? 'active' : '' }} {{ $active > 3 ? 'done' : '' }}">
        <div class="___steps__item--number"><span>3</span></div>
        <div class="___steps__item--text">Pemeriksaan Selesai</div>
    </div>
    <div class="col-12 col-sm-2 ___steps__item {{ $active == '4' ? 'active' : '' }} {{ $active > 4 ? 'done' : '' }}">
        <div class="___steps__item--number"><span>4</span></div>
        <div class="___steps__item--text">Permohonan Diajukan</div>
    </div>
    <div class="col-12 col-sm-2 ___steps__item {{ $active == '5' ? 'active' : '' }} {{ $active > 5 ? 'done' : '' }}">
        <div class="___steps__item--number"><span>5</span></div>
        <div class="___steps__item--text">Kontrak Aktif</div>
    </div>
</div>
