<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi minimal 6 huruf dan sesuai dengan konfirmasi kata sandi.',
    'reset' => 'Pembaruan kata sandi berhasil',
    'sent' => 'Kami sudah mengirimkan email untuk pembaruan kata sandi',
    'token' => 'Token untuk pembaruan kata sandi sudah tidak dapat digunakan.',
    'user' => 'Kami tidak dapat menemukan pengguna dengan alamat email tersebut.',

];
