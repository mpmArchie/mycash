<?php

return [
    'default' => [
        'photo' => env('SETTING_DEFAULT_PHOTO', 'templates/backend/demo/users/user1.jpg'),
        'placeholder' => env('SETTING_DEFAULT_PLACEHOLDER', 'templates/frontend/images/resource/services-1.png'),
    ],
    'api' => [
        'url' => env('SETTING_API_URL'),
    ],
    'email' => [
        'admin' => env('SETTING_EMAIL_ADMIN'),
        'customer' => env('SETTING_EMAIL_CUSTOMER'),
    ],
    'analytics' => [
        'google' => env('SETTING_ANALYTICS_GOOGLE', 'UA-XXXXXXXX-X'),
    ],
];
