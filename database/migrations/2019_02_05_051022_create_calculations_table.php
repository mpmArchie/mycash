<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculations', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->integer('year');
            $table->bigInteger('total');
            $table->string('area');
            $table->string('name');
            $table->string('ktp');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->string('rt');
            $table->string('rw');
            $table->string('city');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('postcode');
            $table->string('verification_code');
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_requested')->default(false);
            $table->timestamp('verified_at')->nullable();
            $table->timestamp('requested_at')->nullable();
            $table->unsignedInteger('simulation_id')->nullable();
            $table->unsignedInteger('installment_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();

            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('set null');
            $table->foreign('installment_id')->references('id')->on('installments')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculations');
    }
}
