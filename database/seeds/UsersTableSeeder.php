<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name' => 'MyCash Administrator',
            'email' => 'mycash@mpm-finance.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret'),
            'role' => 'admin',
            'ktp' => '0000000000000000',
            'remember_token' => str_random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([
            'name' => 'MyCash Customer',
            'email' => 'pelanggan@mpm-finance.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret'),
            'role' => 'customer',
            'ktp' => '0000000000000000',
            'remember_token' => str_random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
