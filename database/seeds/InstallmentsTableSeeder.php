<?php

use Illuminate\Database\Seeder;

class InstallmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // New 4W
        // 1
        DB::table('installments')->insert([
            'duration' => 6,
            'rate' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 2
        DB::table('installments')->insert([
            'duration' => 12,
            'rate' => 17.25,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 3
        DB::table('installments')->insert([
            'duration' => 24,
            'rate' => 18.25,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 4
        DB::table('installments')->insert([
            'duration' => 36,
            'rate' => 19.25,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // New 2W
        // 5
        DB::table('installments')->insert([
            'duration' => 6,
            'rate' => 29,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 6
        DB::table('installments')->insert([
            'duration' => 12,
            'rate' => 29,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 7
        DB::table('installments')->insert([
            'duration' => 24,
            'rate' => 30.5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 8
        DB::table('installments')->insert([
            'duration' => 36,
            'rate' => 30.50,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
