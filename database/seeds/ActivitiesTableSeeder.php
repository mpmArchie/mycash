<?php

use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')->insert([
            'name' => 'Pengajuan Baru',
            'detail' => 'Email pengajuan pinjaman sudah diajukan ke test@example.com',
            'data' => '{"Waktu Pengajuan":"12 Januari 2019","Jenis Klaim":"Life","Jenis Pengajuan":"Baru","Area Pengajuan":"Bandung","Total":"Rp 2.000.000.000"}',
            'user_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
