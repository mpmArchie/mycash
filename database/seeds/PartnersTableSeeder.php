<?php

use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('partners')->insert([
            'name' => 'Partner Satu',
            'website' => 'https://goersapp.com',
            'image_id' => 11,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('partners')->insert([
            'name' => 'Partner Dua',
            'image_id' => 12,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('partners')->insert([
            'name' => 'Partner Tiga',
            'website' => 'https://goersapp.com',
            'image_id' => 13,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
