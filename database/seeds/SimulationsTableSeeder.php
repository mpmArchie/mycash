<?php

use Illuminate\Database\Seeder;

class SimulationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // New 4W
        // 1
        DB::table('simulations')->insert([
            'name' => 'Pengajuan Baru Mobil',
            'code' => '8',
            'start_year' => 2008,
            'end_year' => 2018,
            'max_payment' => 18.25,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 1,
            'installment_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 1,
            'installment_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 1,
            'installment_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 1,
            'installment_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // New 2W
        // 2
        DB::table('simulations')->insert([
            'name' => 'Pengajuan Baru Motor',
            'code' => '7',
            'start_year' => 2008,
            'end_year' => 2018,
            'max_payment' => 30.5,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 2,
            'installment_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 2,
            'installment_id' => 6,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 2,
            'installment_id' => 7,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('installment_simulation')->insert([
            'simulation_id' => 2,
            'installment_id' => 8,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
