<?php

use App\Models\Branch;
use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            'code' => '000',
            'name' => 'Reference',
            'email' => 'reference@example.com',
            'type' => '0',
            'is_active' => false,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        Branch::resync();
    }
}
