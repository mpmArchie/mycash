<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('images')->insert([
            'filename' => 'banner-home-1.jpg',
            'path' => 'images/banner-home-1.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 2
        DB::table('images')->insert([
            'filename' => 'banner-home-2.jpg',
            'path' => 'images/banner-home-2.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 3
        DB::table('images')->insert([
            'filename' => 'banner-home-3.jpg',
            'path' => 'images/banner-home-3.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 4
        DB::table('images')->insert([
            'filename' => 'banner-about-1.jpg',
            'path' => 'images/banner-about-1.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 5
        DB::table('images')->insert([
            'filename' => 'banner-about-2.jpg',
            'path' => 'images/banner-about-2.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 6
        DB::table('images')->insert([
            'filename' => 'banner-about-3.jpg',
            'path' => 'images/banner-about-3.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        // 7
        DB::table('images')->insert([
            'filename' => 'product-car-image.jpg',
            'path' => 'images/product-car-image.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 8
        DB::table('images')->insert([
            'filename' => 'product-car-banner.jpg',
            'path' => 'images/product-car-banner.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 9
        DB::table('images')->insert([
            'filename' => 'product-shop-image.jpg',
            'path' => 'images/product-shop-image.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 10
        DB::table('images')->insert([
            'filename' => 'product-shop-banner.jpg',
            'path' => 'images/product-shop-banner.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // 11
        DB::table('images')->insert([
            'filename' => 'client1.jpg',
            'path' => 'templates/frontend/images/clients/1.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 12
        DB::table('images')->insert([
            'filename' => 'client2.jpg',
            'path' => 'templates/frontend/images/clients/2.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 13
        DB::table('images')->insert([
            'filename' => 'client3.jpg',
            'path' => 'templates/frontend/images/clients/3.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // 14
        DB::table('images')->insert([
            'filename' => 'testimony-suyitno.jpg',
            'path' => 'images/testimony-suyitno.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 15
        DB::table('images')->insert([
            'filename' => 'testimony-thomas.jpg',
            'path' => 'images/testimony-thomas.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 16
        DB::table('images')->insert([
            'filename' => 'testimony-yohan.jpg',
            'path' => 'images/testimony-yohan.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // 17
        DB::table('images')->insert([
            'filename' => 'promo-gratis.jpg',
            'path' => 'images/promo-gratis.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // 18
        DB::table('images')->insert([
            'filename' => 'promo-bunga.jpg',
            'path' => 'images/promo-bunga.jpg',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
