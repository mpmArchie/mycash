<?php

use Illuminate\Database\Seeder;

class TestimoniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testimonies')->insert([
            'name' => 'Suyitno',
            'content' => 'Telah menjadi pelanggan MPMF selama 12 Tahun, "Dari pertama kali kita ajukan sampai ambil barang prosesnya lancar terus, tidak lama tidak ada kendala . Puas sudah kita hanya ngomong sekali langsung ditindak lanjuti dengan cepat dan tidak dipersulit."',
            'image_id' => 14,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('testimonies')->insert([
            'name' => 'Thomas A. Djone Ora',
            'content' => 'Telah menjadi pelanggan MPMF selama 11 Tahun, "Cepat dan ramah, karyawannya cantik cantik, kesannya enak dan kekeluargaan, begitu sampai tau kebutuhan kita, mpmf luar biasa, bikin orang mudah, nyaman, dan lancar harus banyak lagi inovasi."',
            'image_id' => 15,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('testimonies')->insert([
            'name' => 'Yohan M. Hailitik',
            'content' => 'Telah menjadi pelanggan MPMF selama 11 Tahun, "Semua pleayanannya baik dan bagus. lebih dari itu terlepas dari service gratis, service dari bulan ke bulan itu dilayani dengan baik tanpa ada apa-apa. saya sempat ambil motor beberapa unit dan sampai sekarang masih ada 2 unit yang berjalan yang saya jaga terus dan pelihara, dan mudah mudahan jika dipercaya hari itu terus berjalan."',
            'image_id' => 16,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
