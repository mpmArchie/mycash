<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Home
        DB::table('banners')->insert([
            'title' => 'Ingin Buka Usaha Sendiri?',
            'subtitle' => 'Modal tak perlu menunggu lama. Yuk, mulai dengan MyCash',
            'image_id' => 1,
            'page_id' => 1,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('banners')->insert([
            'title' => 'Ingin si kecil meraih mimpinya?',
            'subtitle' => 'Modal tak perlu menunggu lama. Yuk, mulai dengan MyCash',
            'image_id' => 2,
            'page_id' => 1,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('banners')->insert([
            'title' => 'Ingin Jalan-Jalan keliling dunia?',
            'subtitle' => 'Modal tak perlu menunggu lama. Yuk, mulai dengan MyCash',
            'image_id' => 3,
            'page_id' => 1,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // About
        DB::table('banners')->insert([
            'title' => 'MPM Finance adalah salah satu multifinance terbesar di Indonesia',
            'subtitle' => '',
            'image_id' => 4,
            'page_id' => 2,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('banners')->insert([
            'title' => 'Dengan 100 cabang diseluruh Indonesia, kami siap mewujudkan impian Anda',
            'subtitle' => '',
            'image_id' => 5,
            'page_id' => 2,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('banners')->insert([
            'title' => 'Lebih dari 3100 karyawan terampil yang semangat untuk melayani Anda',
            'subtitle' => '',
            'image_id' => 6,
            'page_id' => 2,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
