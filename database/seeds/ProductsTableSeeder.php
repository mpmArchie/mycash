<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'MyCash<br/>Mobil atau Motor',
            'description' => '<p>Produk pembiayaan multiguna dengan jaminan BPKB Mobil/Motor yang didesain untuk menjadi solusi dari berbagai kebutuhan konsumen.</p>
<p>Jenis kebutuhan yang dibiayai:<br />Modal usaha, pariwisata, umrah dan holyland.&nbsp;Bunga mulai dari 0,8% flat perbulan.</p>
<p>Nilai Pembiayaan:<br />Maksimum 80% dari nilai kendaraan.</p>
<p>Jangka waktu pembiayaan:<br />6 bulan hingga 36 bulan</p>
<p>Tahun Kendaraan:<br />2008-2018</p>
<p>Biaya-biaya yang dikenakan:<br />Biaya administrasi dan biaya asuransi (premi dan polis)</p>',
            'summary' => 'Solusi finansial anda cukup dengan jaminan BPKB',
            'image_id' => 7,
            'banner_id' => 8,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('products')->insert([
            'name' => 'MyCash<br/>Belanja',
            'description' => '<p>MPM Finance dan Vospay menghadirkan fasilitas kredit yang memudahkan kegiatan belanja online anda. Dengan MyCash Belanja, anda dapat melakukan pembayaran dengan system angsuran hingga 12 bulan tanpa kartu kredit dan tanpa proses approval yang sulit untuk barang-barang elektronik, gadget, peralatan rumah tangga, pulsa, hingga keperluan ibu dan anak dari berbagai e-commerce terkemuka.</p>
<p>Cara Aktivasi Akun MyCash Belanja:</p>
<ol>
<li>SMS Notifikasi<br />Customer terpilih akan menerima SMS pemberitahuan dari MPM Finance mengenai penawaran fasilitas MyCash Belanja.</li>
<li>Klik Link<br />Klik link yang dikirimkan memlui SMS notifikasi anda, link tersebut mengarahkan anda ke website resmi MPM Finance di www.mpm-finance.com untuk melakukan aktivasi.</li>
<li>Aktivasi Akun<br />Segera lakukan aktivasi dan registrasi akun anda dengan memasukkan nomor HP, email, nomor KTP dan tanggal lahir yang telah terdaftar sebelumnya sesuai petunjuk yang ada.</li>
<li>Verifikasi<br />Lakukan verifikasi akun anda dengan memasukkan kode OTP yang dikirimkan ke nomor HP yang anda daftarkan</li>
<li>Nikmati fasilitas MyCash Belanja anda</li>
</ol>
<p>Cara Menggunakan fasilitas MyCash Belanja:</p>
<ol>
<li>Mulai Belanja<br />Pilih barang yang anda inginkan di berbagai e-commerce yang bermitra dengan kami.</li>
<li>Pilihan Pembayaran<br />Pada halaman checkout, pilih pembayaran dengan Vospay.</li>
<li>Verifikasi<br />Masukkan nomor HP anda yang terdaftar selanjutnya kami akan mengirimkan kode OTP untuk konfirmasi penggunaan fasilitas MyCash Belanja anda.</li>
<li>Pilih Periode Pembayaran<br />Pilih periode pembayaran angsuran yang tersedia sesuai dengan ketentuan yang berlaku.</li>
<li>Selamat transaksi anda berhasil!</li>
</ol>
<p>Partner MPMF X VOSPAY:</p>
<ol>
<li>Hartono</li>
<li>Sepulsa</li>
<li>Muslimarket</li>
<li>InstanTicket</li>
<li>Sociolla</li>
<li>The Watch Co</li>
</ol>',
            'summary' => 'Dapatkan cicilan mudah tanpa kartu kredit',
            'image_id' => 9,
            'banner_id' => 10,
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
