<?php

use Illuminate\Database\Seeder;

class MetaPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meta_page')->insert([
            'page_id' => 1,
            'meta_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('meta_page')->insert([
            'page_id' => 2,
            'meta_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
