<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ImagesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(BranchesTableSeeder::class);
        $this->call(InstallmentsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(MetasTableSeeder::class);
        $this->call(MetaPageTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(PromosTableSeeder::class);
        $this->call(TestimoniesTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(SimulationsTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
    }
}
