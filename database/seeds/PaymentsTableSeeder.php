<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            'name' => 'Melalui Kantor Cabang',
            'description' => '<p><strong>Jam operasional Senin &ndash; Jumat 08.30 &ndash; 16.30 &amp; Sabtu 08.30 &ndash; 12.00 waktu setempat</strong></p>
<ol>
<li>Kunjungi loket pembayaran angsuran di Kantor Cabang MPM Finance</li>
<li>Informasikan nama dan no.kontrak yang ingin dibayarkan</li>
<li>Bayar uang angsuran dan mintalah tanda terima resmi pembayaran</li>
<li>Simpan tanda terima resmi pembayaran sebagai bukti pembayaran yang sah</li>
</ol>',
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('payments')->insert([
            'name' => 'Melalui Giro atau Cek',
            'description' => '<ol>
<li>Tulis Nomor kontrak di balik lembar cek/giro</li>
<li>Tanggal pada cek/giro inkaso ditulis 7 (tujuh) hari sebelum tanggal jatuh tempo</li>
</ol>
<p>Note:</p>
<ul>
<li>Pembayaran akan diakui pada tanggal dana diterima atau cair di rekening MPM Finance.&nbsp;</li>
<li>Keterlambatan masuknya/ cairnya dana ke rekening MPM Finance dan biaya yang timbul akibat proses bank menjadi tanggungjawab debitur.</li>
</ul>',
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
