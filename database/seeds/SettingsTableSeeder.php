<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => 'encouragement_words',
            'value' => 'MyCash Hadir Dengan<br/>Produk Solusi Finansial Terbaik untuk Anda',
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('settings')->insert([
            'key' => 'terms_conditions',
            'value' => '<p><strong>MyCash Mobil dan Motor</strong></p>
<p>Syarat dan ketentuan:</p>
<ol>
<li>Usia minimum 21 tahun atau sudah menikah dan memiliki penghasilan sendiri.</li>
<li>Usia maksimum 60 tahun saat masa kredit selesai</li>
<li>Warga Negara Indonesia (WNI)</li>
<li>Tidak masuk dalam daftar bad debt/negative list APPI dan/atau MPM Finance</li>
</ol>
<p>Dokumen yang dibutuhkan:</p>
<ol>
<li>Fotokopi KK</li>
<li>Fotokopi NPWP</li>
<li>Fotokopi PBB/rekening listrik 3 bulan terakhir</li>
<li>Fotokopi rekening tabungan 3 bulan terakhir/rekening koran/nota/kwitansi</li>
<li>Asli slip gaji/surat keterangan kerja</li>
</ol>
<p><strong>MyCash Belanja</strong></p>
<p>Saat ini, MyCash Belanja hanya diperuntukkan bagi customer terpilih MPM Finance.&nbsp;Bagi customer terpilih yang mendapatkan fasilitas MyCash Belanja, pastikan bahwa SMS yang anda terima dikirim langsung oleh MPM Finance dan tautan yang tersedia dalam SMS tertuju ke website resmi MPM Finance di <a href="https://www.mpm-finance.com">www.mpm-finance.com</a></p>',
            'updated_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
