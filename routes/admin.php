<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\AdminHomeController@index')->name('admin.home');

Route::resource('/settings', 'Admin\AdminSettingController', ['as' => 'admin'])->except(['index', 'show']);
Route::resource('/products', 'Admin\AdminProductController', ['as' => 'admin'])->except(['show']);

Route::resource('/contents/faq', 'Admin\AdminFaqController', ['as' => 'admin.contents'])->except(['show']);
Route::put('/contents/faq', 'Admin\AdminFaqController@updateOrdering')->name('admin.contents.faq.ordering');

Route::resource('/contents/term', 'Admin\AdminTermController', ['as' => 'admin.contents'])->only(['index']);

Route::resource('/contents/payment', 'Admin\AdminPaymentController', ['as' => 'admin.contents'])->except(['show']);
Route::put('/contents/payment', 'Admin\AdminPaymentController@updateOrdering')->name('admin.contents.payment.ordering');

Route::resource('/contents/partner', 'Admin\AdminPartnerController', ['as' => 'admin.contents'])->except(['show']);
Route::resource('/contents/testimony', 'Admin\AdminTestimonyController', ['as' => 'admin.contents'])->except(['show']);
Route::resource('/contents/promo', 'Admin\AdminPromoController', ['as' => 'admin.contents'])->except(['show']);
Route::resource('/contents/banner', 'Admin\AdminBannerController', ['as' => 'admin.contents'])->except(['show']);
Route::resource('/metas', 'Admin\AdminMetaController', ['as' => 'admin'])->except(['show']);

Route::resource('/users', 'Admin\AdminUserController', ['as' => 'admin'])->except(['show']);
Route::get('/users/profile', 'Admin\AdminUserController@showProfile')->name('admin.users.profile');
Route::put('/users/profile/update', 'Admin\AdminUserController@updateProfile')->name('admin.users.profile.update');
Route::put('/users/photo/update', 'Admin\AdminUserController@updatePhoto')->name('admin.users.photo.update');
Route::get('/users/password', 'Admin\AdminUserController@showPassword')->name('admin.users.password');
Route::put('/users/password/update', 'Admin\AdminUserController@updatePassword')->name('admin.users.password.update');

Route::resource('/simulations/installment', 'Admin\AdminInstallmentController', ['as' => 'admin.simulations'])->except(['index', 'show']);
Route::resource('/simulations', 'Admin\AdminSimulationController', ['as' => 'admin'])->except(['show']);
Route::get('/simulations/branch/update', 'Admin\AdminSimulationController@updateBranches')->name('admin.simulations.branch.update');
