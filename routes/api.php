<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Models\Simulation;
use App\Models\Branch;
use App\Models\Installment;

Route::get('references/{city?}/{kecamatan?}/{kelurahan?}', function ($city = 'Nan', $kecamatan = 'Nan', $kelurahan = 'Nan') {
    $params = [];
    $params['city'] = $city;
    $params['kecamatan'] = $kecamatan;
    $params['kelurahan'] = $kelurahan;
    return response()->json([
        'params' => $params,
        'data' => api_get_address_references($city, $kecamatan, $kelurahan),
    ]);
});

Route::get('branches/{type}', function ($type) {
    $params = [];
    $params['type'] = $type;
    $branches = Branch::where('type', $type)->select('type', 'code', 'name')->get();

    return response()->json([
        'params' => $params,
        'data' => $branches->toArray(),
    ]);
});


Route::get('branches-for-postcode/{simulation}/{postcode}', function ($simulation, $postcode) {
    $params = [];
    $params['simulation'] = $simulation;
    $params['postcode'] = $postcode;
    $simulationData = Simulation::find($simulation);
    $branches = api_get_branches($postcode);
    $filteredBranches = array_where($branches, function ($branch) use ($simulationData) {
        return $branch['type'] == $simulationData->code;
    });

    $data = [];
    foreach ($filteredBranches as $key => $value) {
        $data[] = $value;
    }

    return response()->json([
        'params' => $params,
        'data' => $data,
    ]);
});

Route::get('calculate/{installment}/{total}', function ($installment, $total) {
    $params = [];
    $params['installment'] = $installment;
    $params['total'] = $total;

    $result = 0;
    $installmentData = Installment::find($installment);

    $rate = ($installmentData->rate / 100) / 12;
    $nper = $installmentData->duration;
    $pv = $total * -1;
    $fv = 0;
    $type = 0;

    if ($rate == 0)
    {
        $result = round($total / $installmentData->duration, -3);
    }
    else
    {
        // Based on https://stackoverflow.com/a/31088490/5556880
        $PMT = (-$fv - $pv * pow(1 + $rate, $nper)) / (1 + $rate * $type) / ((pow(1 + $rate, $nper) - 1) / $rate);
        $result = round($PMT, -3);
    }

    return response()->json([
        'params' => $params,
        'data' => $result,
    ]);
});

Route::get('topup/{agreementNumber}', function ($agreementNumber) {
    $params = [];
    $params['agreementNumber'] = $agreementNumber;

    $summary = api_get_summary($agreementNumber);
    $branch = Branch::where('code', $summary['branch_id'])->firstOrFail();
    $summary['email'] = $branch->email;

    return response()->json([
        'params' => $params,
        'data' => $summary,
    ]);
});

Route::get('claim/{claimType}/{agreementNumber}', function ($claimType, $agreementNumber) {
    $params = [];
    $params['claimType'] = $claimType;
    $params['agreementNumber'] = $agreementNumber;

    $summary = api_get_summary($agreementNumber);
    $branch = Branch::where('code', $summary['branch_id'])->firstOrFail();
    $claim = api_get_claim($claimType, $agreementNumber);
    $claim['email'] = $branch->email;

    return response()->json([
        'params' => $params,
        'data' => $claim,
    ]);
});

Route::get('retrieve/{agreementNumber}', function ($agreementNumber) {
    $params = [];
    $params['agreementNumber'] = $agreementNumber;

    $summary = api_get_summary($agreementNumber);
    $branch = Branch::where('code', $summary['branch_id'])->firstOrFail();
    $document = api_get_document($agreementNumber);
    $document['email'] = $branch->email;

    return response()->json([
        'params' => $params,
        'data' => $document,
    ]);
});
