<?php

/*
|--------------------------------------------------------------------------
| Customer Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Customer\CustomerHomeController@index')->name('customer.home');
Route::get('/credit', 'Customer\CustomerCreditController@index')->name('customer.credit');
Route::get('/credit/all', 'Customer\CustomerCreditController@showAll')->name('customer.credit.all');
Route::get('/credit/transaction/{agreement?}', 'Customer\CustomerCreditController@showTransaction')->name('customer.credit.transaction');
Route::get('/credit/activity', 'Customer\CustomerCreditController@showActivity')->name('customer.credit.activity');
Route::get('/credit/activity/{activity}', 'Customer\CustomerCreditController@detailActivity')->name('customer.credit.activity.detail');
Route::get('/credit/status', 'Customer\CustomerCreditController@showStatus')->name('customer.credit.status');

Route::get('/service', 'Customer\CustomerServiceController@index')->name('customer.service');
Route::get('/service/request', 'Customer\CustomerServiceController@showRequest')->name('customer.service.request');
Route::get('/service/request/new', 'Customer\CustomerServiceController@showRequestNew')->name('customer.service.request.new');
Route::post('/service/request/new', 'Customer\CustomerServiceController@storeRequestNew')->name('customer.service.request.new.store');
Route::get('/service/request/topup', 'Customer\CustomerServiceController@showRequestTopup')->name('customer.service.request.topup');
Route::post('/service/request/topup', 'Customer\CustomerServiceController@storeRequestTopup')->name('customer.service.request.topup.store');
Route::get('/service/claim', 'Customer\CustomerServiceController@showClaim')->name('customer.service.claim');
Route::post('/service/claim', 'Customer\CustomerServiceController@storeClaim')->name('customer.service.claim.store');
Route::get('/service/retrieve', 'Customer\CustomerServiceController@showRetrieve')->name('customer.service.retrieve');
Route::post('/service/retrieve', 'Customer\CustomerServiceController@storeRetrieve')->name('customer.service.retrieve.store');

Route::get('/account', 'Customer\CustomerAccountController@index')->name('customer.account');
Route::get('/account/profile', 'Customer\CustomerAccountController@showProfile')->name('customer.account.profile');
Route::put('/account/profile/update', 'Customer\CustomerAccountController@updateProfile')->name('customer.account.profile.update');
Route::put('/account/photo/update', 'Customer\CustomerAccountController@updatePhoto')->name('customer.account.photo.update');
Route::get('/account/password', 'Customer\CustomerAccountController@showPassword')->name('customer.account.password');
Route::put('/account/password/update', 'Customer\CustomerAccountController@updatePassword')->name('customer.account.password.update');

Route::get('/info', 'Customer\CustomerInformationController@index')->name('customer.info');
Route::get('/info/complain', 'Customer\CustomerInformationController@showComplain')->name('customer.info.complain');
Route::post('/info/complain', 'Customer\CustomerInformationController@storeComplain')->name('customer.info.complain.store');

