<?php

/*
|--------------------------------------------------------------------------
| Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'Site\SiteHomeController@index')->name('home');
Route::post('/subscribe', 'Site\SiteHomeController@subscribe')->name('home.subscribe');
Route::get('/about', 'Site\SiteAboutController@index')->name('about');
Route::get('/contact', 'Site\SiteContactController@index')->name('contact');
Route::post('/contact', 'Site\SiteContactController@submit')->name('contact.submit');
Route::get('/info', 'Site\SiteInfoController@index')->name('info');
Route::get('/info/article', 'Site\SiteArticleController@index')->name('info.article');
Route::get('/info/article/{id}', 'Site\SiteArticleController@detail')->name('info.article.detail');
Route::get('/info/terms', 'Site\SiteInfoController@terms')->name('info.terms');
Route::get('/info/faq', 'Site\SiteInfoController@faq')->name('info.faq');
Route::get('/product', 'Site\SiteProductController@index')->name('product');
Route::get('/product/{slug}', 'Site\SiteProductController@detail')->name('product.detail');
Route::get('/promo/{slug}', 'Site\SitePromoController@detail')->name('promo.detail');

Route::post('/simulation/calculate', 'Site\SiteSimulationController@calculate')->name('simulation.calculate');
Route::get('/simulation', 'Site\SiteSimulationController@index')->name('simulation.index');
Route::post('/simulation', 'Site\SiteSimulationController@create')->name('simulation.create');
Route::get('/simulation/verification/{calculation}', 'Site\SiteSimulationController@verification')->name('simulation.verification');
Route::put('/simulation/verification/{calculation}', 'Site\SiteSimulationController@verify')->name('simulation.verify');
Route::get('/simulation/{calculation}', 'Site\SiteSimulationController@result')->name('simulation.result');
Route::put('/simulation/resend/{calculation}', 'Site\SiteSimulationController@resend')->name('simulation.resend');
Route::put('/simulation/request/{calculation}', 'Site\SiteSimulationController@request')->name('simulation.request');
